# COMPILING

If you already have a *full* installation you can simply change to the source
directory and run

    latexmk

## Remarks

The official release version of the script uses the Lucida fonts in terms of
the lucimatx package.  All spacing was optimised for that font.  If you don't
use the Lucida font, expect overfull and underfull boxes.

Building in dvi-mode will fail, due to the microtype package.

Building was tested with TeXlive 2014.