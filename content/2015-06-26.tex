\minisec{Version 2 from \cite{schwabl}}
The Hamilton function of the Ising model is given by
\begin{align*}
  H &= - J \sum\limits_{\braket{i,j}} s_is_j - H'\sum\limits_{i}s_i,
\end{align*}
where $s_i = \pm 1$ is the configuration of a spin and $H'$ an
external field. The other method for applying the mean field
approximation is given by rewriting the interaction term
\begin{align*}
  s_is_j &= (s_i - m + m)(s_j - m + m) \\
  &= m^2 + m [(s_i-m)+(s_j-m)] + (s_i-m)(s_j-m).
\end{align*}
Now we can neglect the terms of non-linear fluctuations, viz.\
$(s_i-m)(s_j-m)$. This is allowed if we consider small
fluctuations. Hence the replacing rule is
\begin{align*}
  s_is_j \to  m^2 +  m [(s_i-m)+(s_j-m)].
\end{align*}
Plugging into the Hamilton function leads to the mean field
approximated energy
\begin{align*}
  E^{\mathrm{MF}}_{\alpha} &= J m^2 \frac{Nz}{2} - \sum\limits_i s_i (H' + Jzm).
\end{align*}
Again we can combine the bracket in the last term to a simpler total
field $H \equiv H' + Jzm$. Now we are able to calculate the partition
function
\begin{align*}
  \mathcal{Z} &= \sum\limits_{\alpha} \ee^{-\beta E^{\mathrm{MF}}_\alpha} \\
  &= \ee^{-\frac{1}{2} \beta J m' N Z } \left[ 2 \cosh(\beta \tilde{H}) \right]^N.
\end{align*}
Ignoring the external field $H' = 0$ allows a very simple calculation
of the free energy
\begin{align*}
  F &= - \kB T \log \mathcal{Z} \\
  &= - N \kB T \left\{ -\frac{Kzm^2}{2} + \log 2 + \log(\cosh(Kzm)) \right\} \\
  &= N \kB T f(K,m),
\end{align*}
where the function $f(K,m)$ denotes the free energy per spin. It is
given by
\begin{align*}
  f(k,m) &=  \frac{Kzm^2}{2} - \log 2 - \log(\cosh(Kzm)).
\end{align*}
If we use the Taylor expansion for
$\log(\cosh(ax)) = (ax)^2/2 - (ax)^4/12 + \mathcal{O}(x^6)$ the free
energy per spin function takes on the form
\begin{align*}
  f(K,m) &=  -\log 2 - \frac{Kz(Kz -1)}{2} m^2 + \frac{(K z)^4}{12} m^4.
\end{align*}
As depicted in figure~\ref{fig:26.6.15-1} (a) we can identify two
different cases:
\begin{enumerate}
\item For $K < K_c \equiv 1/z$, i.e.\ $T > T_c \equiv zJ$ we observe
  the orange function.
\item For $K > K_c \equiv 1/z$, i.e.\ $T < T_c \equiv zJ$ we observe
  the blue function.
\end{enumerate}
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =-3, xmax = 3, 
	ymin = -2, ymax =1,
	xtick=\empty, 
	ytick=\empty,
   	ylabel = $f$,
        xlabel = $m$,
	width = 5cm,
        ymajorgrids = true
        ]
	\addplot[DarkOrange3,smooth] {x^2-1}; 
	\addplot[MidnightBlue,smooth,samples=50] {-2*x^2+x^4-1}; 
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin = 0, xmax = 5, 
	ymin = -5, ymax =1,
	xtick=\empty, 
	ytick=\empty,
	xtick={1},
	xticklabels={$T_c$},
        ylabel = $F/N$,
        xlabel = $T$,
	width = 5cm,
	xmajorgrids = true,
        ]
	\addplot[DarkOrange3,smooth,domain=1:5] {-x}; 
	\addplot[dashed,smooth,domain=0:1] {-x}; 
	\addplot[MidnightBlue,smooth,samples=50,domain=0:1] {-(2-x)^2}; 
	\node[circle,fill,inner sep=1pt] at (axis cs: 1,-1) {};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{(a) Free energy per spin $f(K,m)$ as function of the
    magnetization $m$. For $K<K_c$ (high temperatures) we deal with a
    parabolic term and for $K>K_c$ (low temperature) with a ``Mexican
    Hat'' scenario. (b) Phase transition at $T_c$.}
  \label{fig:26.6.15-1}
\end{figure}
Hence the physics is given by the minimum, i.e.\ $m=0$ (no spontaneous
magnetization). According to the thermodynamic expression of the free
energy
\begin{align*}
  F &= E -TS,
\end{align*}
where $S$ is the entropy, we can distinguish between high and low
temperatures. Obviously, we can neglect the first term if we deal with
high temperatures. So we are left with $F \approx -TS$. Further
explicit calculations show
\begin{align*}
  |m| &\propto (T_c - T)^{1/2}, \\
  f &\propto - (T_c -T)^2, \quad\text{for} \quad T<T_c.
\end{align*}
As depicted in figure~\ref{fig:26.6.15-1} (b) there is a phase
transition at $T_c$ (non-analyticity at $T_c$).

\subsection{Exact solution}
\begin{figure}[tb]
  \centering
  \def\spin[#1] at (#2,#3);{
    \draw[DarkOrange3,#1] ({#2},{#3-.4}) -- ({#2},{#3+.4});
    \node[DarkOrange3,draw,fill,circle,inner sep=1pt] at ({#2},{#3}) {};
  } 
  \begin{tikzpicture}[gfx,scale=0.6]
    \draw[] (0,0) -- (10,0);
    \spin[<-] at (1,0);
    \node at (1,-1) {$1$};
    \spin[<-] at (2,0);
    \spin[->] at (3,0);
    \spin[->] at (4,0);
    \spin[<-] at (5,0);
    \spin[->] at (6,0);
    \node at (9,-1) {$N$};
    \node[DarkOrange3,draw,fill,circle,inner sep=0.6pt] at (7,0) {};
    \node[DarkOrange3,draw,fill,circle,inner sep=0.6pt] at (7.5,0) {};
    \node[DarkOrange3,draw,fill,circle,inner sep=0.6pt] at (8,0) {};
    \spin[->] at (9,0);
  \end{tikzpicture}
  \caption{One dimensional Ising model.}
  \label{fig:26.6.15-2}
\end{figure}
Here we want to deal with the one dimensional Ising model as depicted
in figure~\ref{fig:26.6.15-2}. To solve it we first have to calculate
the partition function, which is given by
\begin{align*}
  \mathcal{Z}_N &= \sum\limits_{s_1=\pm 1} \sum\limits_{s_2=\pm 1}\ldots \sum\limits_{s_N=\pm 1} \ee^{K(s_1s_2+s_2s_3+\ldots+s_{N-1}s_N)}
\intertext{Now just concentrate at the last sum over $s_N$}  
  &= \sum\limits_{s_1=\pm 1} \sum\limits_{s_2=\pm 1}\ldots \sum\limits_{s_{N-1}=\pm 1} \ee^{K(s_1s_2+s_2s_3+\ldots+s_{N-2}s_{N-1})} \underbrace{\sum\limits_{s_N=\pm 1} \ee^{Ks_{N-1}s_N}}_{2 \cosh(K)} \\
  &= \mathcal{Z}_{N-1} 2 \cosh(K) \\
  &= (2 \cosh(K))^{N-1} \mathcal{Z}_1
\intertext{where we used the symmetric behavior of $\cosh(\bullet)$. To make progress we have to take in mind that the partition function of the first spin is just $\mathcal{Z}_{1} = 2$. Hence we find}
  &= 2^N (\cosh(K))^{N-1}.
\end{align*}
The free energy for many spins $N \gg 1$ is then given by
\begin{align*}
  F &= - \kB T \log \mathcal{Z} \\
  &= - \kB T N \left[ \log 2 - \log \cosh(K)  \right].
\end{align*}
Interestingly we can see that there is no phase transition for $T>0$,
however, in the two dimensional case it was possible. If we calculate
the average of $s_i$ for $T>0$ we will also see $\braket{s_i} = 0$
(figure~\ref{fig:26.6.15-2-1}). 
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = -0.5, xmax = 2.5, 
      ymin = -1.5, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,,
      ylabel = $m$,
      xlabel = $T$,
      width = 5cm,
      xmajorgrids = true,
      ]
      \draw [help lines] (axis cs: -0.5,0) -- (axis cs: 5,0);
      \draw [help lines] (axis cs: 0,-1.5) -- (axis cs: 0,1.5);
      \node[circle,fill,DarkOrange3,inner sep=1pt] at (axis cs: 0,-1) {};
      \node[circle,fill,DarkOrange3,inner sep=1pt] at (axis cs: 0,1) {};
      \addplot[thick,DarkOrange3,domain=0:2.5] {0};
    \end{axis}
  \end{tikzpicture}
  \caption{Magnetization as function of the temperature $T$. In the
    one dimensional Ising model there is no phase transition for
    $T>0$.}
  \label{fig:26.6.15-2-1}
\end{figure}

To answer the question why there is no phase transition in the one
dimensional Ising model, we start with the following thoughts:
\begin{itemize}
\item Start at $T=0$. All $N$ spins are aligned (e.g.\ up).
\item Increase the temperature $T>0$. Assume that just one spin flips
  to ``down''. The energy difference of this spin flip is then
  $\Delta E = 4 J$.  Considering that there are $N$ possibilities to
  flip one single spin then leads to the entropy change
  $\Delta S = \log N$. Hence the free energy difference is
  \begin{align*}
    \Delta F &= \Delta E - T \Delta S \\
             &= 4 J - T \log N 
  \end{align*}
  which is smaller than zero if $T > 4J/\log(N)$. For the case of an
  infinite number of spins we find
  \begin{align*}
    \lim\limits_{N \to \infty} \frac{4J}{\log N} = 0. 
  \end{align*}
  So the critical temperature vanishes.
\end{itemize}

At any finite temperature fluctuations destroy the order. Hence the
magnetization vanishes for all finite temperatures, too.
 
\subsection{Two dimensions}
\begin{figure}[tb]
  \centering
  \def\spin[#1] at (#2,#3);{
    \draw[DarkOrange3,#1] ({#2},{#3-.4}) -- ({#2},{#3+.4});
    \node[DarkOrange3,draw,fill,circle,inner sep=1pt] at ({#2},{#3}) {};
  } 
  \def\spine[#1] at (#2,#3);{
    \draw[MidnightBlue,#1] ({#2},{#3-.4}) -- ({#2},{#3+.4});
    \node[MidnightBlue,draw,fill,circle,inner sep=1pt] at ({#2},{#3}) {};
  }
  \begin{tikzpicture}[gfx,scale=0.6]
    \foreach  \x in {0,...,8}
    \spin[->] at (\x,0);
    \foreach  \x in {0,...,2}
    \spin[->] at (\x,-1);
    \foreach  \x in {0,...,2}
    \spin[->] at (\x,-2);
    \foreach  \x in {7,...,8}
    \spin[->] at (\x,-1);
    \foreach  \x in {7,...,8}
    \spin[->] at (\x,-2);
    \foreach  \x in {0,...,8}
    \spin[->] at (\x,-3);
    \foreach  \x in {3,...,6}
    \spine[<-,] at (\x,-1);
    \foreach  \x in {3,...,6}
    \spine[<-] at (\x,-2);
    \draw [MidnightBlue,dashed,rounded corners] (2.5,-0.5) rectangle (6.5,-2.5);
  \end{tikzpicture}
  \caption{Peierl's argument. Island of $-1$ spins in a sea of $+1$
    spins. }
  \label{fig:26.6.15-3}
\end{figure}
\acct{Peierl's argument} shows that in two dimensions there is a
phase transitions. To show that we start with a lattice filled with
spins, viz.\ with $+1$. Instead of flipping just one single spin we
now flip a whole cluster of spins to $-1$. Hence we obtain a
configuration of an island of $-1$ spins in a sea of $+1$ spins (see
figure~\ref{fig:26.6.15-3}). Assume that the island of $-1$ spins has a
circumference of $L$. The energy difference is then
\begin{align*}
  \Delta E &= 2 J L
\end{align*}
and the entropy change
\begin{align*}
  \Delta S &= \log 3^L = L \log 3.
\end{align*}
The free energy change than takes on the form
\begin{align*}
  \Delta F &= \Delta E - T \Delta S \\
  &= L \left[ 2 J - T \log 3 \right].
\end{align*}
Consequently, there is a phase transition at the critical temperature
\begin{align*}
  T_c = \frac{2J}{\log 3}.
\end{align*}

\begin{notice}
  An exact solution of the two dimensional Ising model was found by
  Onsager in 1944. He showed that for a squared lattice in two
  dimension there is a phase transition as depicted in
  figure~\ref{fig:26.6.15-4}. Note that the magnetization near the
  critical temperature $T_c$ decreases with $m \propto (T_c-T)^{1/8}$.
\end{notice}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin =0, xmax = 2, 
      ymin = 0, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,
      xtick={1},
      xticklabels={$T_0$},
      ytick={1},
      ylabel = $m$,
      xlabel = $T$,
      width = 5cm,
      xmajorgrids = true,
      ymajorgrids = true
      ]
      \draw[MidnightBlue] (axis cs: 0,1) .. controls (axis cs: 1,1) and (axis cs: 1,1) .. (axis cs: 1,0);
    \end{axis}
  \end{tikzpicture}
  \caption{Exact solution of the two dimensional Ising model. Depicted
    is the magnetization as function of the temperature.}
  \label{fig:26.6.15-4}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: