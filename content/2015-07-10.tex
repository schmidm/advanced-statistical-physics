\section{Derivation of the grand-canonical ensemble} 
\index{grand-canonical ensemble}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]   
      \draw (-0.5,-0.5)--(7,-.5)--(7,3.25) --(-0.5,3.25)-- cycle;
      \draw (0,0.5)--(2,0.5) --(2,2)--  node [midway,above] {\circled{1}} (0,2)--cycle;
      \foreach \i in {1,...,250}
      \fill [MidnightBlue] (rnd*2cm+0.0cm, rnd*1.5cm+0.5cm) circle (.25pt);
      \draw (2.5,0)--(6,0)--(6,2.5)--  node [midway,above] {\circled{2}} (2.5,2.5)--cycle;
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*3.5cm+2.5cm, rnd*2.5cm) circle (.25pt);
      \draw[<->] (1.5,1.5) -- node [midway,above] {$E$}(3,1.5);
      \draw[<->] (1.5,1) -- node [midway,below] {$N$}(3,1);   
    \end{scope}
  \end{tikzpicture} 
  \caption{A small system \circled{1} and a big system \circled{2} are
    allowed to exchange energy and particels.}
  \label{fig:10.7.15-1}
\end{figure}

Given is a system as depicted in figure~\ref{fig:10.7.15-1}. To boxes
of different size are allowed to exchange particles and energy. The
Hamilton function of the whole system is
\begin{align*}
  \HH(\bm{\xi}) &= \HH_1(\bm{\xi}) + \HH_2(\bm{\xi})  = E,
\end{align*}
and shows that the total energy is conserved. Also the particle number
in the system is conserved, so we can note
\begin{align*}
  N(\bm{\xi}) &= N_1(\bm{\xi}) + N_2(\bm{\xi}) = N.
\end{align*}
If we are interested in an observable $A(\bm{\xi})$ that lives in
\circled{1}, the probability is given by
\begin{align*}
  p(A) &\propto \int \diff \bm{\xi} \delta(A(\bm{\xi}) - A) \delta(\HH(\bm{\xi}) -E).
\end{align*}
Introducing an additional integration over $E_1$ gives
\begin{multline*}
  p(A) = \int \diff \bm{\xi} \int \diff E_1 \sum\limits_{N_1} \delta(\HH_1(\bm{\xi}) -E_1) \delta(\HH_2(\bm{\xi}) -(E-E_1)) \\ \times \delta(N_1(\bm{\xi}) - N_1) \delta(A(\bm{\xi})-A) \delta(N_2(\bm{\xi}) -(N-N_1)).
\end{multline*}
Integrating over $\bm{\xi}$ leads to
\begin{align*}
  p(A)  &= \frac{N!}{N_1!N_2!} \int \diff E_1 \Omega_2(E-E_1,V_2,N-N_1) \int \diff \bm{\xi}_1 \delta(A(\bm{\xi}) - A)\delta(\HH_1(\bm{\xi}) -E_1) \\
        &= \frac{N!}{N_1!} \int \diff E_1 \tilde{\Omega}_2(E-E_1,V_2,N-N_1) \int \diff \bm{\xi}_1 \delta(A(\bm{\xi}) - A)\delta(\HH_1(\bm{\xi}) -E_1). 
\end{align*}
\begin{notice}
  To make progress we have to use the definition of the chemical
  potential and rewrite it with help of a logarithmic Taylor expansion
  gives
  \begin{align*}
    \mu &\equiv -\frac{1}{\beta} \log \tilde{\Omega}(E,V,N) \\
    \implies &\Omega(E+\Delta E,V,N+\Delta N) \approx \tilde{\Omega}(E,V,N)\ee^{\beta(\Delta E - \mu \Delta N).}
  \end{align*}
\end{notice}
Plugging in and consider that system \circled{2} is much bigger than
system \circled{1} leads to
\begin{align*}
  p(A) &\propto \frac{N!}{N_1!} \tilde{\Omega}_2(E,V,N) \int \diff \bm{\xi}_1 \diff E_1 \delta(A(\bm{\xi}_1) -A)\delta(\HH_1(\bm{\xi}_1) - E_1) \ee^{-\beta(E_1-\mu N_1)} \\
  &= N! \tilde{\Omega}_2(E,V,N) \int \frac{\diff \bm{\xi}_1}{N_1!} \delta(A(\bm{\xi})_1 -A) \ee^{-\beta[\HH_1(\bm{\xi}_1) - \nu N_1(\bm{\xi}_1)]} \\
  &= \int \frac{\diff \bm{\xi}_1}{N_1!} \delta(A(\bm{\xi}_1) - A) p_{\mathrm{gcan}} (\bm{\xi}_1),
\end{align*}
where $p_{\mathrm{gcan}} (\bm{\xi}_1)$ is the probability to be in
$\bm{\xi}_1$. The \acct{grand-canonical distribution} is defined as
\begin{align*}
  p_{\mathrm{gcan}} (\bm{\xi}_1) \equiv \frac{1}{\mathrm{Z}_{\mathrm{gcan}}} \ee^{-\beta[\HH_1(\bm{\xi}_1) - \mu N_1(\bm{\xi}_1)]}
\end{align*}
with the \acct{grand-canonical partition function}
\begin{align*}
  \mathcal{Z}_{\mathrm{gcan}} &\equiv \sum\limits_{N_1=0}^{\infty} \int \diff \bm{\xi}_1 \frac{1}{N_1!} \frac{1}{h^{3N_1}} \ee^{-\beta[\HH_1(\bm{\xi}_1) - \mu N_1(\bm{\xi})]} \\
  &= \ee^{-\beta \mathcal{J}(\beta,V,\mu)}.
\end{align*}
Here $\mathcal{J}(\beta,V,\mu)$ is the \acct{grand-canonical
  potential}. Note that we introduces the factor $1/h^{3N_1}$ to make
the partition function dimensionless. Because the particle number of
system \circled{1} fluctuates we want to calculate the mean number of
particles
\begin{align*}
  \braket{N_1} &= \int \diff \bm{\xi_1} p(\bm{\xi}_1) N_1(\bm{\xi}_1) \\
  &= \frac{1}{\beta} \partial_\mu \log \mathcal{Z}_{\mathrm{gcan}} \\
  &= - \partial_\mu \mathcal{J}(\beta,V,\mu).
\end{align*}

\section{Thermodynamic structure}

The total differential of the free energy $F(T,V,N)$ is given by
\begin{align*}
  \diff F &= \frac{\partial F}{\partial T} \diff T + \frac{\partial F}{\partial V} \diff V + \frac{\partial F}{\partial N} \diff N \\
  &= - S \diff T - p \diff V + \mu \diff N,
\end{align*}
where we use the well known relations from the first part of the
lecture. Only the last term is new. To show that this is true we look
at the total differential of the Gibbs free enthalpy $G(T,p,N)$
\begin{align*}
  \diff G &= \frac{\partial G}{\partial T} \diff T + \frac{\partial G}{\partial p} \diff p + \frac{\partial G}{\partial N} \diff N \\ 
  &= -S \diff T + V \diff p + \mu \diff N.
\end{align*}
The total differential of the entropy $S(E,V,p)$ is
\begin{align*}
  \diff S &= \frac{\partial S}{\partial E} \diff E + \frac{\partial S}{\partial V} \diff V + \frac{\partial S}{\partial N} \diff N,
\end{align*}
and for the energy
\begin{align*}
  \diff E &= T \diff S - p \diff V + \mu \diff N.
\end{align*}
We can calculate the free energy from the energy by applying the
Legendre transformation. For the grand-canonical potential we then
find
\begin{align*}
  \diff \mathcal{J}(T,\mu,V) &= \frac{\partial \mathcal{J}  }{\partial T} \diff T + \frac{\partial \mathcal{J}}{\partial V} \diff V + \frac{\partial \mathcal{J}}{\partial \mu} \diff \mu \\
  &= - S \diff T - p \diff V - N \diff \mu.
\end{align*}

\section{Gibbs-Duhem relation}

Given is a big system. The homogeneity says
\begin{align*}
  E(\lambda S, \lambda V, \lambda N) &= \lambda E(S,V,N).
\end{align*}
Derive with respect to $\lambda$
\begin{align*}
  S \frac{\partial E}{\partial S} + V \frac{\partial E}{\partial V} + N \frac{\partial E}{\partial N} = E.
\end{align*}
With $\diff E = T \diff S - p \diff V + \mu \diff N$ we find
\begin{align*}
  \boxed{TS - pV + \mu N = E}
\end{align*}
Derivation of the \acct{Gibbs-Duhem relation}:
\begin{enumerate}
\item From Gibbs free enthalpy we find
  \begin{align*}
    G(T,p,V) &= E-TS+pV \\
    &= F + pV \\
    &= \mu(T,p,V) N
  \end{align*}
  Now we want to show that $\mu(T,p,V) = \mu(T,p)$.
  \begin{proof}
    Given is Gibbs free enthalpy
    \begin{align*}
      G(t,p,V) = \mu N. 
    \end{align*}
    Deriving with respect to $N$ with constant $T$ and $p$ yields
    \begin{align*}
      \frac{\partial G}{\partial N} \bigg|_{T,p} = \frac{\partial \mu}{\partial N} \bigg|_{T,p} N + \mu. 
    \end{align*}
    We know $\partial_N G = \mu$, so the term $\partial_N \mu |_{T,p} = 0$.
  \end{proof}
\item For the grand-canonical potential we find
  \begin{align*}
    \mathcal{J}(T,V,\mu) &= E - TS - \mu N \\
    &= - p(T,V,\mu) V,
  \end{align*}
  with $p(T,V,\mu) = p(T,\mu)$. The proof is the same as before.
\item The variables $\mu$, $T$, $p$ are not independent.
  \begin{proof}
    \begin{align*}
      \diff E &= T \diff S - p \diff V + \mu \diff N \\
      &= \diff (TS-pV+\mu N) \\
      &= T \diff S + S \diff T - p \diff V - V \diff p + \mu \diff N + N \diff \mu.
    \end{align*}
    Comparing this with the known total differential of the Energy gives
    \begin{align*}
      \boxed{\diff \mu  = - s \diff T + v \diff p}
    \end{align*}
    where $s = S/N$ is the entropy per spin and $v = V/N$ the volume
    per spin. This relation is also know as \acct{Gibbs-Duhem
      relation}.
  \end{proof}
\end{enumerate}

\chapter{Quantum gases}

\section{General aspects}

\subsection{Single particle systems}

We have $N$ non-interacting particles. The Hamilton operator is given
by
\begin{align*}
  \HH_N(\bm{r}_1, \bm{p}_1,\bm{s}_1,\ldots,\bm{r}_N, \bm{p}_N,\bm{s}_N) &= \sum\limits_{j=1}^{N} \HH_1(\bm{r}_j, \bm{p}_j, \bm{s}_j).
\end{align*}
$\HH_1$ can be diagonalized into eigenstates
\begin{align*}
  \HH_1 &= \sum\limits_{i=1}^{N} E_I \ket{i}\bra{i}.
\end{align*}

\begin{example}
  \begin{enumerate}
  \item Free particle in a box. The Hamilton operator is
    \begin{align*}
      \HH_1 &= \frac{\bm{p}^2}{2m} = - \frac{\hbar^2}{2m} \bm{\nabla}^2.
    \end{align*}
    The allowed $\bm{k}$ values are
    \begin{align*}
      \bm{k} &= \frac{2 \pi}{ L}
               \begin{pmatrix}
                 n_x & n_y & n_z
               \end{pmatrix},
    \end{align*}
    where $n_\alpha = 0, \pm 1, \pm 2, \ldots$. The eigenenergies are
    \begin{align*}
      E_1 &= \frac{\hbar^2}{2m} \bm{k}^2 \\
          &= \frac{\hbar^2}{2m}\left(\frac{2 \pi }{L} \right)^2 ( n_x^2 + n_y^2 + n_z^2).
    \end{align*}
    So we can write $\ket{i} = \ket{n_x,n_y,n_z}$.
  \item With spin the vector is $\ket{i} = \ket{n_x,n_y,n_z,m_s}$. In
    a magnetic field the eigenenergies are
    \begin{align*}
      E_1 &= \frac{\hbar^2}{2m}\left(\frac{2 \pi }{L} \right)^2 ( n_x^2 + n_y^2 + n_z^2) + \mu_{\mathrm{B}} B m_s.
    \end{align*}
  \item Three dimensional harmonic oscillator the vector is
    $\ket{i} = \ket{n_x,n_y,n_z}$ and the eigenenergies are
    \begin{align*}
      E_i &= \hbar \omega \left( n_x + n_y + n_z + \frac{3}{2} \right),
    \end{align*}
    with $n_i = 0,1,2,\ldots$.
  \end{enumerate}
\end{example}

\subsection{Counting in quantum mechanics}

From the classical point of view the energy levels $i = 1,2,3,\ldots$
of a two particle system is given by the Boltzmann distribution, where
it is possible to find the particle \circled{A} and particle
\circled{B} either in different energy levels or in the same. Note
that all permutations are allowed. 

However, in quantum mechanics we deal with \acct{fermions}
(half-integer spin) or \acct{bosons} (integer spin). In the case of
fermions the \acct{Pauli exclusion principle} forbids that both
particles have the same energy level. Moreover, the particles are
indistinguishable. In the case of bosons, both particles, which are
also indistinguishable, must not obey the Pauli exclusion
principle. Hence all permutations of the energy levels are allowed.

The probability of finding both particles in the same state is
\begin{align*}
  p_{\mathrm{AB}} &= \frac{3}{9} = \frac{1}{3},\\
  p_{\mathrm{f}} &= 0,\\
  p_{\mathrm{b}} &= \frac{1}{2},
\end{align*}
where the subscript $\mathrm{AB}$ denotes the classical case,
$\mathrm{f}$ the fermionic case, and $\mathrm{b}$ the bosonic case.

\subsection{Microstates}

A microstate will be denoted as
\begin{align*}
  \ket{\alpha} &= \ket{n_0,n_1,n_2,\ldots},
\end{align*}
with eigenenenergies $E_0 ,E_1 , E_2,\ldots$. We have to consider
that for fermions and bosons different $n_i$ are possible.
\begin{itemize}
\item Fermions: $n_i = 0 ,1$ 
\item Bosons: $n_i = 0,1,2,\ldots$
\end{itemize}
Hence the number of states and the energy are given by
\begin{align*}
  N &= \sum_i n_i \, , \\
  E &= \sum_i n_i E_i \, .
\end{align*}

\subsection{Going to the ensemble level}
The \acct{canonical partition function} is given by
\begin{align*}
  Z(\beta ,V ,N) &= \sum\limits_{\alpha\big|_{N_\alpha = N}} \ee^{-\beta E_\alpha}  
 = \sum\limits_{\alpha|_{N_\alpha = N}} \ee^{-\beta \sum_i n_i E_i},
\end{align*}
where $\alpha$ denotes the sum over all microstates under the
condition $N_\alpha = N$. 

The \acct{grand-canonical partition function} is given by
\begin{align*}
  Z(\beta ,V ,N) &= \sum\limits_{N=0}^{\infty} z(\beta ,V ,N) \ee^{-\beta \mu N} \\
  &= \sum\limits_{N=0}^{\infty} \sum\limits_{\alpha|_{N_\alpha = N}} \ee^{- \beta \sum_i n_i E_i } \ee^{-\beta \mu N} \\
  &= \sum\limits_{N=0}^{\infty} \sum\limits_{\alpha|_{N_\alpha = N}} \ee^{- \beta \sum_i n_i (E_i-\mu) } \\
  &= \sum\limits_{\{n_i\}} \ee^{- \beta \sum_i n_i (E_i-\mu) } \\
  &= \sum_{n_0} \sum_{n_1} \sum_{n_2} \ldots \ee^{-\beta \sum_i n_i (E_i -\mu)} \\
  &= \prod_i \sum_{\{n_i\}} \ee^{-\beta n_i (E_i - \mu)} \equiv J_i(\beta,V,\mu).
\end{align*}
Depending on the choice we deal with two cases:
\begin{enumerate}
\item Fermions:
  \begin{align*}
    J_i &= 1+ \ee^{-\beta(E_i - \mu)} 
  \end{align*}
\item Bosons:
  \begin{align*}
    J_i &= \sum\limits_{n=0}^{\infty} \ee^{-\beta n_i (E_i -mu)} = \frac{1}{1- \ee^{-\beta (E_i-\mu)}}
  \end{align*}
\end{enumerate}
Hence the partition functions of fermions and bosons are given by
\begin{align*}
  Z_{\mathrm{FD}} &= \prod_i \left(1 + \ee^{-\beta (E_i -\mu)} \right), \\
  Z_{\mathrm{BE}} &= \prod_i \frac{1}{1- \ee^{-\beta (E_i-\mu)}}.
\end{align*}
This allows us to define the \acct{grand canonical potential}
\begin{align*}
  J(\beta,V,\mu) &= - \frac{1}{\beta} \log Z \\
  &= \pm \frac{1}{\beta} \sum_i \log \left(1 \mp \ee^{-\beta(E_i - \mu)} \right) = - pV.
\end{align*}
The average number of particles is
\begin{align*}
  \bar{N} &= \frac{1}{\beta} \partial_{\mu} \log Z = -\partial_{\mu} J.
\end{align*}
Using $\bar{n}_i = \sum_\alpha n_i p_\alpha$ leads to
\begin{align*}
  \bar{n}_i (\beta,V, \mu) &= \prod_i \sum_{n_i} n_i \ee^{-\beta n_i (E_i - \mu)} \frac{1}{Z(\beta ,V ,\mu)} \\
  &= - \frac{1}{\beta} \partial_{E_i} \log Z(\beta,V,\mu) \\
  &= \partial_{E_i} J(\beta,V,\mu) \\
  &= \pm \frac{1}{\beta} \frac{(\pm \beta) \ee^{- \beta (E_i - \mu)}}{1 \mp \ee^{-\beta(E_i-\mu)}}.
\end{align*}
Hence we find the distribution
\begin{align*}
  \bar{n}_i &= \frac{1}{\ee^{\beta(E_i -\mu)} \mp 1},
\end{align*}
which is depicted in figure~\ref{fig:distn} fermions and bosons.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =-5, xmax = 7, 
	ymin = 0, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
    	ylabel = $\bar{n}_i$,
        xlabel = $E_i$,
	xtick={1},
	xticklabel={$\mu$},
	ytick={1},
        width = 5cm,
        xmajorgrids = true
        ]
        \addplot [MidnightBlue,smooth,samples=50,domain=-5:7] {1/(exp(x-1)+1)};
        \addplot [DarkOrange3,smooth,samples=50,domain=1:7] {1/(exp(x-1)-1)};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Bose-, and Fermi distribution $\bar{n}_i$.}
  \label{fig:distn}
\end{figure}

\subsection{Classical limit I}
For a classical non-interacting gas the partition function is given by
\begin{align*}
  Z_{\mathrm{cB}}(\beta,V,N) &= \frac{1}{N!} Z(\beta,V,N=1)^N, \\
  Z_{\mathrm{mB}}(\beta,V,N) &= \sum\limits_{N=0}^{\infty} \frac{\ee^{\beta \mu N} Z^N(\beta , V ,N=1)}{N!} \\
  &= \ee^{\ee^{\beta \mu} z(\beta,V,N=1)} \\
  &= \ee^{\ee^{\beta \mu} \sum_o \ee^{-\beta E_i}}.
\end{align*}
The grand canonical potential is then given by
\begin{align*}
  J_{\mathrm{mB}} &= - \frac{1}{\beta} \log Z_{\mathrm{mB}} \\
  &= - \frac{1}{\beta} \ee^{\beta \mu} \sum_i \ee^{-\beta E_i} \\
  &= - \frac{1}{\beta} \sum_i \ee^{- \beta (E_i -\mu)}.  
\end{align*}
Using the Taylor expansions
\begin{align*}
  \ee^x &= \sum \frac{x^n}{n!} = 1+ x +\frac{x^2}{2}+ \ldots \\
  \log (1+x) &= x - \frac{x^2}{1} + \dots
\end{align*}
leads to
\begin{align*}
  J_{\mathrm{mB}} &= \lim\limits_{\beta(E,-\mu) \to \infty} \left\{ \pm \frac{1}{\beta} \sum_i \log \left(1 - \mp \ee^{-\beta (E_i -\mu)} \right)  \right\} \\
  &= \lim\limits_{\beta(E,_\mu) \to \infty} J_{\mathrm{BE}/\mathrm{FD}},
\end{align*}
with $\ee^{- \beta (e_i -\mu)} \ll 1$, i.e., small densities.

\subsection{Density of states}
For $i \to \infty$ we use the substitutions
\begin{align*}
  \sum_i &\to \int\limits_{0}^{\infty} \omega(E) \diff E, \\
  N(E) &\to \omega(E) \frac{\diff N}{\diff E}.
\end{align*}
Using periodic boundary conditions in the case of a three dimensional
box leads to the momentum vector
\begin{align*}
  \bm{p} &= \hbar \bm{k} = \frac{2 \pi \hbar }{L}
           \begin{pmatrix}
             n_x \\ n_y \\ n_z
           \end{pmatrix}.
\end{align*}
The particle number $N$ is then given by
\begin{align*}
  N(p_{\mathrm{max}}) &= \int\limits_{0}^{n_{\mathrm{max}}} \diff^d n \\
                      &= \left(\frac{L}{2 \pi \hbar}\right)^d \int\limits_{0}^{n_\mathrm{max}} \diff^d p \\
                      &= \left(\frac{L}{2 \pi \hbar}\right)^d  C_d p^d_{\mathrm{max}},
\end{align*}
where $d$ denotes the dimension. For typical values of $d$ we find
\begin{align*}
  N(p_{\mathrm{max}}) \propto
  \begin{cases}
    2 p_{\mathrm{max}}, & d=1 \\
    \pi p^2_{\mathrm{max}}, & d=2 \\
    \frac{4}{3} \pi p_{\mathrm{max}}^3, & d=3 
  \end{cases}.
\end{align*}
Note, that we have to distinguish between two regimes:
\begin{enumerate}
\item Classically: $E = p^2/2m$
  \begin{align*}
    N(E) &= \left(\frac{L}{2 \pi \hbar}\right)^d C_d (2mE)^{d/2} \\
         &=
           \begin{cases}
             \sqrt{E}, & d=1 \\ E, & d=2 \\ E^{3/2}, & d=3
           \end{cases}, \\
    \omega(E) &= \frac{\diff N}{\diff E} \propto E^{(d-2)/2} \\
         &=
           \begin{cases}
             \frac{1}{\sqrt{E}}, & d=1 \\ \const, & d=2 \\ \sqrt{E}, & d=3
           \end{cases}.
  \end{align*}
\item Relativistic regime: $E = c p$
  \begin{align*}
    N(E) &= \left( \frac{L}{4 \pi \hbar} \right)^d C_d \left( \frac{E}{c} \right)^d , \\
    \omega(E) &\propto E^{d-1}.
  \end{align*}
\end{enumerate}

\subsection{Non-relativistic quantum gas ($d=3$)}
Rewriting the grand canonical potential $J$ with $z = \ee^{\beta\mu}$  
\begin{align*}
  J(\beta,V,\mu) &= \pm \frac{1}{\beta} \sum_i \log\left[ 1 \mp \ee^{-\beta(E_i - \mu)} \right] \\
                 &= \pm \frac{V}{(2 \pi \hbar)^3} \frac{1}{\beta} \frac{4}{3} \pi \int\limits_{0}^{\infty} \diff E \log \left[ 1 \mp z \ee^{-\beta E} \right] (2mE)^{3/2}
                   \intertext{integrating by parts gives}
                 &= \pm \frac{1}{\beta} \frac{V}{(2\pi\hbar)^3} \frac{4}{3} \pi (2m)^{3/2} \bigg\{ \underbrace{[\ldots]}_{=0} - \int  E^{3/2} \frac{(\pm 1) - z \beta \ee^{-\beta E}}{1 \mp z \ee^{-\beta E} } \diff E   \bigg\}
                   \intertext{with $\lambda = (\hbar/(2 \pi m \kB T))^{1/2}$ we find}
                   J(\beta,V,\mu) &= - \frac{V}{\lambda^3} \kB T - g_{5/2}^{\pm}(z)\big|_{\mathrm{BE}/\mathrm{FD}}. 
\end{align*}
Note, that we used the \acct{polylogarithm} which is defined as
\begin{align*}
  g_{\nu}^{\pm} \equiv \frac{1}{\Gamma(\nu)} \int\limits_{0}^{\infty} \frac{x^{\nu-1}}{\ee^{x}/z \pm 1} \diff x,
\end{align*}
with the \acct{Gamma function} which is defined via a convergent
improper integral
\begin{align*}
  \Gamma(n) &\equiv \int\limits_{0}^{\infty} x^{n-1} \ee^{-x} \diff x  
\end{align*}
and fulfills the relations
\begin{align*}
  \Gamma(n) &= \Gamma(n+1), \\
  \Gamma(n) &= (n-1)!.
\end{align*}
For $n = 5/2$ the Gamma function we find
\begin{align*}
  \Gamma(5/2) &= \frac{3}{4} \sqrt{\pi}.
\end{align*}
Hence we find
\begin{align*}
  p V &= \frac{V}{\lambda^3} \kB T g_{5/2}^{\pm}(z) \\
  \implies p &= \frac{\kB T}{\lambda^3} g_{5/2}^{\pm}(z).
\end{align*}
Furthermore we can write
\begin{align*}
  \bar{N} (z) &= - \partial_\mu J(\beta,V,\mu) \\
              &= - \beta z \partial_z J  \\
              &= \frac{V}{\lambda^3} z g_{3/2}^{\pm}(z),
\end{align*}
and
\begin{align*}
  \bar{E} &= \partial_\beta \left( \frac{J}{\kB T} \right)\bigg|_z \\
          &= - \kB T^2 \partial_T \left( \frac{J}{\kB T} \right)\bigg|_z \\
          &= - \kB T^2 \partial_T \left( \frac{V}{\lambda^3} \kB T g_{5/2}^{\pm}(z)  \right)\bigg|_z \\
          &= \frac{3}{2} \kB T^2 \frac{p V}{\kB T^2} \\
          &= \frac{3}{2} p V.
\end{align*}

\section{Fermi gas}

\subsection{For $T=0$}

For zero Temperature $T=0$ the Fermi-distribution becomes a step
function
\begin{align*}
  \bar{n}_i &= \Theta\left(  \mu - E_i \right),
\end{align*}
in this case one speaks from a degenerate fermi gas. The Fermi-energy
is then given by
\begin{align*}
  E_\mathrm{f} &= \mu = \frac{p_f^2}{2 m}
\end{align*}
and the number of states
\begin{align}
  N &= \int\limits_{0}^{\mu} \omega(E) \, \diff E \\
    &= \int\limits_{0}^{\mu} \frac{V}{4 \pi^1 \hbar^3} \left( 2 m\right)^{3/2} \sqrt{E} \, \diff E \\
    &= \frac{V}{4 \pi^2 \hbar^3} \left( 2m \right)^{3/2} E_\mathrm{f}^{3/2}.
\end{align}
Rewriting leads to
\begin{align*}
  E_\mathrm{f} &= \left( \frac{6 \pi^2 N}{V} \right)^{2/3} \frac{\hbar^2}{2 m}.
\end{align*} 
This allows us to calucalte the average energy $\bar{E}$ and the
fermionic pressure $p$ at zero temperature
\begin{align*}
  \bar{E} &= \sum\limits_{i} n_i E_i = \int\limits_{0}^{\mu} \omega(E) E \, \diff E = \frac{3}{5} N E_\mathrm{f} , \\
  p &= - \frac{\partial E}{\partial V} = - \left( - \frac{2}{3} \right) \frac{E}{V} = \frac{2}{3} \frac{E}{V}.
\end{align*}
Note that in the case of $T=0$ the entropy vanishs $S=0$. The
Gibbs-Duhem relation
\[
  \mu = \frac{1}{N} (E +  p V - TS),
\]
we find for the chemical potential $\mu = E_\mathrm{f}$.

\subsection{For $0<T<E_\mathrm{f}/\kB$}

Depending on the material it is possible to find a characterisitc
Fermi-temperature $T_\mathrm{f}$ by simply rearranging
\begin{align*}
  E_\mathrm{f} = \kB T_\mathrm{f}.
\end{align*}
Typicall Fermi-temperatures are:
\begin{itemize}
\item Metals $\approx \SI{1e5}{\kelvin}$
\item Helium $\approx \SI{2}{\kelvin}$
\item nucleons $\approx \SI{1e11}{\kelvin}$
\item white dwarf $\approx \SI{1e9}{\kelvin}$
\end{itemize}

Let us caluclate the energy difference between the energy at finite
temperatures and zero temperature. It is given by the number of
electrons that jumpm multiplicated with $\kB T$
\begin{align*}
  \bar{E}(T) - \bar{E}(T=0) &=  \frac{N \kB^2 T^2}{E_\mathrm{f}}.
\end{align*}
From the Sommerfeld-expansion we get 
\begin{align*}
  \bar{E}(T) &= \bar{E}(0) + \frac{\pi^2}{4} N \frac{(\kB T)^2}{E_\mathrm{f}} + \mathcal{O}\left(T^4\right) \\
             &= N  \left( \frac{3}{5} E_\mathrm{f} + \frac{\pi^2}{4} \frac{(\kB T)^2}{E_\mathrm{f}} + \mathcal{O}\left(T^4\right) \right).
\end{align*}
The heat capacity for constant volume and particl number is 
\begin{align*}
  C_V &= \frac{\partial E}{\partial T}\big|_{V,N} = N \kB \frac{\pi^2}{2} \frac{T}{T_\mathrm{f}} + \mathcal{O}(T^3),
\end{align*}
which is obviously much smaller than in the classical case
$3 N \kB/2$. With
\begin{align*}
  C_V &= T \frac{\partial S}{\partial T}
\end{align*}
it is possible to find a relation for the entropy
\begin{align*}
  S &= N \frac{\pi^2}{4}\frac{\kB^2 T}{E_\mathrm{f}} + \mathcal{O}\left( T^2 \right).
\end{align*}

\section{Bose gas ($d=3$)}

\minisec{Photon gas}

From Maxwells equations, which describe electromagnetic waves, we can
derive a wave equation (vacuum) for the electric field
\begin{align*}
  \left( \nabla^2 + \frac{1}{c^2} \partial_t^2 \right) \bm{E} &= 0.
\end{align*}
Analogously, it is possible to find an equation for the magnetic field
$\bm{B}$. An appropriate ansatz are plain waves
\begin{align*}
  \bm{E} &= \bm{E}_0 \ee^{- \ii (\omega t - \bm{k} \cdot \bm{r})}.
\end{align*}
Plugging into the wave equation gives the well known linear dispersion
relation
\begin{align*}
  \omega = c \vert k \vert.
\end{align*}
Using one of Maxwells equation (Gauss's law) we find
\begin{align*}
  \div(\bm{E}) = 0 \quad \implies \quad \bm{E}_0 \cdot \bm{k} = 0.
\end{align*}
If dealing with waves, it is common to use periodic boundary conditions
in a three dimensional box. Hence the wave vector and energy are given
by
\begin{align*}
  \bm{k} &= \frac{2 \pi}{L} 
           \begin{pmatrix}
             n_x \\ n_y \\ n_z
           \end{pmatrix}, \quad n_i \in \mathbb{N}.
\end{align*}
The particle number $N$ is not conserved.
\begin{align*}
  \frac{\partial F}{\partial N} &= 0 = \mu \quad \implies \quad \overline{n}_i = \frac{1}{\ee^{\beta \hbar c k } -1}, \\
  E &= \hbar \omega \left( \frac{3}{2} + \ldots \right).
\end{align*}
The average energy is 
\begin{align*}
  \bar{E}' &= \bar{E} - E_0 \\
           &= \sum_i n_i \underbrace{\hbar \omega_i}_{  \to \hbar c k } \\
           &= g \cdot \frac{V}{(2\pi)^3} \int \diff^3 k \frac{\hbar c k }{\ee^{\beta \hbar c k} -1}.
\end{align*}
where we ignored the term $E_{0}$. Hence it is possible to find
\begin{itemize}
\item the \acct{Stefan-Boltzmann law}
  \begin{align*}
    \overline{E} &= 2 \frac{V}{(2 \pi)^3} \frac{4 \pi}{c^3} \int\limits_{0}^{\infty} \diff \omega \frac{\hbar \omega^3}{\ee^{\beta \hbar \omega } -1 } \\
                 &= \ldots = \frac{\pi^2}{15} V \frac{\left(\kB T\right)^4}{\hbar c)^2}.
  \end{align*}
\item the \acct{Planck law}
  \begin{align*}
    \overline{E} &= V \int\limits_{0}^{\infty} n(\omega) \, \diff \omega, \\
    n(\omega) &= \frac{\hbar }{\pi^2 c^3} \frac{\omega^3}{\ee^{\beta \hbar \omega} - 1}.
  \end{align*}
\end{itemize}


%%% Local Variables:  
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: