Introducing the \acct{Fokker-Planck operator}
\begin{align*}
  L(x) \equiv -\partial_x \left[ \mu f(x) -D \partial_x \right]
\end{align*}
allows to write the FPG in the form
\begin{align*}
  \partial_t p(x,t) &= L(x) p(x,t).
\end{align*}
Important solutions of the FPG are:
\begin{enumerate}
\item Constant force with the initial distribution $p(x,0) =
  \delta(x)$
  \begin{align*}
    p(x,t) = \left[ \frac{1}{4 \pi D t} \right]^{1/2} \exp\left[ - \frac{(x-\mu f t)^2}{4Dt} \right]. 
  \end{align*}
  The propagation of the probability distribution is depicted in
  figure~\ref{fig:10.6.15-1}.
\item Given is a potential force $f(x) = - \partial_x V(x) =
  -V'(x)$. If we look at the long time limit $t \to \infty$ and a
  vanishing probability current $j(x) = 0$ we can write the continuity
  equation in the form
  \begin{align*}
    0 = \mu V'(x) p^s - D \partial_x p^s,
  \end{align*}
  where $p^s$ is the stationary probability distribution. Obviously
  the solution of this equation is
  \begin{align*}
    p^s(x) \propto \ee^{- \beta V(x)},
  \end{align*}
  which is the Boltzmann distribution.
\end{enumerate}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = -1, xmax = 4,
      ymin = 0, ymax = 4,
      domain=-1:4,
      xlabel = $x$,
      ylabel = {$p(x,t)$},
      xtick=\empty, ytick=\empty,
      width=6cm
      ]
      \addplot [MidnightBlue,smooth,samples=50] {1/(sqrt(0.08))*exp(-(x-2*0.08)^2/(0.08))};
      \addplot [DarkOrange3,smooth,samples=50] {1/(1*sqrt(0.5))*exp(-(x-2*0.5)^2/(0.5))};
      \addplot [Purple,smooth,samples=50] {1/(sqrt(1))*exp(-(x-2*1)^2/(1))};
      \draw [help lines] (axis cs: 0,0) -- (axis cs: 0,4);
      \node[label={[MidnightBlue]-10:{$p(x,t_1)$}}] at (axis cs: 0.5,3.75) {};
      \node[label={[DarkOrange3]-10:{$p(x,t_2)$}}] at (axis cs: 0.75,2.5) {};
      \node[label={[Purple]-10:{$p(x,t_3)$}}] at (axis cs: 1.5,1.75) {};
    \end{axis}
  \end{tikzpicture}
  \caption{Time evolution of the probability distribution for a
    constant force $f = \const$.}
  \label{fig:10.6.15-1}
\end{figure}

\subsection{Propagator}

The \acct{propagator} is denoted by
\begin{align*}
  p(x',t'|x,t) \quad,\quad t'>t
\end{align*}
and describes the probability to get $x'$ at the time $t'$ if $x$ at
$t$ was given. This is also a solution of the FPG with the initial
condition
\begin{align*}
  p(x',t|x,t) &= \delta (x-x').
\end{align*}
Hence the Fokker-Planck equation is
\begin{align*}
  \partial_{t'} p(x',t'|x,t) = L(x') p(x',t'|x,t),
\end{align*}
with no explicit time dependence of the force and the diffusion
coefficient $D$. This gives the homogeneity in time
\begin{align*}
  p(x',t'|x,t) = p(x',t'-t|x,0) = p(x',0|x,t-t'),
\end{align*}
which leads to
\begin{align*}
  \partial_{t'} p(x',t'|x,t) &= - \partial_t p(x',t'|x,t).
\end{align*}
Depending which variable we hold fix we have two different
interpretations of the propagator:
\begin{enumerate}
\item For fix $x$: Probability distribution for $x'$ after time
  $t'-t$.
\item For fix $x'$: Probability distribution (probability of
  ``hitting'' x') after time $t'-t$.
\end{enumerate}

\begin{notice}[Adjoint operators:]
  Given is a complex Hilbert space with inner product
  $\braket{\bullet|\bullet}$ and an operator $A$. The \acct{adjoint
    operator} $A^\dagger$ then satisfies
  \begin{align*}
    \braket{\varphi|A \psi} = \braket{A^\dagger \varphi|\psi}.
  \end{align*}
  In integral form we can write
  \begin{align*}
    \int \diff x \varphi(x) A \psi(x) = \int \diff x (A^\dagger \varphi(x)) \psi(x).
  \end{align*}
\end{notice}

\begin{example}
  We want to show that the adjoint of $\partial_x$ is
  $-\partial_x$. Therefore we use the definition above and integration
  by parts
  \begin{align*}
    \int\limits_{-\infty}^{\infty} \diff x \varphi(x) \partial_x \psi(x) &= - \int\limits_{-\infty}^{\infty} \diff x (\partial_x \varphi(x)) \psi(x).
  \end{align*}
\end{example}

With help of these definitions we can find the adjoint Fokker-Plank
operator
\begin{align*}
  \int \diff x \varphi(x) L(x) \psi(x) &= \int \varphi(x) \left[-\partial_x\mu f(x) + D \partial_x^2  \right] \psi(x) \\
  &= \int \diff x \left[\psi(x) \mu f(x) \partial \varphi(x) + D \psi(x) \partial_x^2 \varphi(x)  \right].
\end{align*}
Comparing leads to
\begin{align*}
  L^\dagger(x) &= \mu f(x) \partial_x + D \partial_x^2.
\end{align*}

\subsection{Backward Fokker-Planck equation}

Consistency requirement
\begin{align*}
  p(x',t'|x,t) = \int \diff y p(x',t'|y,\tau) p(y,\tau|x,t),
\end{align*}
which is also known as \acct{Chapman-Kolmogorov equation}. By using
the chain rule we find
\begin{align*}
  0 &= \int \diff y \left[ (\partial_\tau p(x',t'|y,\tau)) p(y,\tau|x,t) + p(x',t'|y,\tau)  \partial_\tau p(y,\tau|x,t)\right]
  \intertext{with $\partial_\tau p(y,\tau|x,t) = L(y) p(x,\tau|x,t)$ we find}
  &= \int \diff y \left[ (\partial_\tau p(x',t'|y,\tau)) p(y,\tau|x,t) +(L^\dagger(y) p(x',t'|y,t)) p(y,\tau|x,t)\right].
  \intertext{For $\tau \to r$ we get}
  &= \int \diff y \left[ \partial_t p(x',t'|x,t) \delta(x-y) + L^\dagger(y) p(x',t'|y,t) \delta(x-y)  \right] \\
  &= \partial_ t p(x',t'|x,t) + L^\dagger(x) p(x',t'|x,t).
\end{align*}
Hence we end with 
\begin{align*}
  \boxed{ \partial_{t'} p(x',t'|x,t) = L^\dagger(x) p(x',t'|x,t)}
\end{align*}
the \acct{backward Fokker-Planck equation}.

\section{Boundary Conditions}

As shown in figure~\ref{fig:10.6.15-2} we want now discuss the
behavior of a trajectory if we choose different boundary conditions,
viz.\ reflecting and absorbing boundary conditions.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = 0, xmax = 4,
        ymin = -1.5, ymax = 1.5,
        domain= 0.:5,
        xlabel = $t$,
        ylabel = {$x(t)$},
        ytick = {-1, 1},
	yticklabels ={ $a$,$b$},
	xtick=\empty,
        width=5cm,
        ymajorgrids = true,
        xmajorgrids = true
        ]
        \addplot [MidnightBlue,samples=100,domain=0:2] {0.5*x+0.2*rnd-0.1};
        \addplot [DarkOrange3,samples=100,domain=2:4] {-0.5*x+0.2*rnd+1.9};
	\draw [pattern=north west lines , opacity=0.5] (axis cs: 0,1) rectangle (axis cs: 4,1.5);
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Trajectory under reflecting (orange) and absorbing (blue
    and filled area) boundary conditions. }
  \label{fig:10.6.15-2}
\end{figure}

For \acct{absorbing boundary conditions} particles vanishes at the
boundary. The probability is not conserved. The probability for no
absorption before the time $t$ is
\begin{align*}
  p(\text{no absorption before $t$}) &= \int\limits_{a}^{b} \diff x p(x,t)
\end{align*}
and
\begin{align*}
  p(b,t'|x,t) = 0.
\end{align*}

For \acct{reflecting boundary conditions} the flux at the boarder
vanishes
\begin{align*}
  j(b) = 0.
\end{align*}

A prototype of solution for an initial $\delta$-condition is
\begin{align*}
  p(x',t'|x,t) = \ee^{(t-t')L(x')} \delta(x-x') + \ee^{(t-t')L(x')} \varphi(x),
\end{align*}
where $\varphi(x)$ represents the stuff that is nonzero beyond $a$ or
$b$. It is used for satisfying the boundary conditions. The
Fokker-Planck equation for $a < x < b$ is unaltered. For the backward
FPG one finds:
\begin{itemize}
\item Absorbing: $p(x',t'|b,t) = 0$ 
\item Reflecting: $\partial_x p(x',t'|x,t)|_b = 0$
\end{itemize}

\section{First passage time}

The \acct{first passage time} is a random variable. As shown in
figure~\ref{fig:10.6.15-3} it is the time it need till we first reach
a special the position and will be denoted with $\mathcal{T}$.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (1.75,3.2) {(a)};
    \node at (7.25,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = -2, xmax = 2,
        ymin = -1.75, ymax = 1.25,
        domain=-2:2,
        xlabel = $x$,
        ylabel = {$V(x)$},
        xtick = {1},
	xticklabel = {$x_0$},
        ytick=\empty,
        width=5cm,
        xmajorgrids = true
        ]
        \addplot [MidnightBlue,smooth,samples=50] {-2*x^2+1*x^4-0.4*x};
	\node[circle,fill,inner sep=1pt] (a) at (axis cs: -1,-0.3) {};
	\draw[->,out=60, in = 120, looseness=2,DarkOrange3] (a) to (axis cs: 1,-1);
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5.5,0)}]
      \begin{axis}[
        xmin = -1.5, xmax = 1.75,
        ymin = -1.25, ymax = 1.5,
        domain= -1.:1.5,
        xlabel = $t$,
        ylabel = {$x(t)$},
        ytick = {-0.75},
	yticklabel = {$x_0$},
        xtick = {1.2},
	xticklabel = {$\mathcal{T}$},
        width=5cm,
        ymajorgrids = true,
        xmajorgrids = true
        ]
        \addplot [MidnightBlue,samples=100] {-0.7*x+0.2*rnd};
	\node[circle,fill,inner sep=1.5pt] (a) at (axis cs: -1,0.8) {};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Particle in a potential $V(x)$. The partilce will diffuse
    through the potential and reach after a special time the position
    $x_0$. This time is called first passage time.}
  \label{fig:10.6.15-3}
\end{figure}

Here we want to answer the question what the distribution
$P(\mathcal{T},x_0)$ is. Because this is rather hard to find we want
to calculate some important quantities, viz.\ the \acct{means first
  passage time} (MFPT)
\begin{align*}
  \mathcal{T}_1(x_0) &= \braket{\mathcal{T}} 
\end{align*}
and for higher moments
\begin{align*}
  \mathcal{T}_n(x_0) &= \braket{\mathcal{T}^n}.
\end{align*}
The probability distribution will depend on the initial value and the
first passage time, i.e.\ $p(\mathcal{T},x_0)$.

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: