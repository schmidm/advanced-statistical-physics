For long times $t \to \infty$ the probability distribution runs into
the Boltzmann distribution
\begin{align*}
  p(x,t\to\infty) &= \ee^{-\beta(V(x)-F)}.
\end{align*}

\subsection{Relaxation towards equilibrium}
We want to determine the probability distribution from the
Fokker-Planck equation (FPE) for $t \to \infty$. Therefore we use the
substitution
\begin{align*}
  p(x,t) &= \varrho(x,t)\exp\left[-\frac{\beta}{2}V(x) \right].
\end{align*}
Plugging into the FPE gives
\begin{align*}
  \partial \varrho(x,t) &= \left[ D \partial_x^2 + W(x) \right] \varrho(x,t),
\end{align*}
where we introduced the effective potential
\begin{align*}
  W(x) \equiv \frac{\beta \mu V'^2}{4} - \frac{\mu}{2} V''.
\end{align*}
Here the prime denotes the order of differentiation. Compared with the
\acct{Schrödinger equation}
\begin{align*}
  \ii \hbar \partial_t \psi(x,t) &= \left[-\frac{\hbar^2}{2m} \partial_x^2 + W(x) \right] \psi(x,t)
\end{align*}
we see a formal similarity if we introduce a imaginary time. From the
quantum mechanics lecture we know how to deal with this equation. The
eigenfunctions $\varphi_n(x)$ with energy $E_n$, i.e.\ separation of
variables leads to
\begin{align*}
  \left[ -\partial_x^2 + W(x)\right] \varphi_n(x) &= E_n \varphi_n(x). 
\end{align*}
The corresponding wave function to the ground state with energy
$E_0=0$ is
\begin{align*}
  \varphi_0(x) &= \exp\left[-\frac{\beta}{2}V(x) \right],
\end{align*}
which is true because it solves the Schrödinger equation and has no
zeros. A simple calculation shows that. Hence all other energies are
positive. We can expand each wave function, i.e.\ distribution in
eigenfunctions
\begin{align*}
  \varrho(x,t) &= \sum\limits_n c_n(t) \varphi_n(x) \\
  &= \sum\limits_n c_n(0) \exp\left[-t E_n \right] \varphi_n(x).
\end{align*}
By back substitution we end with
\begin{align*}
  p(x,t) &= \exp\left[-\frac{\beta}{2} V(x) \right] \left( c_0(0) \exp\left[- \frac{\beta}{2}V(x) \right] + \sum\limits_{n>1} c_n(0) \exp\left[-t E_n\right] \varphi_n(x) \right).
\end{align*}
For the limit $t \to \infty$ the distribution runs towards the
Boltzmann distribution
\begin{align*}
  \lim\limits_{t \to \infty} p(x,t) = c_0(0) \exp\left[-\beta V(x)\right],
\end{align*}
which still needs to be normalized. Hence any initial distribution
runs into the Boltzmann distribution. The \acct{relaxation time} for
this process is connected to the first energy eigenvalue $E_1$ and is
given by
\begin{align*}
  t_{\mathrm{rel}} &= \frac{1}{E_1}.
\end{align*}

\begin{notice}
  With inertia (underdamped regime) the Langevin equation is
  \begin{align*}
    m \ddot{x} + \gamma \dot{x} &= - \frac{\partial V}{\partial x} + \xi.
  \end{align*}
  We can reduce this differential equation of second order into two
  equations of first order
  \begin{align*}
    \dot{x} &= v, \\
    \dot{v} &= - \tilde{\gamma} v + \frac{1}{m} (-V') + \frac{1}{m} \xi.
  \end{align*}
  The corresponding Fokker-Planck equation is 
  \begin{align*}
    \partial_t p(x,v,t) &= - \nabla\cdot\bm{j},
  \end{align*}
  with
  \begin{align*}
    \bm{j} &=
    \begin{pmatrix}
      j_x \\ j_v
    \end{pmatrix} =
    \begin{pmatrix}
      v p \\
      \left[- \tilde{\gamma} V + \frac{1}{m}(-V') \right] p - \tilde{D} \partial_v p
    \end{pmatrix}
  \end{align*}
  which is also known as \acct{Klein-Kramers equation}.
\end{notice}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: