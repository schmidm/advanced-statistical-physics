\section{Canonical equilibrium, free energy, partition function, and entropy}

\subsection{Discretization}

Suppose we have some (unnormalized) probability distribution $p(x)$ with the dimension $[p]\neq 1/[x]$. The average of an observable is
\begin{align*}
  \braket{A} &= \frac{\int \diff x p(x) A(x)}{\int \diff x p(x)},  
  \intertext{discretization as depicted in figure~\ref{fig:6.5.15-1} possible for $[p] = 1$, i.e.\ if $p$ is dimensionless}
  &=  \frac{\int \frac{\diff}{\Delta x} p(x) A(x)}{\int \frac{\diff x}{\Delta x} p(x)} \\
  &\approx \frac{\sum\limits_{n=-\infty}^{\infty} p(n \Delta x) A(n \Delta x)}{\sum\limits_{n=-\infty}^{\infty} p(n \Delta x)},
  \intertext{which is identical to a Riemann sum. Rewriting by using an other notation}
  &\equiv \frac{\sum\limits_x p(x) A(x)}{\sum\limits_x p(x)}.
\end{align*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = -1, xmax = 5, 
      ymin = -0.5, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,
      xlabel = $x$,,
      width = 5cm,
      xtick = {0.25,0.5,0.75,1,1.25,1.5,1.75},
      xticklabels = {$$,$$,$\Delta x$},
      xmajorgrids = true
      ]
      \draw [help lines] (axis cs: -1,0) -- (axis cs: 5,0);
      \draw [help lines] (axis cs: 0,-0.5) -- (axis cs: 0,1.5);
      \addplot [MidnightBlue,samples=150]  {1*exp(-(x-2)^2/1)};
      \addplot [orange,samples=150]  {0.1*(x-2)^2-0.01*(x-2)^4+0.5-0.1*x};
      \node[MidnightBlue,circle,fill,inner sep=0.7pt] at (axis cs: 0.75,0.215) {};
      \node[MidnightBlue,circle,fill,inner sep=0.7pt] at (axis cs: 1,0.36) {};
      \node[MidnightBlue,circle,fill,inner sep=0.7pt] at (axis cs: 1.25,0.56) {};
      \node[MidnightBlue,circle,fill,inner sep=0.7pt] at (axis cs: 1.5,0.77) {};
      \node[orange,circle,fill,inner sep=0.7pt] at (axis cs: 0.75,0.56) {};
      \node[orange,circle,fill,inner sep=0.7pt] at (axis cs: 1,0.49) {};
      \node[orange,circle,fill,inner sep=0.7pt] at (axis cs: 1.25,0.43) {};
      \node[orange,circle,fill,inner sep=0.7pt] at (axis cs: 1.5,0.38) {};
      \node[label={[orange]-10:{$A(x)$}}] at (axis cs: 3,0.75) {};
      \node[label={[MidnightBlue]-10:{$p(x)$}}] at (axis cs: 2,1.3) {};
    \end{axis}
  \end{tikzpicture} 
  \caption{Discretization of the integral. Here $p(x)$ is the
    probability distribution and $A(x)$ the observable of interest.}
  \label{fig:6.5.15-1}
\end{figure}

The discretization cause some important modifications: 
\begin{enumerate}
\item All pairs $\diff \xi_i = \diff q_i \diff p_i$ have the dimension
  $[\diff \xi_i] = [h] = \si{\joule\second}$, which is the action. Here
  $h$ is the Planck constant. Note that all quantities of classical
  statistical physics are smooth on quantum scale. For each pair we
  choose $\Delta_\xi = h$. Hence it is possible to change from the
  integral with the differential $\diff \xi_i$ to sum over the states $\xi$
  \begin{align*}
    \int \diff \xi_i = \int \diff q_i \diff p_i \to \sum\limits_\xi.
  \end{align*}
\item Due to the practically indistinguishability of the particles we
  find
  \begin{align*}
    \int \diff \bm{\xi} \to \frac{1}{N!} \int \diff \xi,
  \end{align*}
  with $N$ identical particles.
\item Now it is possible to define the \acct{partition function}
  \begin{align*}
    Z[\beta] &\equiv \frac{1}{N!(h)^{3N}} \int \diff \xi_1 \ldots \int \diff \xi_{3N} \ee^{- \beta \HH(\xi)} \\
    &= \sum\limits_\xi \ee^{-\beta \HH(\xi)}.
  \end{align*}
\item The \acct{free energy} is defined as 
  \begin{align*}
    F(\beta) &\equiv - \frac{1}{\beta} \log Z[\beta]
    \intertext{i.e. }
    \ee^{-\beta F} &= \sum\limits_\xi \ee^{-\beta \HH(\xi)}
  \end{align*}
\end{enumerate}

Hence the probability distribution of a microstate $\xi$ is given by
\begin{align*}
  p(\xi) &= \ee^{-\beta(\HH(\xi)-F)}.
\end{align*}

\subsection{Shannon entropy (1948)}
Given is a discreet distribution $p(\xi)$. The \acct{Shannon entropy}
is defined as
\begin{align*}
  \Sh[p(\xi)] &\equiv - \sum\limits_\xi p(\xi) \log p(\xi).
\end{align*}

\begin{example}
  \begin{enumerate}
  \item Suppose we have a uniform distribution $p(\xi) = 1/N$. For $N$
    events the Shannon entropy is
    \begin{align*}
      \Sh[p(\xi) = 1/N] &= - \sum\limits_{\xi=1}^{N} \frac{1}{N} \log\left(\frac{1}{N} \right) = \log N.
    \end{align*}
    For a uniform distribution the Shannon entropy is $\log N$.
  \item Given is a certain distribution
    \begin{align*}
      p_1 &= 1 \\
      p_2 \ldots p_N &= 0.
    \end{align*}
    The Shannon entropy is then 
    \begin{align*}
      \Sh[p] &= - \sum\limits_{\xi=1}^{N} p \log p \\
      &= -1 \log 1 - \sum\limits_{\xi = 2}^{N} 0 \log 0 \\
      &= 0,
    \end{align*}
    where we used $\lim\limits_{x\to 0} x \log x = 0$.
  \end{enumerate}
\end{example}

For a factorizing distribution
\begin{align*}
  p(\xi) &= p_1(\xi_1) p_2(\xi_2), \quad \xi =(\xi_1, \xi_2)
\end{align*}
the Shannon entropy is
\begin{align*}
  \Sh[p(\xi)] &= -\sum\limits_\xi p(\xi) \log p(\xi) \\
  &= -\sum\limits_{\xi_1}\sum\limits_{\xi_2} p_1(\xi_1)p_2(\xi_2) \log\left( p_1(\xi_1)p_2(\xi_2) \right) \\
  &= \Sh[p_1(\xi_1)] + \Sh[p_2(\xi_2)].
\end{align*}
In the case of a factorizing distribution the Shannon entropy is
additive. Note that $\xi_1$ and $\xi_2$ are independent variables.

The Shannon entropy of the \acct{chain rule}
\begin{align*}
  p(x,y) = p(x|y) p(y), 
\end{align*}
where $p(x,y)$ is a joint probability and $p(x|y)$ a conditional
probability, is
\begin{align*}
  \Sh[p(x,y)] &= - \sum\limits_{x,y} p(x|y)p(y) \log\left(p(x|y) p(y) \right) \\
  &= - \sum\limits_y p(y) \underbrace{\sum\limits_x p(x|y) \log p(x|y) }_{=\Sh[p(x|y)]} - \sum\limits_y \underbrace{\sum\limits_x p(x|y)}_{=1} p(y) \log p(y) \\
  &= \sum\limits_y p(y) \Sh[p(x|y)] + \Sh[p(y)].
\end{align*}

\subsection{Entropy in the canonical distribution}

The Shannon entropy of the canonical probability distribution
\begin{align*}
  p(\xi) &= \ee^{-\beta(\HH(\xi) -F)}
\end{align*}
is given by
\begin{align*}
  \Sh[p(\xi)] &=  \sum\limits_\xi p(\xi)\underbrace{[- \beta \HH(\xi)- \beta F]}_{= \log p(\xi)} \\
  &= \beta \braket{\HH(\xi)} - \beta F \\
  &= \beta^2 \partial_\beta F,
\end{align*}
where we used the definition of the average for $\braket{\HH(\xi)} =
\sum_\xi p(\xi) \HH(\xi)$. The physical interpretation allows us to
rename the Shannon entropy to the entropy known from thermodynamics
\begin{align*}
  S(\beta) &= \beta \braket{\HH(\xi)} - \beta F = \beta^2 \partial_\beta F.
\end{align*}
\begin{proof}
  A simple calculation shows that the last equal sign is correct
  \begin{align*}
    \beta^2 \partial_\beta F &=  \beta^2 \partial_\beta \left[ - \frac{1}{\beta} \log \sum\limits_\xi \ee^{-\beta \HH}  \right] \\
    &= \log \sum\limits_\xi \ee^{-\beta \HH} - \beta \frac{\sum_\xi (-\HH)\ee^{-\beta \HH}}{\sum_\xi \ee^{-\beta \HH}} \\
    &= - \beta F + \beta \braket{\HH}.
  \end{align*}
\end{proof}

\subsection{Constrained states or meso-states}

A mesoscopic system is for example a molecule we can observe under a
microscope but not with our eyes. A meso-state is a discreet state. A
particle in a box with fluid is discreet if we are just interested in
the position of the particle in the box, i.e.\ is it either on the top
or bottom part of the box. These two positions are two discreet
meso-states. Another example is a polymer. Are we only interested in
the folded or in the unfolded configuration of the polymer we deal
again with two discreet states.

Let $Y$ be the discreet meso-state in the canonical equilibrium. Any
$\xi$ belongs to one $Y$. The characteristic conditional probability
of $\xi$ conditioned on $y$ is
\begin{align*}
  \chi(\xi|Y) &=
  \begin{cases}
    1 ,& \text{if $\xi$ belongs to $Y$}\\
    0 ,& \text{otherwise}
  \end{cases} \\
  &= p(Y|\xi).
\end{align*}
The \acct{conditional probability} is given by
\begin{align*}
  p(\xi|Y) &= p(\xi|Y) p(\xi) = \chi(\xi,Y) \ee^{-\beta(\HH(\xi) - F)},
\end{align*}
where $p(\xi,Y)$ is the joint probability and $p(\xi|Y)$ the
conditional probability. We then find
\begin{align*}
  p(\xi|Y) &= \frac{p(\xi,Y)}{p(Y)} = \frac{\chi(\xi|Y)\ee^{-\beta(\HH(\xi)-F)}}{p(Y)}
\end{align*}
with
\begin{align*}
  p(Y) &= \sum\limits_\xi \chi(\xi|Y) \ee^{-\beta (\HH(\xi)-F)} \\
  &\equiv \ee^{-\beta (F(Y)-F)}.
\end{align*}
The mean energy in the state $Y$ is 
\begin{align*}
  \braket{\HH|Y} &= \sum\limits_\xi p(\xi|Y) \HH(\xi) \\
  &= \frac{1}{\beta} \Sh[p(\xi|Y)] + F(Y) \\
  &= \frac{1}{\beta} \beta^2 \partial_\beta F(Y) + F(Y).
\end{align*}
In equilibrium 
\begin{align*}
  \log\left(\frac{p(Y^2)}{p(Y^1)}\right) &= - \beta \left( F(Y^2) - F(Y^1) \right) \\
  &= - \Delta(\beta F).
\end{align*}
compared to the microcanonical case
\begin{align*}
  \log\left(\frac{p(\xi^2)}{p(\xi^1)}\right) &= - \beta \Delta \HH.
\end{align*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: