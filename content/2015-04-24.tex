A good approximation of $\Omega^k(E)$ is given by
\begin{align*}
 \Omega^k(E+\Delta E) &= \Omega^k(E) \ee^{\int\limits_{0}^{\Delta E} \diff E' \beta(E')}  
 \intertext{expanding around $\Delta E = 0$ yields}
 &= \Omega^k(E)\ee^{\Delta E + \frac{1}{2} \beta^{k\prime}(E) (\Delta E)^2}. 
\end{align*}
This approximation is also a good one for high energy values. Against
this, a simple linear approximation of the form $\Omega^k(E) + \Delta
E \Omega^{k\prime}(E)$ is for high energy values, that scale with $E^N$,
not enough. Hence the most likely value $\hat{E}^k$ is
\begin{align*}
  \partial_{E^k} p(E^k)\big|_{\hat{E}^k} &= \Omega^{k\prime}(\hat{E}^k) \Omega^{c}(E-E^k) - \Omega^{k}(E^k) \Omega^{c\prime}(E-E^k) \stackrel{!}{=} 0 \\
  \implies \frac{\Omega^{k\prime}(\hat{E}^k)}{\Omega^{k}(E^k)} &= \frac{\Omega^{c\prime}(E-\hat{E}^k)}{\Omega^{c}(E-\hat{E}^k)}.
\end{align*}

\subsection{Configurational integral and configurational average}

The configurational integral and the corresponding $\beta$-function are
defined as
\begin{align*}
  \Omega^{c}(E) &\equiv \int \diff \bm{q} \delta \left( \Phi(\{q_j\})- E \right), \\
 \beta^c(E) &\equiv \partial_{E} \log \Omega^c(E). 
\end{align*}
The configurational average is then
\begin{align*}
  \braket{A(\{q_j\})|E}^c &\equiv \frac{\int \diff q_j \delta(\Phi(\{q_j\})-E) A(\{q_j\})}{\int \diff q_j \delta(\Phi(\{q_j\})-E)}.
\end{align*}
The $\beta$-function of the kinetic integral and the configuratinal
integral are then related to each other by
\begin{align*}
  \beta^k(\hat{E}^k) &= \beta^c(E-\hat{E}^k).
\end{align*}
The most likely value of the kinetic energy is determined by
$\hat{E}^k = \hat{E}^k(E,V,N)$. The derivative of the total energy
leads to
\begin{align}
  \partial_E \beta^k(\hat{E}^k)  \frac{\partial \hat{E}^k}{\partial E} &= \beta^{c\prime} (E-\hat{E}^k) \left(1-\frac{\partial \hat{E}^{k}}{\partial E} \right). \label{eq:24.04.15-1}
\end{align}

\subsection{Derivation of the Gaussian distribution}

Given is the microcanonical distribution
\begin{align*}
  p(E^k) &= \frac{\Omega^k(E^k) \Omega^C(E-E^k)}{\Omega(E)}.
\end{align*}
Expanding around $E^k$ lads to 
\begin{multline*}
  p(E^k) \approx \frac{1}{\Omega(E)} \Omega^k(E^k) \ee^{\beta^k(\hat{E}^k) (E^k -\hat{E}^k) + \frac{1}{2} \beta^{k\prime}(E^k) (E^k - \hat{E}^k)^2} \\ \times \Omega^c(E-\hat{E}^k)\ee^{-\beta^c(E-\hat{E}^k)(E-\hat{E}^k)+\frac{1}{2}\beta^{c\prime}(E-\hat{E}^k)(E-\hat{E}^k)^2},
\end{multline*}
where we used the relation $E^k = \hat{E}^k +(E^k -\hat{E}^k)$. Hence we see the \acct{Gaussian distribution}
\begin{align*}
  p(E^k) \propto \ee^{-\frac{1}{2}\frac{(E^k-\hat{E}^k)^2}{(\Delta E^k)^2}},
\end{align*}
where the variance of the Gaussian is given by
\begin{align*}
  (\Delta E^k)^2 &= \left[ \beta^{k\prime}  + \beta^{c\prime}\right]^{-1} \\
  &\stackrel{\eqref{eq:24.04.15-1}}{=} \left[ \beta^{k\prime} \left(1+\frac{E^{k\prime}}{1-E^{k\prime}} \right)  \right]^{-1} \\
  &= \left[ \frac{\beta^{k\prime}}{1-E^{k\prime}} \right]\\
  \implies (\Delta E^k)^2 &= \frac{1-E^{k\prime}}{\beta^{k\prime}} \\
  &\approx \frac{2}{3} \frac{(E^k)^2}{N} \underbrace{\left(1-\frac{\partial E^k}{\partial E} \right)}_{= \mathcal{O}(1)}\\
  &= \frac{2}{3}\frac{N^2(\varepsilon^k)^2}{N} \propto \mathcal{O}(N).
\end{align*}
The term $\left(1-\frac{\partial E^k}{\partial E} \right)$ is of order
$\mathcal{O}(1)$ because $E$ and $E^k$ scales with $N$. As depicted
in figure~\ref{fig:24.04.15-1} the distribution $p(E^k)$ is an
extremely narrow Gaussian distribution. 
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = 0, xmax = 3.5, 
      ymin = 0, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,
      ylabel = {$p(E^k)$},
      xtick = {2.5},
      xticklabels = {$\hat{E}^k\propto N$},
      xlabel = $E^k$,
      width = 5cm,
      xmajorgrids = true
      ]
      %	\draw [help lines] (axis cs: -3,0) -- (axis cs: 5,0);
      %	\draw [help lines] (axis cs: 0,-3) -- (axis cs: 0,5);
      \addplot [MidnightBlue,smooth,samples=150]  {1.3*exp(-(x-2.5)^2/0.025)};
    \end{axis}
  \end{tikzpicture} 
  \caption{Gaussian distribution $p(E^k)$ as function of the kinetic
    energy. The width of the Gaussian is proportional to $N^{1/2}$. }
  \label{fig:24.04.15-1}
\end{figure}
In the case of the central limit theorem we assumed independent
variables, whereas here only the kinetic energies (not
independent) were used. Especially for small $(\Delta E)^2$ the
distribution says that the energy is almost equal on the whole energy
shell.

\subsection{Inverse temperature}\index{Inverse temperature}
Given is a system $E$, $V$, $N$. Hence kinetic energy is
$\hat{E}^k(E,V,N)$. The $\beta$-function is then given by
\begin{align*}
  \beta^k(\hat{E}^k) &\approx \frac{3}{2}\frac{N}{\hat{E}^{k}} = \frac{3}{2 \hat{\varepsilon}^k}
\end{align*}
and $\hat{E}^k$ is the mean kinetic energy. 
\begin{align*}
  \implies \beta(E) &\equiv \beta^k(\hat{E}^k(E)) \\
  &= \beta^C(E-\hat{E}^k(E)) \\
  &= \frac{3}{2} \frac{1}{\hat{\varepsilon}^k\left(\frac{E}{N},\frac{V}{N}\right)}.
\end{align*}
The inverse temperature $\beta$ is $1/2$ of the (mean) kinetic energy
per degree of freedom.

\begin{notice}
  From textbooks or undergraduate lectures we know: $\braket{E^k} = \kB
  T/2$ and hence $\beta = 1/(\kB T)$.
\end{notice}

\subsection{Total \texorpdfstring{$\Omega$}{Ω} function} 

The total $\Omega$ function is given by
\begin{align*}
  \Omega(E) &= \int \diff E^k \Omega^k(E^k) \Omega^c(E-E^k).
\end{align*}
Expanding around $\Delta E = 0$ leads to
\begin{align*}
  \Omega(E+\Delta E) &= \int \diff E^k \Omega^k(E^k+\Delta E)\Omega^c(E + \Delta E - E^k) \\
  &\approx \int \diff E^k \Omega^k(E^k) \ee^{\beta^k(E^k) \Delta E} \Omega^c(E-E^k)\ee^{\beta^C(E-E^k) \Delta E} \\
  &= \int \diff E^k \Omega^k(E^k) \Omega^c(E-E^k) \underbrace{\ee^{\left(\beta^k(E^k) + \beta^c(E-E^k)\right) \Delta E}}_{\text{weakly depending on $E^k$}} \\
  &= \ee^{\beta(E) \Delta E} \Omega(E) \left(1+ \mathcal{O}(1(N) \right).
\end{align*}
In figure~\ref{fig:24.04.15-2} the function $\Omega^c$, $\Omega^k$,
the product of both, and the weakly from $E^k$ depending term are
depicted.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (0.55,2.2) {$\Omega^c$};
    \node (a) at (2.9,2.2|-a) {$\Omega^k$};
    \begin{axis}[
      xmin = 0, xmax = 5, 
      ymin = 0, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,
      ylabel = {$\Omega^c$, $\Omega^k$},
      xtick = {2.5},
      xticklabels = {$\hat{E^k}$},
      xlabel = $E^k$,
      width = 5cm,
      xmajorgrids = true
      ]
      %	\draw [help lines] (axis cs: -3,0) -- (axis cs: 5,0);
      %	\draw [help lines] (axis cs: 0,-3) -- (axis cs: 0,5);
      % \addplot [orange,smooth,samples=100]  {0.5*exp(-(x-2.5)^2/0.1)};
      \addplot [DimGray,smooth,samples=100]  {2*exp(-((x+1)^2)/3)};
      \addplot [domain=0:5,DimGray,smooth,samples=100]  {2*exp(-((x-6)^2)/3)};
      % \addplot [MidnightBlue,samples=150]  {(1*exp(-(x-0.5))+(0.03*exp(x-1))};
      \addplot [DarkOrange3,smooth,samples=100]  {300*(2*exp(-((x+1)^2)/3))*(2*exp(-((x-6)^2)/3))};
      \addplot [MidnightBlue,smooth,samples=100]  {-0.01*(x-5)^2+0.7};
    \end{axis}
  \end{tikzpicture}  
  \caption{Behaviour of the functions $\Omega^c$, $\Omega^k$, the
    product of both (orange), and the weakly from $E^k$ depending term
    (blue) as function of the kinetic energy.}
  \label{fig:24.04.15-2}
\end{figure}


\chapter{Canonical distribution}

\section{Derivation}

We have a small system (colloidal particle, \ldots) in contact/being
part of a bigger system. The whole system is isolated. Hence it is
subjected to the microcanonical distribution. 
\begin{notice}[Aim:]
  Derive the distribution of observable referring only to the small
  system.
\end{notice}
The Hamilton function is 
\begin{align*}
  \HH(\xi_0,\xi) &= \HH_0(\xi_0) + \HH_{\mathrm{int}}(q_0,q) + \HH(\xi),
\end{align*}
where $\HH_0(\xi_0)$ describes the small system,
$\HH(\xi) = \sum_j p_j^2/(2m) + \Phi(\{q_j\})$ the bath/reservoir (the
big system) and $\HH_{\mathrm{int}}$ the interaction between both
systems. The coordinates $\xi = (p,q)$ labeled with $0$ refer to the
small system. Non labeled coordinates to the big system. Note that
$\HH_0(\xi_0), \HH_{\mathrm{int}}(q_0,q) \ll \HH$. The microcanonical
distribution for the full system implies
\begin{align*}
  p(\xi_0) &= \int \frac{\delta(\HH(\xi_0,\xi)-E)}{\Omega(E)} \diff\xi. 
  \intertext{Introducing an additional integration over the
    configurational energy}
  &\propto \int \diff \bm{q} \int \diff \bm{p} \int\diff E^c \delta(\Phi(\{q_j\})-E^c) \delta(\HH_o(\xi_0) + \HH_{\mathrm{int}}(q_0,q) + E^c + \sum_j \frac{p_j^2}{2m} - E) 
  \intertext{Integrating over the kinetic energy}
  &= \int\diff\bm{q}\int\diff E^c \delta(\Phi(\{q_j\})-E^c) \underbrace{\Omega^k(\HH_0(\xi_0) + \HH_{\mathrm{int}}(q_0,q) + E^c -E)}_{\text{$\HH_0+\HH_{\mathrm{int}}$ small compared to $E^c$}} \\
  &\propto \int\diff\bm{q}\int\diff E^c \delta(\Phi(\{q_j\})-E^c) \ee^{-\beta^k(E-E^c) \left[ \HH_0(\xi_0) + \HH_{\mathrm{int}}(q_0,q) \right]} \Omega^k{E-E^c} \\
  &= \int \diff E^c \ee^{-\beta^k(E-E^c)\HH_0(\xi_0)} \braket{\ee^{-\beta^k(E-E^c)\HH_{\mathrm{int}}(q_0,q)}|E^c} \Omega^c(E^c) \Omega^k(E-E^c) 
  \intertext{where $\braket{\bullet|E^c}$ is a smooth and slowly increasing function, $\Omega^c(E^c)$ a fast increasing function, and $\Omega^c(E^c)$ a fast decreasing function. Using a saddle point integration leads to}
  &\propto \ee^{-\beta^k(E-E^c)\HH_0(\xi_0)}\braket{\ee^{-\beta^k(E-\hat{E}^c)\HH_{\mathrm{int}}(q_0,q)}|\hat{E}^c} \Omega^c(\hat{E}^c) \Omega^k(E-\hat{E}^c) \\
  &\propto \ee^{-\beta \HH_0(\xi_0)} \braket{\ee^{-\beta\HH_{\mathrm{int}}(q_0,q)}}. 
\end{align*}


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: