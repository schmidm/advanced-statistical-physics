\subsection{Monte Carlo Simulation (\texorpdfstring{$d=3$}{d=3})}

The method is known to the audience. Artificial dynamics
$\{ \bm{s}_i \}_t \to \{\bm{s}_i\}_{t+\Delta t}$ such that
\begin{align*}
  \overline{A}\{\bm{s}_i\} &= \lim\limits_{\tau \to \infty} \frac{1}{\tau} \int\limits_{0}^{\infty} \overline{A}\{\bm{s}_i(\tau')\} \diff \tau' \\
  &= \sum\limits_{\{\bm{s}_i\}} A(\{\bm{s}_i\}) \ee^{-\beta \HH(\{\bm{s}_i\})} \frac{1}{\mathcal{Z}}.
\end{align*}
Typical clusters are depicted in figure~\ref{fig:1.7.15-1}. 
\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] at (0,0) {\includegraphics[width = 0.5\textwidth]{Ising.pdf}};
    \node at (0,3.5) {(a)};
    \node at (3.5,3.5) {(b)};
  \end{tikzpicture}
  \caption{Graphical output from some runs of the Ising model. (a) For
    eight runs at temperature $T=5$. (b) For a few billion runs at
    temperature $T=2.27$ and a much greater lattice. Taken from
    \cite{schroeder}.}
  \label{fig:1.7.15-1}
\end{figure}
As shown in figure~\ref{fig:1.7.15-1} (a) there are some clusters of
spin up or spin down. Here we have the temperature $T=5$
(dimensionless). The correlation length for this case is about
$\xi = 3 \pm 2$.  Note that the \acct{correlation length} $\xi$ is
given by
\begin{align*}
  \braket{s_is_j} &= f(|i-j|,K) \propto \ee^{-\frac{|i-j|}{\xi(\tau)}},
\end{align*}
where $|i-j|$ is the distance and $f(|i-j|,K)$ the correlation
function. In the case of figure~\ref{fig:1.7.15-1} (b) the correlation
length becomes infinity $\xi = \infty$, so it is impossible to
distinguish between the depicted case and a blown up picture. This is
a characteristic signature of a second order phase transition or a
critical point. The correlation length as function of the temperature
is
\begin{align*}
  \xi(T) &\propto
           \begin{cases}
             A_+ |T-T_c|^{-\nu} &,\text{for $T>T_c$} \\
             A_{-} |T_c-T|^{-\nu} &,\text{for $T<T_c$}
           \end{cases}
\end{align*}
as depicted in figure~\ref{fig:1.7.15-2}. Here $\nu$ is a
\acct{critical exponent}
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx] 
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin = 0, xmax = 5, 
	ymin = 0, ymax =5,
	xtick=\empty, 
	ytick=\empty,
	xtick={1},
	xticklabels={$T_c$},
        ylabel = $\xi$,
        xlabel = $T$,
	width = 5cm,
	xmajorgrids = true,
        ]
	\addplot[MidnightBlue,smooth,domain=1:5] {-(1-x)^(-1)}; 
	\addplot[MidnightBlue,smooth,domain=-0:0.9] {-(x-1)^(-1))};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Correlation length $\xi$ as function of the temperature. At
    $T_c$ the correlation length diverges, as it is typical for a
    second order phase transition.}
  \label{fig:1.7.15-2}
\end{figure}
Below temperature $T_c$ we find for the \acct{correlation function}
\begin{align*}
  G(\bm{x}_i,\bm{x}_j) \equiv \braket{(s_i - \braket{s_i})(s_j-\braket{s_j})} \propto \ee^{-\frac{|i-j|}{\xi}},
\end{align*}
which is a measure of how the deviation of the according mean values
at $i$ and $j$ are correlated with each other.


\subsection{Scaling relations and critical exponents}

Here we introduce some \acct*{critical exponents} (``indices''). The
magnetization is then given by
\begin{align*}
  m \simeq (T_C-T)^\beta, \quad T<T_c
\end{align*}
and at the critical temperature $T_c$
\begin{align*}
  m &\propto |H|^{1/5},\\
  \xi &\propto A_\pm |T-T_c|^{-\nu}.
\end{align*}
The free energy is
\begin{align*}
  f \propto A_\pm |T - T_c |^{2-\alpha} + \ldots
\end{align*}
The \acct{susceptibility} is defined as
\begin{align*}
  \frac{\partial m}{\partial H} &= \chi^{\pm} \simeq \frac{A_\pm}{|T-T_c|^\gamma}.
\end{align*}
All in all we have five critical exponents which we can measure by experiments. Three of them are
\begin{align*}
  \alpha &= 2 - \beta (1+\delta), \\
  \gamma &=- \beta(1-\delta), \\
  2- \alpha &= d \nu.
\end{align*}
The last equation is also known as \acct{hyperscaling relation}. Note
that only two of them are independent.
\begin{table}[tb]
  \centering
  \caption{Comparison of the mean field theory (MF) with simulations for the one, two, and three dimensional Ising model.}
  \label{tab:1.7.15-1}
  \begin{tabular}{ccccc}
    \toprule
    & MF  & $d=1$ & $d=2$ & $d=4$  \\   
    \midrule    
    $T_c$  & $2DJ$ & - & $0.56 (\times 4J)$ & $0.75 (\times 6J)$ \\
    $\beta$  & $\frac{1}{2}$ & -  & $\frac{1}{8}$  & $\simeq 0.32$  \\
    $\alpha$ & $0$ & - & $0$ & $\simeq{0.11}$ \\             
    \bottomrule  
  \end{tabular}
\end{table}
As tabulated in table~\ref{tab:1.7.15-1} we see that the mean field
approximation becomes better the higher the dimension is. It becomes
exact for $d>4$ (more neighbors)! In the case of the Ising model the
upper critical dimension is $d=4$ and the lower critical dimension is
$d=1$.

\section{Renomalization Group (Wilson, Fisher, Kadanoff)}

\subsection{The idea of the RG: One dimensional Ising model}

The partition function for the one dimensional Ising model is given by
\begin{align*}
  \mathcal{Z}_{N}(K) &= \sum\limits_{s_1=\pm1}\ldots\sum\limits_{s_N=\pm1} \ee^{K(s_1s_2+s_2s_3+ \ldots +s_{N-1}s_N )}. 
  \intertext{We start with the "even" spins}
  &= \sum\limits_{s_1,s_3,\ldots,s_{N-1}} \bigg( \sum\limits_{s_2=\pm1} \ee^{K(s_1s_2+s_2s_3)} \bigg) \bigg( \sum\limits_{s_3=\pm 1} \ldots \bigg) \ldots 
  \intertext{what we want is}
  &\stackrel{!}{=} \sum\limits_{s_1,s_3,\ldots} \ee^{K'(s_1s_3+s_3s_5+s_5s_7+\ldots)} \left[ \ee^{2g} \right]^{N/2.}
\end{align*}
It will work if
\begin{align*}
  \sum\limits_{s_2=\pm 1} \ee^{K(s_1s_2+s_2s_3)} \stackrel{!}{=} \ee^{K'(s_1s_2)} \ee^{2g}.
\end{align*}
If $s_1 = s_3 = \pm 1$ a simple calculation leads to
\begin{align}
  2 \cosh(2K) &= \ee^{K'} \ee^{2g}. \label{eq:1.7.15-1}
\end{align}
If $s_1 = -s_3$ we get
\begin{align}
  2 = \ee^{-K'} \ee^{2g}. \label{eq:1.7.15-2}
\end{align}
Hence we have two equations and two unknowns. Dividing
equation~\eqref{eq:1.7.15-1} by equation~\eqref{eq:1.7.15-2} gives
\begin{align*}
  \cosh(2K) &= \ee^{2K'} \\
  \implies K' &= \frac{1}{2} \log(\cosh(2K)).
\end{align*}
Then using equation~\eqref{eq:1.7.15-2} again allows us to determine $g$,
so we end with
\begin{align*}
  g &= \frac{1}{2} \log\left(2 \ee^{K'}\right) \\
  &= \frac{1}{2} \log 2 + \frac{1}{2}K' \\
  &= \frac{1}{2}\left[ \log 2 + \frac{1}{2} \log(\cosh(2K)) \right].
\end{align*}
The variable $K'$ is then given by
\begin{align*}
  K' =
  \begin{cases}
    K^2 &,K\ll 1\\
    K - \frac{1}{2} - \log 2 &,K\gg 1
  \end{cases}
\end{align*}
Consequently, by using this method we lost the half of the
spins. Iterating this over $m$ steps gives the recursive equations
\begin{align*}
  K^{(m)} &= \frac{1}{2} \log\left( \cosh\left(2 K^{(m+1)}  \right) \right),\\
  g^{(m)} &= g(K^{(m)}) = \frac{1}{2} \log 2 + \frac{1}{2} K^{(m)}.
\end{align*}
Hence we deal with a kind of discrete dynamics. In
figure~\ref{fig:1.7.15-4} there is depicted the discrete dynamics of
$K^{(m)}$ for some iterations. As shown there is a \acct{renormalization
flow} towards an attractive fix point.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = 0, xmax = 1.5, 
      ymin = 0, ymax =1.5,
      xtick=\empty, 
      ytick=\empty,
      xlabel = $K^ {(m+1)}$,
      ylabel=$K^{(m)}$,
      width = 5cm,
      xtick = {1,0.66,0.35,0.11},
      xticklabels = {$K^{(0)}$,$K^{(1)}$,$K^{(2)}$},
      xmajorgrids = false
      ]
      \draw [help lines] (axis cs: 1,0) -- (axis cs: 1, 0.66 );
      \draw [help lines,->] (axis cs: 1,0.66) -- (axis cs: 0.66, 0.66 );
      \draw [help lines,] (axis cs: 0.66,0.66) -- (axis cs: 0.66, 0 );
      \draw [help lines,->] (axis cs: 0.66,0.35) -- (axis cs: 0.35, 0.35 );
      \draw [help lines,] (axis cs: 0.35,0.35) -- (axis cs: 0.35, 0 );
      \draw [help lines,->] (axis cs: 0.35,0.11) -- (axis cs: 0.11, 0.11);
      \draw [help lines] (axis cs: 0.11,0.11) -- (axis cs: 0.11,0);
      \addplot [samples=10]  {x};
      \addplot [DarkOrange3,samples=50,smooth]  {0.5*ln(cosh(2*x))};
    \end{axis}
  \end{tikzpicture} 
  \caption{Iteration map of $K^{(m)}$ over $K^{(m+1)}$. The
    renormalization flow seeks to an attractive fix point.}
  \label{fig:1.7.15-4}
\end{figure}
The renormalization flow has an attractive fix point $K^\ast = 0$ (no
coupling) and a repulsive fix point $K^\ast = \infty$. The crucial
point is what happens with the correlation length
\begin{align*}
  \xi(K') &= \frac{1}{2} \xi(K).
\end{align*}
The partition function after one step takes on the form
\begin{align*}
  \mathcal{Z}_N(K) &= \mathcal{Z}_{\frac{N}{2}}(K') \left[ \ee^{2g} \right]^{N/2}.
\end{align*}
After $M$ steps we get
\begin{align*}
  \mathcal{Z}(K) &= \mathcal{Z}_{\frac{N}{2M}}(K^{(M)}) \ee^{2 g(K^{(1)}) \frac{N}{2}} \ee^{2 g(K^{(2)})\frac{N}{4}} \ldots \\
  &= \mathcal{Z}_{\frac{N}{2M}}(K^{(M)}) \exp\bigg[2 \sum\limits_{m=1}^{M} g(K^{(m)}) \frac{N}{2^m} \bigg].
\end{align*}
The free energy per spin is then given by
\begin{align*}
  \frac{\text{free energy}}{\text{spin $\kB T$}} &= - \frac{1}{N} \log \mathcal{Z}_N \\
  &= - 2 \sum\limits_{m=1}^{M} \frac{1}{2^m} g(K^{(m)}) - \frac{1}{N} \log\left( \mathcal{Z}_{\frac{N}{2M}}(K^{(M)}) \right).
\end{align*}
The second term on the right side of this equation vanishes because
\begin{align*}
  \frac{1}{N}\lim\limits_{M \to \infty} \log\left( \mathcal{Z}_{\frac{N}{2M}}(K^{(M)}) \right) = \frac{1}{N} 2^{\frac{N}{2M}}  \to 0.
\end{align*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: