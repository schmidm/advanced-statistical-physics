\chapter{Process I: Relation to classical thermodynamics}

\section{Reminder on processes in classical thermodynamics}

\subsection{Experiments}

Here we want to present some typical experiments, viz.\ the experiments
shown in figure~\ref{fig:13.5.15-1}.

\begin{figure}[tbh]
  \centering
  \begin{tikzpicture}[gfx]
    \node at (-2,0.5) {(a)};
    \node at (-2,-3) {(b)};
    \node at (-2,-6) {(c)};
    \begin{scope}[shift={(0,0)}]
      \draw [fill opacity=.3,fill, MidnightBlue] (-1.5,0) -- (-1.5,-2) --(0,-2) -- (0,0)  -- cycle;
      \draw [fill opacity=.4,fill, DarkOrange3] (0.5,0) -- (0.5,-2) --(2,-2) -- (2,0)  -- cycle;
      \node at (0.2,-2.3) {$t=0$};
      \draw[->] (2.5,-1) -- (3.5, -1);
      \draw [fill opacity=.2,fill, DarkOrange] (4,0) -- (4,-2) --(7,-2) -- (7,0)  -- cycle;
      \node at (0.2,-2.3) {$t=0$};
      \node at (5.5,-2.3) {$t \gg \tau_{\mathrm{rel}}$};
    \end{scope}
    \begin{scope}[shift={(1,-4)}]
      \draw[->] (1.5,0) -- (2.5, 0);
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-1.5 cm, rnd*-1cm) circle (.25pt);
      \draw [fill,DimGray!75] (-1,0.5) -- (-1,0) --(-0.5,0) -- (-0.5,0.5)  -- cycle;
      \draw (4,1) -- (4,-1) -- (5.5,-1) -- (5.5,1)  (4,0.4) --(5.5,0.4) ;
      \draw (6,0.8) -- (5.5,0.4) -- (6,0);
      % 
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-1.5 cm+5.5cm, rnd*-1.4cm+0.4cm) circle (.25pt);
      \draw [fill,DimGray!75] (4.5,0.9) -- (4.5,0.4) --(5,0.4) -- (5,0.9)  -- cycle;
      \draw (-1.5,1) -- (-1.5,-1) -- (0,-1) -- (0,1)  (-1.5,0) --(0,0) ;
      \draw (0.5,0.4) -- (0,0) -- (0.5,-0.4);
    \end{scope}
    % 
    \begin{scope}[shift={(0.5,-6)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-2cm+1cm, rnd*-1cm) circle (.25pt);
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-1,0) -- (-1,-1) (-1,-0.5) -- (-2.5,-0.5) -- (-2.5,-1.5) -- (-1,-1.5); 
      \draw[decoration={aspect=0.3, segment length=3mm, amplitude=3mm,coil},decorate] (-1,-1.5) -- (1.5,-1.5);
      \draw[->] (1.5,-1.5) -- (2,-1.5) node [above,midway] {$v$}; 
      \draw[->] (2,-0.5) -- (3,-0.5); 
      % 
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-1.5cm+6.5cm, rnd*-1cm) circle (.25pt);
      \draw (4,0) -- (6.5,0) -- (6.5,-1) -- (4,-1)  (5,0) -- (5,-1) (5,-0.5) -- (3.5,-0.5) -- (3.5,-1.5) -- (5,-1.5); 
      \draw[decoration={aspect=0.3, segment length=3mm, amplitude=3mm,coil},decorate] (5,-1.5) -- (7.5,-1.5);
      \draw[->] (7.5,-1.5) -- (8,-1.5) node [above,midway] {$v$}; 
    \end{scope}
  \end{tikzpicture} 
  \caption{Examples for classical thermodynamic experiments. (a) Heat
    exchange. (b) Piston under a constraint without coupled heat
    bath. (c) Piston related to a spring.}
  \label{fig:13.5.15-1}
\end{figure}

\subsection{First and second law}

The \acct{first law} of thermodynamics is
\begin{align}
  \boxed{W = \Delta E + Q}
\end{align}
where $W$ is the work put into a system ($<0$ for work get out),
$\Delta E$ the energy of the system and $Q$ the dissipated heat ($<0$
for heat absorbed by the system). 

The \acct{second law} states, that there is a quantity $\Delta
S_{\mathrm{tot}}\geq 0$ in any process with
\begin{align*}
  \Delta S_{\mathrm{tot}} &= \Delta S_{\mathrm{sys}} + \Delta S_{\mathrm{res},\mathrm{med},\mathrm{bath}},
\end{align*}
where the subscribes of $\Delta
S_{\mathrm{res},\mathrm{med},\mathrm{bath}}$ are ``reservoir'',
``medium'' or ``bath''. Now we have the following problems
\begin{enumerate}
\item Identify these quantities
\item Proof the second law and answer the question whether the second
  law is valid on an individual trajectory or ``only'' on an
  ``ensemble''
\end{enumerate}

\section{Theoretical setting}
\subsection{Three Phases}

\begin{description}
\item[Phase 1:] Initial state: constraint equilibrium. For $t \leq 0$
  \begin{align*}
    \HH_{\mathrm{tot}}^0(\bm{\xi}) = \sum\limits_{i=1}^{n} \HH_i^0(\bm{\xi}_i)
  \end{align*}
  the energy is
  \begin{align*}
    E_{\mathrm{tot}}^0(\bm{\xi}') = \sum\limits_{i=1}^{n} E_i^0 (\bm{\xi}_i^0).
  \end{align*}
\item[Phase 2:] At $t=0^+$ the Hamilton function changes suddenly
  (quench) or smoothly (i.e.\ time dependent)
  \begin{align*}
    \HH_{\mathrm{tot}}(\bm{\xi},t) = \HH_{\mathrm{tot}}^{t} (\bm{\xi}) =  \HH_{1}^{t} (\bm{\xi}_1) + \HH_{2}^{t} (\bm{\xi}_2) + \HH_{12}^{t} (\bm{\xi}_1,\bm{\xi}_2, 
  \end{align*}
  where the energy becomes time independent. This allows us to
  introduce the \acct{work}
  \begin{align*}
    W(\bm{\xi}_0,t) \equiv \HH_{\mathrm{tot}}^t(\bm{\xi}^t) - \HH_{\mathrm{tot}}^0(\bm{\xi}^0),
  \end{align*}
  which describe the energy change of the system. If driving (or
  quench) affects only system 1 and if the interaction is
  infinitesimal small, then
  \begin{align*}
    W(\bm{\xi}^0,t) &= \underbrace{\HH_1^t(\bm{\xi}_1^0) - \HH_1^0(\bm{\xi}_1^0)}_{\Delta E_{\mathrm{sys}(\bm{\xi}_1^t,\bm{\xi}_1^0)}} + \underbrace{ \HH_2(\bm{\xi}_2^t) - \HH_2(\bm{\xi}_2^0)}_{Q(\bm{\xi}_2^t,\bm{\xi}_2^0)} 
  \end{align*}
  since $\bm{\xi}^t=\bm{\xi}^t(\bm{\xi}^0)$ the work is
  \begin{align*}
    W(\bm{\xi}^0,t) &= \Delta E(\bm{\xi}^0,t) + Q(\bm{\xi}^0,t).
  \end{align*}
\item[Phase 3:] Final relaxation. If after time $t^1$, the total Hamilton function becomes time independent then the total energy will remain constant. System approaches the microcanonical distribution with the ``new'' Hamilton function $\HH^1(\bm{\xi}) = \HH^{t_1}(\bm{\xi})$. During this phase the total energy change is zero
  \begin{align*}
    \Delta E_{\mathrm{tot}} &= 0 = \bigg[ \Delta E_1(t) + Q^{(s)}(t)  \bigg].
  \end{align*}
\end{description}

\subsection{New equilibrium and \texorpdfstring{$y$}{y} dependent entropy}

After the time $t^1$ we reached a new equilibrium by applying the old
rules. With constant energy
\begin{align*}
  E_{\mathrm{tot}}^1 (\bm{\xi}^1) &= E_{\mathrm{tot}}^1(\bm{\xi}^1(\bm{\xi}^0))
\end{align*}
we probe to observe $y$. Hence the $y$ dependent entropy change is
\begin{align*}
  \Delta S_{\mathrm{tot}} &= S(y^{(2)}) - S(y^{(1)}) \\
  &= \log \frac{p(y^{(2)})}{p(y^{(1)})},
\end{align*}
which is maximal for $y^{(2)} = \hat{y}$ (the most likely value). For
$y^{(1)}=y^0$, where $y^0$ is the initial value in phase 1, we get
\begin{align}
  \Delta S_{\mathrm{tot}} \equiv \Delta S(\hat{y},y^0) \geq 0,
\end{align}
where $\Delta S$ is the thermodynamic entropy change. Note that this
is just a mathematical fact by construction. 

Splitting of entropy:
\begin{enumerate}
\item If $p(y) = p(y_1) p(y_2)$ factorizes the total entropy change is additive
  \begin{align*}
    \Delta S_{\mathrm{tot}} = \Delta S_1 + \Delta S_2.
  \end{align*}
  Here the subscribes represent the subsystems, superscripts the
  numbering.
\item If the system is in contact with a heat bath the probability of
  observing $y$ is given by
  \begin{align*}
    p(y) &= \ee^{-\beta (F(y)- F}
    \intertext{and the entropy change is}
    \Delta S_{\mathrm{tot}}(y^{(2)},y^{(1)}) &= \log \frac{p(y^{(2)})}{p(y^{(1)})} \\
    &= - \beta \Delta F \\
    &= - \beta \Delta E(y) + \Delta S[p(\xi|y)].
  \end{align*}
  With the most likely value $y^{(2)} = \hat{y}$ we obtain
  \begin{align*}
    \Delta S_{\mathrm{tot}} &= \Delta S_{\mathrm{res}} + \Delta S_{\mathrm{sys}} \geq 0.
  \end{align*}
  With
  \begin{align*}
    \Delta S_{\mathrm{res}} = \beta Q(\hat{y},y^0)
  \end{align*}
  we find the first law along a trajectory by identification
  \begin{align*}
    \beta Q(\hat{y},y^0) = - \beta \Delta E(y).
  \end{align*}
\end{enumerate}

\section{Example: Heat exchange}

Using the example depicted in figure~\ref{fig:13.5.15-1} (a) and
neglect interactions $\HH_{12} \approx 0$ we want to discuss the total
entropy change. The total energy is conserved, i.e.\
$E_{\mathrm{tot}} = E_1 + E_2 = \const$. The initial energies of the
boxes are given by $E_1^0$ and $E_2^0$. The probability to observe the
energy $E_1$ in system 1 is in contact
\begin{align*}
  p(E_1) \propto \Omega_1(E_1) \Omega_2(E_\mathrm{tot}-E_1),
\end{align*}
where $E_\mathrm{tot}-E_1$ describes the energy left in the right
box. The most likely value $\hat{y}$ and $\hat{E}$ leads to
\begin{align*}
   \beta(\hat{E}_1) \stackrel{!}{=} \beta_2(E_{\mathrm{tot}} -\hat{E}_1)
\end{align*}
and confirms $\beta$ as monotonic in naive temperature. The total
entropy change is
\begin{align*}
  \Delta S_{\mathrm{tot}} &= \log \frac{p(\hat{E}_1)}{p(E_1^0)} \\
  &= \log \frac{\Omega_1(\hat{E}_1)}{\Omega_1(E_1^0)} + \log \frac{\Omega_2(E_{\mathrm{tot}} -\hat{E}_1)}{\Omega_2(E_2^0)}
  \intertext{with $\hat{E}_2 = E_{\mathrm{tot}} - \hat{E}_1$ we find}
  &=\int\limits_{E_1^0}^{\hat{E}_1} \diff E_1' \beta(E_1') + \int\limits_{E_2^0}^{\hat{E}_2} \diff E_2' \beta_2(E_2') \geq 0. 
\end{align*}
Note that the temperature is only defined in equilibrium. Here it is
just algebra.

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: