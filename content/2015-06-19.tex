\subsection{Stochastic energetics (Sekimoto 1997)}
Given is box with a fluid. By applying work to a piston the fluid is
compressed where heat is dissipated in the environment (see
figure~\ref{fig:19.6.15-1}).
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,,decoration={random steps,segment length=1.2mm}]
    \begin{scope}[shift={(0,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-2cm+1cm, rnd*-1cm) circle (.25pt);
      \node at (0,0.25) {$T$};
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-1,0) -- (-1,-1) (-2,-0.5)--(-1,-0.5); 
      \draw [->] (1.5,-0.5)-- node [above,midway] {$W$}(2.5,-0.5);
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-1cm+1cm, rnd*-1cm) circle (.25pt);
      \node at (0.5,0.25) {$T$};
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-0,0) -- (-0,-1) (-1.5,-0.5)--(-0,-0.5); 
    \end{scope}
  \end{tikzpicture} 
  \caption{Classical system. A box is filled with a fluid. The whole
    system has the temperature $T$. After appying work with a piston
    the fluid is compressed and heat is dissipated into the
    environment.}
  \label{fig:19.6.15-1}
\end{figure}
According to the first law the work is
\begin{align*}
  E &= \Delta E + Q,
\end{align*}
where $Q$ is the dissipated heat and $\Delta E$ the energy
change. Bellow we want to find an expression of the first law for a
single particle in a potential. In the following we deal with a
particle in a time dependent potential $V(x,t)$ as depicted in
figure~\ref{fig:19.6.15-2}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =-2, xmax = 2, 
	ymin = 0, ymax =4,
	xtick = {0.75}, 
	xticklabel = {$x_0$},
	ytick=\empty,
    	ylabel = {$V(x,t_0)$},
        xlabel = $x$,
	width = 5cm,
        xmajorgrids = true
        ]
 	\node[circle,fill,inner sep=1.25pt] at (axis cs: 0.75,0.5625) {};
	\addplot [domain=-2:2,MidnightBlue,samples=35,smooth]  {1*x^2};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin =-2, xmax = 2, 
	ymin = 0, ymax =4,
	xtick = {0.75}, 
	xticklabel = {$x_0$},
	ytick=\empty,
    	ylabel = {$V(x,t_1)$},
        xlabel = $x$,
	width = 5cm,
        xmajorgrids = true
        ]
 	\node[circle,fill,inner sep=1.25pt] at (axis cs: 0.75,1.6875) {};
	\addplot [domain=-2:2,MidnightBlue,samples=35,smooth]  {3*x^2};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Particle in a harmonic potential. (a) At time $t_0$ the
    particle has a certain energy in the non-squeezed potential. (b)
    At a certain time $t_1$ we squeeze the parabola and the particle
    will gain energy.}
  \label{fig:19.6.15-2}
\end{figure}
The Langevin equation for the system is
\begin{align*}
  \dot{x} &= \mu \left[ - \frac{\partial V(x,t)}{\partial x} \right] + \xi,
\end{align*}
with 
\begin{align*}
  \braket{\xi(t_1)\xi(t_2)} = 2 \mu \kB T \delta(t_2-t_1).
\end{align*}
The first law states
\begin{align*}
  \delta W = \diff E + \delta Q.
\end{align*}
Sekimotos idea was to identify the energy over the potential
\begin{align*}
  \diff E \equiv \diff V.
\end{align*}
Hence we can rewrite the first law
\begin{align*}
  \delta W &= \diff V + \delta Q \\
  &= \frac{\partial V}{\partial x} \diff x + \frac{\partial V}{\partial t} \diff t + \delta Q.
\end{align*}
To identify the work we have to identify the heat, which is
\begin{align*}
  \delta Q &= - \frac{\partial V}{\partial x} \diff x
\end{align*}
and the work 
\begin{align*}
  \delta W &= \frac{\partial V}{\partial t} \diff t.
\end{align*}
Therefore we find
\begin{align*}
  \frac{\partial V}{\partial t} \diff t &= \frac{\partial V}{\partial x} \diff x + \frac{\partial V}{\partial t} \diff t - \frac{\partial V}{\partial x} \diff x.
\end{align*}
Furthermore we find
\begin{align*}
  \delta Q &= - \frac{\partial V}{\partial x} \diff x \\
  &= F \diff x,
\end{align*}
where $F$ is a force. Hence we can rewrite the Langevin equation
\begin{align*}
  \delta Q &= \frac{\dot{x} - \xi}{\mu} \diff x.
\end{align*}
The time derivative leads to
\begin{align*}
  \dot{q} &= F \dot{x} \quad \implies \quad Q = \int\limits_{t_0}^{t_1} \left( -\frac{\partial V}{\partial x} \right) \dot{x} \diff x, \\
  \dot{w} &= \frac{\partial V}{\partial t} \quad \implies \quad W = \int\limits_{t_0}^{t_1} \frac{\partial V}{\partial t} \diff t, \\
  \dot{E} &= \frac{\diff V}{\diff t}  \quad\implies\quad \Delta E = V(x(t_1),t_1) - V(x(t_0),t_0).
\end{align*}

Experiments in \cite{seife06} showed that the work must be a
functional of all trajectories, i.e.\ $W =W[x(t)]$. The average is
then given by
\begin{align*}
  \overline{W} &= \sum\limits_{x(t)} W[x(t)] p[x(t)],
\end{align*}
where $p[x(t)]$ is given by a path integral. Note that due to the
``shoulder'' in the probability distribution as function of $W$ in
\cite{seife06} we are beyond the linear response theory.

\subsection{Path integral with time dependent potential}

Given is the Langevin equation
\begin{align*}
  \dot{x} &= \mu \left(-\frac{\partial V}{\partial x} \right) + \xi.
\end{align*}
Using the same calculation as in previous sections we find
\begin{align*}
  p[x(\tau)|x_0] \propto \exp\left[- \frac{1}{4D} \int\limits_{0}^{t} \diff \tau \left( \dot{x} + \mu \frac{\partial V}{\partial x}  \right)^2 \right] \underbrace{ \exp\left[ \frac{\mu}{2} \int\limits_{0}^{t} \diff \tau  V''(x(\tau),\tau) \right]}_{\text{Jacobian}}.
\end{align*}
The probability to require a certain work $W$ is then
\begin{align*}
  p(W) &= \sum\limits_{\text{all paths}} p[x(\tau)|x_0] \delta(W[x(\tau)]-W)
\end{align*}
which is a \acct{path integral} and the work distribution, respectively. 

\subsection{Time reversal}

Given is potential $V(x,\lambda(\tau))$, where $\lambda(\tau)$ is a control parameter and the path $x(\tau)$. Time reversal means
\begin{align*}
  \tau \to t-\tau
\end{align*}
and for the path
\begin{align*}
  x(\tau) \to \tilde{x}(\tau) \equiv x(t-\tau).
\end{align*}
Here we have to reverse the control parameter too
\begin{align*}
  \lambda(\tau) \to \tilde{\lambda}(\tau) \equiv \lambda(t-\tau).
\end{align*}
Hence we can write
\begin{align*}
  \dot{\tilde{x}}(\tau) = - \dot{x}(t-\tau).
\end{align*}
The \acct{stochastic action} under time-reversal is then given by
\begin{align*}
  S[x(\tau)] &= \frac{1}{4 D} \int\limits_{0}^{t} \diff \tau \left( \dot{x} + \mu V'(x,\lambda) \right)^2 +  \frac{\mu}{2} \int\limits_{0}^{t} V''(x) \diff \tau \\
  &= \frac{1}{4 D} \int\limits_{0}^{t} \diff \tau \left[ \dot{x}^2 + 2 \mu \dot{x} \frac{\partial V}{\partial x} + \mu^2 {V'}^2+ 2 \mu D V'' \right], \\
  S[\tilde{x}(\tau)] &= \frac{1}{4 D} \int\limits_{0}^{t} \diff \tau \left[ \dot{\tilde{x}}^2 + 2 \mu \dot{\tilde{x}} \frac{\partial \tilde{V}}{\partial x} + \mu^2 \tilde{V}'^2+ 2 \mu D V'' \right]. \\
\end{align*}
The following terms are identical
\begin{align*}
  \dot{x}^2 = \dot{\tilde{x}}^2, \quad
  {V'}^2 = \tilde{V}'^2, \quad
  V''= \tilde{V}'',
\end{align*}
so we can write
\begin{align*}
  S[\tilde{x}(\tau)] &= S[x(\tau)] - \frac{1}{\kB T} \int\limits \dot{x} \frac{\partial V}{\partial x} \diff \tau,
\intertext{where the right part contains the dissipated heat}
  &= S[x(\tau)] + \frac{Q}{\kB T}.
\end{align*}
The probability to observe the time reversal trajectory by given
$\tilde{x}_0$ divided by the probability to observe the trajectory by
given $x_0$ is
\begin{align*}
  \frac{p[\tilde{x}(\tau)|\tilde{x}_0]}{p[{x}(\tau)|{x}_0]} &= \ee^{- \left( S[\tilde{x}] - S[x]\right)} = \ee^{- \frac{Q[x(\tau)]}{\kB T}}.
\end{align*}

\subsection{Derivation of the Jarzynski relation}
Given is the initial potential $V(x,\lambda_0)$. In equilibrium the
probability is given by
\begin{align*}
  p(x_0) &= \ee^{-\beta V(x_0,\lambda_0) + \beta F(\lambda_0)}.
\end{align*}
As in the previous section we assume that the control parameter
changes to $\lambda_1$ so the final potential will be
$V(x,\lambda_1)$. To derive the Jarzynski relation we start with the
normalization respectively the conservation of the probability and a
bit ``black magic''
\begin{align*}
  1 &= \sum\limits_{\tilde{x}_0} \sum\limits_{\tilde{x}(\tau)} p[\tilde{x}(\tau)|\tilde{x}_0] p_1(\tilde{x}_0),
  \intertext{where $p_1[\tilde{x}_0]$ is a normalized probability. Continuing with  the calculation leads to}
  &= \sum\limits_{\tilde{x}_0} \sum\limits_{\tilde{x}(\tau)} \frac{p[\tilde{x}(\tau)|\tilde{x}_0]}{p[x(\tau)|x_0]} p[x(\tau)|x_0]p_1(\tilde{x}_0) \\
  &= \sum\limits_{\tilde{x}_0} \sum\limits_{\tilde{x}(\tau)} \ee^{-\frac{1}{\kB T} Q[x(\tau),x_0]} p[x(\tau)|x_0] \frac{p_0(x_0)}{p_0(x_0)} p_1(\tilde{x}_0)
\intertext{because summing over all backward paths is equal to summing over all normal paths we can rewrite the two sums}
  &= \sum\limits_{x_0} \sum\limits_{x(\tau)} \ee^{-\frac{1}{\kB T} \left( W[x(\tau)|x_0] -(V(x(\tau),\lambda_1) - V(x(\tau),\lambda_0))  \right)} p[x(\tau)|x_0] \frac{p_0(x_0)}{p_0(x_0)} p_1(x_0).
\end{align*}
Using
\begin{align*}
  p_1(\tilde{x}_0) &\equiv \ee^{- \beta V(\tilde{x}_0,\lambda_1) + \beta F(\lambda_1)} \\
  &= \ee^{ -\beta V(x_1,\lambda_1) + \beta F(\lambda_1)}
\end{align*}
and $p_0(x)$ we find for the equation above
\begin{align*}
  1 &= \sum\limits_{x_0,x(\tau)} \ee^{ -\beta W +\beta \Delta F} p_0(x_0) p[x(\tau)|x_0] \\
  &=\Braket{\ee^{ -\beta W + \beta \Delta F}},
\end{align*}
which is the \acct{Jarzynski relation}.


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: