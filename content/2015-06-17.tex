\section{Path Integral} \index{Path integral}

\subsection{The Question}

Given is a particle in the gravitation field of the earth in a fluid
as depicted in figure~\ref{fig:17.6.15-1} (a). In equilibrium the
ratio of the probabilities of finding the particle at the height $z_1$
and $z_2$ is given by the Boltzmann distribution
\begin{align*}
  \frac{p_{eq}(z_2)}{p_{eq}(z_1)} = \ee^{-\beta (z_2 -z_1)}.
\end{align*}
Now we are interested in which path of the brownian particle leading
from $z_1$ to $z_2$ is more probable. Therefore we deal with a
situation as depicted in figure~\ref{fig:17.6.15-1} (b).

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,rnd/.style={
      postaction={
        draw,decorate,
        decoration={
          random steps, segment length = 1pt, amplitude = 1pt
          % saw,segment length = 2pt, amplitude = 2pt
        }
      }
    }]
    \begin{scope}[shift={(0,0)},xscale=.7]
      \node at (0,2.5) {(a)};
      \draw[->] (-0.5,1.5) -- (3,1.5) node [above] {$t$};
      \draw[->] (0,-1) --(0,1) node [left] {$z$};
      \draw[dashed] (1,1.5)--(1,-0.5);
      \draw[dashed] (2.5,1.5)--(2.5,0.5);
      \draw[dashed] (0,0.5) node [left] {$z_2$} --(2.5,0.5) ;
      \draw[dashed] (0,-0.5) node [left] {$z_1$} --(1,-0.5);
      \node[dot] at (1,-0.5) {};
      \node[dot] at (2.5,0.5) {};
    \end{scope}
    \begin{scope}[shift={(4,0)},xscale=.3,samples=10]
      \node at (-3*pi/6,2.5) {(b)};
      \draw[<->] (-3*pi/6,1.5) node[left] {$z$} |- (31*pi/6,-1) node [below] {$t$};
      \draw[dashed] (-3*pi/6,0.5)  node[left] {$z_2$}  -- (31*pi/6,0.5);
      \draw[dashed] (-3*pi/6,-0.5)  node[left] {$z_1$}  -- (31*pi/6,-0.5);
      \path[domain=-3*pi/6:-pi/6,MidnightBlue] plot ({\x},{sin(deg(\x))}) [rnd];
      \path[domain=-pi/6:pi/6,DarkOrange3] plot ({\x},{sin(deg(\x))}) [rnd];
      \path[domain=pi/6:7*pi/6,MidnightBlue] plot ({\x},{sin(deg(\x))}) [rnd];
      \path[domain=7*pi/6:13*pi/6,DarkOrange3] plot ({\x},{sin(deg(\x))}) [rnd];
      \path[domain=13*pi/6:31*pi/6,MidnightBlue] plot ({\x},{sin(deg(\x))}) [rnd];
      \node[DarkOrange3] at (0,-0.3) {$A$};
      \node[DarkOrange3] at (10*pi/6,-0.3) {$B$};
      \draw[decorate,decoration=brace] (13*pi/6,-1.1) -- node[below] {$\Delta t$} (7*pi/6,-1.1);
    \end{scope}
  \end{tikzpicture}
  \caption{(a) Particle in a fluid with gravitation field. (b)
    Possible path from $z_1$ to $z_2$ for a brownian particle. Path
    $A$ is here more probable than path $B$. }
  \label{fig:17.6.15-1}
\end{figure}

\subsection{Free diffusion}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = -1, xmax = 10, 
      ymin = -0.5, ymax =2,
      xtick=\empty, 
      ytick=\empty,
      xlabel = time,
      ylabel = $x$,
      width = 5cm,
      xtick = {0,1,2,3,4,5,6,7,8,9},
      xticklabels = {$0$, $1$, $2$, $3$, $4$, $5$, $$, $N$},
      xmajorgrids = true
      ]
      \draw [help lines] (axis cs: -1,0) -- (axis cs: 10,0);
      \draw [help lines] (axis cs: 0,-0.5) -- (axis cs: 0,2);
      \draw [MidnightBlue] (axis cs: 0,1) -- (axis cs: 1,1.5) -- (axis cs: 2,1.25)  -- (axis cs: 3,1.75)  -- (axis cs: 4,1.5)  -- (axis cs: 5,1)  -- (axis cs: 6,1.25)  -- (axis cs: 7,1);
      \draw [DarkOrange3] (axis cs: 0,1) -- (axis cs: 1,0.5) -- (axis cs: 2,0.75)  -- (axis cs: 3,1.25)  -- (axis cs: 4,0.75)  -- (axis cs: 5,0.75)  -- (axis cs: 6,1)  -- (axis cs: 7,1);
    \end{axis}
  \end{tikzpicture} 
  \caption{Discrete system with two differnt paths. The orange one is
    more likely than the blue one.}
  \label{fig:17.6.15-2}
\end{figure}

We start with a discrete description of a system. For this system
different paths are shown in figure~\ref{fig:17.6.15-2}. To answer the
question whether the blue or the orange trajectory is more likely we
use the discrete equation of motion which is given by
\begin{align}
  x_i = x_{i-1} + \varepsilon \xi_i \quad,\quad i =1,\ldots,N \label{eq:17.6.15-1}.
\end{align}
Here $x_i$
describes the position and $\xi_i$
the noise respectively the stochastic force which fulfills
\begin{align*}
  \braket{\xi_i} &= 0,\\
  \braket{\xi_i,\xi_j} &= \frac{2 D}{\varepsilon} \delta_{ij}.
\end{align*}
Hence we deal with Gaussian white noise. The probability of observing
$\xi_1$ is a Gaussian distribution
\begin{align*}
  p(\xi_1) &= \left[\frac{\varepsilon}{ 4 \pi D} \right]^{1/2} \exp\left[ -\frac{\varepsilon \xi_1^2}{4 D} \right].
\end{align*}
Because we are now interested in all possible paths the probability
\begin{align*}
  p(\xi_1,\ldots,\xi_N) &= \left[\frac{\varepsilon}{4 \pi D}\right]^{N/2} \exp\left[-\frac{\varepsilon}{4 D} \sum\limits_{i=1}^{N} \xi_i^2 \right],
\end{align*}
which is a joint probability. A coordinate transfomation allows us to
express tehis probability in terms of the descrete positions
\begin{align*}
  p(x_1,\ldots,x_N|x_0) &= \left[\frac{\varepsilon}{4 \pi D}\right]^{N/2} \exp\left[-\frac{\varepsilon}{4 D} \sum\limits_{i=1}^{N} \left( \frac{x_i-x_{i-1}}{\varepsilon}\right)^2 \right] \cdot \left| \frac{\partial \xi_i}{\partial x_j} \right|,
\end{align*}
where we used equation~\ref{eq:17.6.15-1}. The last term is the
Jacobian which is inevitable for a coordinate transformation. The
Jacobian is given by
\begin{align*}
  \frac{\partial \xi_i}{\partial x_j} &=
  \begin{pmatrix}
    \frac{\partial \xi_1}{\partial x_1} & \frac{\partial \xi_1}{\partial x_2} & \frac{\partial \xi_1}{\partial x_3} & \ldots \\
    \frac{\partial \xi_2}{\partial x_1} & \frac{\partial \xi_2}{\partial x_2} & \frac{\partial \xi_2}{\partial x_3} & \ldots \\
    \vdots & \vdots & \vdots & \ddots 
  \end{pmatrix} \\
  &=
    \begin{pmatrix}
      \frac{1}{\varepsilon} & 0 & 0 & 0 & \ldots \\
      \bullet & \frac{1}{\varepsilon} & 0 & 0 & \ldots \\
      \vdots & \vdots & \vdots & \vdots & \ddots 
    \end{pmatrix}.
\end{align*}
Obviously we deal with a lower triangular matrix where only the
diagonal entries are important for the calculation of the
determinat. Therefore we find
\begin{align*}
  \det\left( \frac{\partial \xi_i}{\partial x_j} \right) = \frac{1}{\varepsilon^N}.
\end{align*}
Hence the whole probability of all paths is 
\begin{align*}
  p(x_1,\ldots,x_N|x_0) &= \left[ \frac{\varepsilon}{4 \pi D}\right]^{N/2} \exp\left[-\frac{\varepsilon}{4 D} \sum\limits_{i=1}^{N} \left( \frac{x_i - x_{i-1}}{\varepsilon} \right)^2 \right] \frac{1}{\varepsilon^N}.
\end{align*}
Fot the transition form the descrete descitprion to the continuous
descritpion related with the continuum limit where we look at the
limits $\varepsilon \to 0$ and $N \to \infty$. Consequently, we have
to obey the following substitutions
\begin{align*}
  i &\to \tau = i \varepsilon, 
  &\sum\limits_{i=1}^{N} &\to \frac{1}{\varepsilon} \int\limits_0^{N \varepsilon}, \\
  \frac{x_i - x_{i-1}}{\varepsilon} &\to \dot{x},
  &\xi_i &\to \xi(\tau), \\
  \frac{1}{\varepsilon} \delta_{ij} &\to \delta(\tau-\tau').
\end{align*}
The result of our calculation leads to the Langevin equation for the
free diffusion
\begin{align*}
  \dot{x} &= \xi(t),
\end{align*}
with the stochastic force $\xi(t)$ which fulfills
\begin{align*}
  \braket{\xi(t)} &= 0, \\
  \braket{\xi(t) \xi(t')} &= \delta(t-t'). 
\end{align*}
Moreover we find
\begin{align*}
  p(\xi_1,\ldots,\xi_N) \to p[\xi(t)] = \lim_{\substack{\varepsilon\to0\\N\to\infty\\N\cdot\varepsilon=t}}\left[ \frac{1}{4 \pi D \varepsilon} \right]^{N/2} \exp\left[ - \frac{1}{4 D} \int\limits_{0}^{t} \diff\tau \delta(\tau- \tau')^2 \right]. 
\end{align*}
This allows us to find a weight for a each path
\begin{align*}
  p[x(\tau)|x_0] = \lim_{\substack{\varepsilon\to0\\N\to\infty\\N\cdot\varepsilon=t}}\left[ \frac{1}{4 \pi D \varepsilon} \right]^{N/2} \exp\left[ - \frac{1}{4 D} \int\limits_{0}^{t} \dot{x}^2(\tau) \diff\tau  \right],
\end{align*}
which is very nice. The variation principle, more precisely Hamilton's
principle, then leads to the most likely path. As known form the
classical mechanics lecture the Euler-Lagrange equation leads to the
most likely path and the equation of motion
\begin{align*}
  m \ddot{x} &= 0 \implies \dot{x} = \const
\end{align*}
for this trajectory. As we can see the most likely value is given by
constant velocity. In the case of a brownian particle the most
probable path is staying at the same position. Of cource we will not
observe this trajectory.

\begin{notice}[Reminder on classical mechanics:]
  Given is a one dimensional particle in a potential $V(x)$. The
  action is given by
  \begin{align*}
    S[x(\tau)] &= \int\limits_{0}^{t} \diff\tau \left( \frac{m}{2} \dot{x}^2 - V(x) \right).
  \end{align*}
  We obtain the classical path from minimizing the action
  $S[x(\tau)]$, i.e.\
  \begin{align*}
    \frac{\partial S[x(\tau)]}{ \partial x(\tau)}\bigg|_{x(\tau) = x_{\mathrm{cl}}(\tau)} &= 0. 
  \end{align*}
  The solution obeys the Euler-Lagrange equations which leads to the
  equation of motion
  \begin{align*}
    m \ddot{x} + \frac{\partial V(x)}{\partial x} = 0.
  \end{align*}
  If $V(x) = 0$ we find
  \begin{align*}
    m \ddot{x} &= 0 \\
    \implies \dot{x} &= \const
  \end{align*}
\end{notice}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: