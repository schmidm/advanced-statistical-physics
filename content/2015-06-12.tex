\subsection{Calculation of the MFPT}
Given is a system as demonstrated in figure~{\ref{fig:10.6.15-3}}. We
are just n interested in the first passage time $\mathcal{T}$. Hence
we use absorbing boundary conditions to get rid of further
passages. The first passage is at $x=b$. We introduce the function
\begin{align*}
  G(x,t) \equiv \prob[\mathcal{T}<t] = \int\limits_{a}^{b} \diff x' p(x',t'|x,0),
\end{align*}
which is something like the cumulated probability. To obtain the probability $p(\mathcal{T},x_0)$ we have to differentiate to $t$
\begin{align*}
  p(\mathcal{T},x_0) = - \partial_t G(x,t)\big|_{t=\mathcal{T}}.
\end{align*}
We draw the initial condition
\begin{align*}
  G(x,0) = 1.
\end{align*}
The evolution is then given by
\begin{align*}
  \partial_t G(x,t) &= \int\limits_{a}^{b} \diff x' \partial_t p(x',t|x,0) \\
  &= \int\limits_{a}^{b} \diff x' L^\dagger(x)p(x',t|x,0) \\
  &= L^\dagger(x) G(x,t).
\end{align*}
The boundary conditions are then (example):
\begin{itemize}
\item Reflecting at $a$: $\partial_x G(x,t)\big|_{x=a} = 0$ 
\item Absorption at $b$: $G(x,t) = 0$
\end{itemize}
In the long time limit the $G(x,t)$-function vanishes
\begin{align*}
  \lim\limits_{t \to \infty} G(x,t) = 0.
\end{align*}
The \acct{mean first passage time} is then defines as
\begin{align*}
  \mathcal{T}_1(x) &\equiv \int\limits_{0}^{\infty} \diff \mathcal{T} \mathcal{T} p(\mathcal{T},x) \\
  &= - \int\limits_{0}^{\infty} \diff t t \partial_t G(x,t) \\
  &= \int\limits_{0}^{\infty} G(x,t) - \lim\limits_{t \to \infty} t G(x,t)
  \intertext{we can show that the $G(x,t)$-function has an exponential decay $G(x,t) \propto \exp(-\alpha t)$. Hence the limit vanishes and we find}
  &= \int\limits_{0}^{\infty} \diff t G(x,t).
\end{align*}
Using the backward FPG operator leads to
\begin{align*}
  L^\dagger(x) \mathcal{T}_1(x) &= \int\limits_{0}^{\infty} \diff t L^\dagger(x) G(x,t) \\
  &= \int\limits_{0}^{\infty} \diff t \partial_t G(x,t) \\
  &= G(x,t\to\infty) - G(x,0) = -1.
\end{align*}
Hence we find the result
\begin{align*}
  \boxed{ L^\dagger(x) \mathcal{T}_1(x) = -1}
\end{align*}
with absorbing boundary condition $\mathcal{T}(b) = 0$ and reflecting
boundary condition $\partial_x \mathcal{T}_1(x)\big|_a$ = 0.

Higher order moments $\mathcal{T}_n$ are defined as
\begin{align*}
  \mathcal{T}_n(x) &\equiv\int\limits_{0}^{\infty} \diff \mathcal{T} \mathcal{T}^n p(\mathcal{T},x) \\
  &= - \int\limits_{0}^{\infty} \diff t t^n \partial_t G(x,t) \\
  &= n \int\limits_{0}^{\infty} \diff t t^{n-1} G(x,t).
\end{align*}
Using again the backward FPG operator leads to
\begin{align*}
  L^\dagger(x) \mathcal{T}_n(x) &= n \int\limits_{0}^{\infty} \diff t t^{n-1} \underbrace{L^\dagger(x) G(x,t)}_{\partial_t G(x,t},
\end{align*}
which gives the recursion formula
\begin{align*}
  \boxed{L^\dagger(x) \mathcal{T}_n(x) = - n \mathcal{T}_{n-1}(x)}
\end{align*}

\subsection{Example}

Given is the potential depicted in figure~\ref{fig:10.6.15-3} (a). We
are interested in the time when the particle reached a special
position. From the sections above we know that the evolution of the
$G(x,t)$ is given by
\begin{align*}
  L^\dagger(x) G(x,t) = -1,
\end{align*}
where the backward FPG operator is
\begin{align*}
  L^\dagger(x) = \mu \left[ -V'(x) + T \partial_x  \right] \partial_x.
\end{align*}
Hence we can write
\begin{align*}
  \mu \left[ -V'(x) + T \partial_x  \right] \underbrace{\partial_x G(x,t)}_{\equiv \psi(x)} = -1.
\end{align*}
The solution for a reflecting boundary conditions at $x=1$ and a
absorbing one at $x=b$ leads to the MFPT
\begin{align*}
  \mathcal{T}_1 = \frac{1}{D} \int\limits_{x}^{b} \diff y \int\limits_{a}^{y} \diff z \exp\left[ \beta (V(y) - V(z)) \right].
\end{align*}

\minisec{Escape over a potential barrier}

Let $a\to\infty$ and $\beta \gg 1/\Delta V$. Hence the MFPT takes on
the form
\begin{align*}
  \mathcal{T}_1(x) = \frac{1}{D} \int\limits_{x}^{b} \diff y \exp[\beta V(y)] \int\limits_{-\infty}^{y} \diff z \exp[-\beta V(z)].
\end{align*}
Only one of the two integrals is varying at a time. The single
integrals are depicted in figure~\ref{fig:12.6.15-1}. 
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (1.75,3.2) {(a)};
    \node at (7.25,3.2|-a) {(b)};
    \pgfplotstableread{content/schmid1.dat}{\sch}
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = -2, xmax = 2,
        ymin = -1.75, ymax = 1.25,
        domain=-2:2,
        xlabel ={ $x,z,y$ },
        ylabel = {$V(x)$},
        ytick=\empty,
        width=5cm,
        xtick = {-0.95,-0.1, 1.05},
        xticklabels ={ $x_1$,$x_0$,$x_2$},
        xmajorgrids = true
        ]
        \addplot [smooth,samples=50] {-2*x^2+1*x^4-0.4*x};
        \addplot [MidnightBlue,smooth,samples=50] {0.3*exp(-(-2*x^2+1*x^4-0.4*x))-1.75};
        \addplot [DarkOrange3,smooth,samples=50,domain=-1.65:1.75] {2*exp((-2*x^2+1*x^4-0.4*x))-2.25};
	 \node at (axis cs: -1.1,0.75) {\circled{1}};
	 \node at (axis cs: 1.1,0.75) {\circled{2}};
         \addplot [Purple] table [x={time}, y={pos1}] {\sch};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5.5,0)}]
      \begin{axis}[
        xmin = -1.75, xmax = 1.75,
        ymin = -1.75, ymax = 0,
        domain= -1.:1.5,
        xlabel = $x$,
        ylabel = {$\mathcal{T}_1(x,b)$},
        xtick = {-0.95,-0.1, 1.05},
        xticklabels ={ $x_1$,$x_0$,$x_2$},
        width=5cm,
        ytick=\empty,
        xmajorgrids = true
        ]
	\draw (axis cs: -0.5,-1.75) .. controls (axis cs: -0.1,-1.75) and (axis cs: 0.1,-1) .. (axis cs: 0.4,-1) .. controls (axis cs: 1,-1) and (axis cs: 1.7,-1) .. (axis cs: 1.75,1.5);
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{(a) The left part of the local maximum of the potential
    V(x) is called part \circled{1} and the right part
    \circled{2}. The blue curve corresponds to $\exp[-\beta V(z)]$ and
    the orange curve to $\exp[\beta V(y)]$. The purple curve shows the
    integrated red curve. (b) MFPT $\mathcal{T}_1$.}
  \label{fig:12.6.15-1}
\end{figure}
We start at $x \approx x_1$ and end at $b \approx x_2$. For $y \approx
x_0$ we find
\begin{align*}
  \int\limits_{-\infty}^{y} \diff z \exp[-\beta V(z)] &\stackrel{\text{S.P.}}{\approx} \int\limits_{-\infty}^{\infty} \diff z \exp\left[ -\beta V(x) - \frac{1}{2} \beta V''(x_1)(z-x_1)^2 \right] \\
  &= \exp[-\beta V] \underbrace{\left[\frac{2 \pi}{\beta V''(x_1)} \right]^{1/2}}_{\equiv c},
\end{align*}
where the constant $c$ is the plateau value. The MFPT is the given by
\begin{align*}
  \mathcal{T}_1(x) &\approx \frac{c}{D} \int\limits_{-\infty}^{\infty} \exp\left[ \beta V(x_0) + \frac{1}{2} \beta V''(x_0)(y-x_0)^2  \right] \diff y \\
  &= \frac{c}{D} \exp[\beta V(x_0)] \left[ \frac{2\pi}{\beta |V''(x_0)|} \right]^{1/2}\\
  &= \frac{1}{D} \frac{2\pi}{\beta \sqrt{ V''(x_1) |V''(x_0)|}} \exp\left[ -\beta (V(x_0) - V(x_1)) \right] \\
  &\propto \exp[\beta \Delta V].
\end{align*}
The result is depicted in figure~\ref{fig:12.6.15-1} (b). Here we have
two mesostates. One mesostate \circled{1} with $x<x_0$ and \circled{2}
with $x>x_0$. The transition rate from $\circled{1} \to \circled{2}$
is then given by
\begin{align*}
  K_{12} = \frac{1}{ \mathcal{T}_1(x,b)} = \underbrace{\frac{D \beta }{2 \pi} \sqrt{V''(x_0) |V''(x_1)|}}_{(a)} \underbrace{\exp\left[ \beta (V(x_0) - V(x_1))  \right]}_{(b)},
\end{align*}
which is known as \acct{Arrhenius law}. The factor (a) is called
``attempt rate'' and the factor (b) is the Boltzmann
factor. Analogously we find for $\circled{2} \to \circled{1}$
\begin{align*}
  K_{21} &= \frac{D \beta }{2 \pi} \sqrt{|V''(x_0)| V''(x_2)} \exp\left[ \beta (V(x_0) - V(x_2))  \right].
\end{align*}
Given are the probabilities of the occupying mesostates $p_1(t)$ and
$p_2(t)$. We can then find the \acct{master equation}
\begin{align*}
  -\partial_t p_1(t) &= - K_{12} p_1(t) + K_{21} p_2(t),\\
  \partial_t p_2(t) &= K_{12} p_1(t) - K_{21} p_2(t).
\end{align*}
In the vector matrix notation we find
\begin{align*}
  \boxed{\partial_t \bm{p} = \mathbb{W} \bm{p}}
\end{align*}
with
\begin{align*}
  \bm{p} =
  \begin{pmatrix}
    p_2 \\ p_2
  \end{pmatrix} \quad, \quad 
  \mathbb{W} =
  \begin{pmatrix}
    -K_{12} & K_{21} \\
    K_{12} & -K_{21} 
  \end{pmatrix}.
\end{align*}
The stationary state is given by
\begin{align*}
  p_1^s = \frac{K_{21}}{K_{12} + K_{21}} \quad , \quad p_2^s = \frac{K_{12}}{K_{12} + K_{21}}.
\end{align*}
Thereby we can calculate the \acct{detailed balance ratio}
\begin{align*}
  \frac{p_1^s}{p_2^s} &= \frac{K_{21}}{K_{12}} = \sqrt{\frac{V''(x_1)}{V''(x_2)}} \exp\left[ \beta (V(x_1)- V(x_2)) \right] \approx \exp[-\beta \Delta F],
\end{align*}
with the free energy of a mesostate
\begin{align*}
  F_1 &\equiv - \frac{1}{\beta} \log\left[\int_{-\infty}^{x_0} \diff x \exp[-\beta V(x)] \right] \\
  &\stackrel{\text{S.P.}}{\approx} - \frac{1}{\beta} \log\left[ \exp[-\beta V(x_1)] \left(\frac{2\pi}{\beta V''(x_1)}\right)^{1/2} \right], \\ 
    F_2  &\approx - \frac{1}{\beta} \log\left[ \exp[-\beta V(x_2)] \left(\frac{2\pi}{\beta V''(x_2)}\right)^{1/2} \right].
\end{align*}


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: