\section{Fokker-Planck equation for diffusion}
\subsection{Langevin equation for pure diffusion}
The Langevin equation for pure diffusion contains no damping force
caused by friction. Hence one gets
\begin{align*}
  \dot{x} &= \zeta(t),
\end{align*}
where $\zeta(t)$ is the stochastic force which fulfills
\begin{align*}
  \braket{\zeta(t)} &= 0,\\
  \braket{\zeta(t)\zeta(t')} &= 2 D \delta(t-t').
\end{align*}
This description corresponds to a \acct{random walk}. The solution
(trajectory) with the initial condition $x(0) = 0$ is then given by
\begin{align*}
  x(t) &= \int\limits_{0}^{t} \diff t' \zeta(t') + x(0) \\
  &= \int\limits_{0}^{t} \diff t' \zeta(t') \\
  \implies \braket{x(t)} &=  \int\limits_{0}^{t} \diff t'\braket{ \zeta(t')} = 0.
\end{align*}
The squared average of $x$ is 
\begin{align*}
  \braket{x^2(t)} &= \int\limits_{0}^{t} \diff t' \int\limits_{0}^{t} \diff t'' \braket{\zeta(t')\zeta(t'')} \\
  &=  \int\limits_{0}^{t} \diff t' \int\limits_{0}^{t} \diff t'' 2 D \delta(t'-t'') \\
  &= 2Dt,
\end{align*}
which corresponds to the diffusion in one dimension.

\subsection{Derivation of the Fokker-Planck equation for pure diffusion}

Given is a probability distribution $p(x,t)$ an the initial condition
$x(0)=0$. The initial distribution then corresponds to a delta
function $\delta(x)$, i.e.\ all particles are at the time $t=0$ at the
same position. Hence we are interested on the behavior of an ensemble
of particles. In the case of diffusion the time evolution of the
probability density is depicted in figure~\ref{fig:2015-06-03-1}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = -2, xmax = 2,
      ymin = 0, ymax = 4,
      domain=-2:2,
      xtick=\empty, ytick=\empty,
      width=6cm
      ]
      \addplot [MidnightBlue,smooth,samples=50] {1/(sqrt(0.1))*exp(-x^2/0.1)};
      \addplot [DarkOrange3,smooth,samples=50] {1/(sqrt(0.3))*exp(-x^2/0.3)};
      \addplot [Purple,smooth,samples=50] {1/(sqrt(1))*exp(-x^2/1)};
      \draw [help lines] (axis cs: 0,0) -- (axis cs: 0,4);
      \node[label={[MidnightBlue]-10:{$p(x,t_1)$}}] at (axis cs: 0.5,2.75) {};
      \node[label={[DarkOrange3]-10:{$p(x,t_2)$}}] at (axis cs: 0.5,2) {};
      \node[label={[Purple]-10:{$p(x,t_3)$}}] at (axis cs: 0.5,1.25) {};
    \end{axis}
  \end{tikzpicture}
  \caption{Time evolution of the probability distribution $p(x,t)$ in
    the case of diffusion. To begin on the process the inital
    distribution is given by a delta function $p(x,t) = \delta(x)$.}
  \label{fig:2015-06-03-1}
\end{figure}
The time evolution of the probability density is given by
\begin{align*}
  p(x,t+\tau) &= \int\limits_{-\infty}^{\infty} \diff x P(x',t+\tau|x',t) p(x',t),
\end{align*}
where $P(x,t+\tau|x',t)$ is the \acct{transition probability}, which
is a conditional probability. It gives the probability to get to the
place $x$ at time $t+\tau$ if we were at time $t$ at $x'$. 

To end with a equation of motion for $p(x,t)$ we look at small
$\tau$. Hence only small $\Delta\equiv x-x'$ will contribute. We then
find
\begin{align*}
  p(x,t+\tau) &= \int\limits_{-\infty}^{\infty} \diff \Delta P(x+\Delta -\Delta,t+\tau|x-\Delta,t) p(x-\Delta,t) \\
  &= \int\limits_{-\infty}^{\infty} \diff \Delta \sum\limits_{n=0}^{\infty} \frac{(-\Delta)^n}{n!} \frac{\partial^n}{\partial x^n} P(x+\Delta,t+\tau|x,t)p(x,t),
  \intertext{where we expanded around $-\Delta$. Rewriting leads to} 
  &= \sum\limits_{n=0}^{\infty} \frac{(-1)^n}{n!} \frac{\partial^n}{\partial x^n} \underbrace{\int\limits_{-\infty}^{\infty} \Delta^n P(x+\Delta,t+\tau|x,t)}_{\equiv M^{(n)}(x,t,t+\tau)} p(x,t),
  \intertext{with $M^{(n)}(x,t,t+\tau) = \braket{(x(t+\tau)-x(t))^n}$. So we end up with}
  &= \sum\limits_{n=0}^{\infty} \frac{(-1)^n}{n!} \frac{\partial^n}{\partial x^n} M^{(n)}(x,t,t+\tau) p(x,t).
\end{align*}
This method is also known as \acct{Kramers-Moyal expansion}. First we
want to calculate the first moments. These are given by
\begin{align*}
  M^{(0)} &= \braket{(x(t+\tau)-x(t))^0} = 1, \\
  M^{(1)} &= \braket{(x(t+\tau)-x(t))^1} = 0, \\
  M^{(2)} &= \braket{(x(t+\tau)-x(t))^2} = 2D\tau,\\
  M^{(3)} &= \braket{(x(t+\tau)-x(t))^3} = 0,
\end{align*}
where the third moment vanishes due to symmetry. Hence all higher
moments with odd $n$ vanishes as well
\begin{align*}
  M^{(2n+1)} &= 0.
\end{align*}
The fourth moment is
\begin{align*}
  M^{(4)} &= \braket{(x(t+\tau)-x(t))^4} \\
  &= \Braket{\left(\int \diff t' \zeta(t') \right)^4} \\
  &=  \int \diff t_1 \int \diff t_2 \int \diff t_3 \int \diff t_4 \braket{\zeta_1\zeta_2\zeta_3\zeta_4} \\
  &= 3 \cdot 4 D^2 \tau^2,
\end{align*}
where we used the Wick theorem. All other moments with even $n$ will
scale with $t^n$. Plugging in leads to
\begin{align*}
  p(x,t+\tau) &= p(x,t) + \frac{1}{2} \partial_x^2[2D\tau p(x,t)] + \frac{12}{4!} \partial_x^4 [D^2 \tau^2 p(x,t)] \\
  \lim\limits_{\tau \to 0} \frac{p(x,t+\tau) -p(x,t)}{\tau} &= D \partial_x^2 p(x,t) + \mathcal{O}(\tau).
\end{align*}
Which gives the well known \acct{diffusion equation} (special case of
the \acct{Fokker-Planck equation})
\begin{align*}
  \boxed{\partial_t p(x,t) = D \partial_x^2 p(x,t)}
\end{align*}
We solve this partial differential equation via Fourier transformation
\begin{align*}
  p(x,t) &= \int \frac{\diff k}{2 \pi} \ee^{\ii k x} p(k,t).
\end{align*}
The diffusion equation then takes on the form
\begin{align*}
  \int \frac{\diff k}{2\pi} \ee^{\ii k x} \partial_t p(k,t) &= D \int \frac{\diff k}{2\pi} (-k^2) \ee^{\ii k x} p(k,t) \\
  \implies \partial_t p(k,t) &= - D k^2 p(k,t)\\
  \implies p(k,t) &= \ee^{-D k^2t} p(k,0).
\end{align*}
Back to the real space gives
\begin{align*}
  p(x,t) &= \int \frac{\diff k}{2\pi} \ee^{-\ii k x} p(k,t) \\
  &= \int \frac{\diff k}{2\pi} \ee^{-\ii k x} \ee^{-Dk^2t}p(k,0) \\
  &= \int \frac{\diff k}{2\pi} \ee^{-\ii k x - D k^2 t} \int \diff x \ee^{\ii k x} p(x,0) \\
  &= \frac{1}{\sqrt{4 \pi D t}} \int \diff x' \exp\left(\frac{-(x-x')^2}{4Dt}\right) p(x',0).
\end{align*}
If we draw the initial condition $p(x',0) = \delta(x')$ we find
\begin{align*}
  p(x,t) &= \frac{1}{\sqrt{4\pi D t}} \exp\left(-\frac{x^2}{4Dt}\right) \\
  &= \frac{1}{\sqrt{4\pi D t}} \exp\left(-\frac{x^2}{2 \braket{x^2(t)}}\right).
\end{align*}

\subsection{Diffusion in a potential}

Consider potentials as depicted in figure~\ref{fig:2015-06-03-2}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node at (1.75,3.25) {(a)};
    \node at (6.75,3.25) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = -2, xmax = 2,
        ymin = 0, ymax = 4,
        domain=-2:2,
        xtick=\empty, ytick=\empty,
	xlabel = $x$,
        ylabel = $V(x)$,
	width=5cm
        ]
        \addplot [MidnightBlue,smooth,samples=50] {x^2};
	\draw [help lines] (axis cs: 0,0) -- (axis cs: 0,4);
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
        xmin =-1.25, xmax = 1.25, 
	ymin = -1.25 , ymax =1,
	xtick=\empty, 
	ytick=\empty,
        ylabel = $V(x)$,
	width = 5cm
        ]
        \addplot [domain=0:2*pi,smooth,samples=200,dashed ]({cos(deg(x))},{sin(deg(x))/2});  
        \addplot [domain=0:2*pi,smooth,samples=200,MidnightBlue ]({cos(deg(x))},{sin(deg(x))/2 -0.173205*cos(deg(10*x))}); 
        \draw (axis cs: -3.14/3.15,-1) -- (axis cs: -3.14/3.15,0);
        \draw (axis cs: 3.14/3.15,-1) -- (axis cs: 3.14/3.15,0);
        \node[circle,fill,inner sep=1pt] at (axis cs: 0.28,0.48) {};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Different potentials. (a) Harmonic Potential. (b) Periodic
    potential with periodic boundary conditions (ring).}
  \label{fig:2015-06-03-2}
\end{figure}
The harmonic potential
\begin{align*}
  V(x) = \frac{k}{2}x^2
\end{align*}
leads to the Langevin equation
\begin{align*}
  \dot{x} &= \mu F(x) + \zeta(t) \\
  &= \mu (-kx) + \zeta(t),
\end{align*}
where $\mu$ is the mobility which is given by $\mu = D/(\kB T)$.

The periodic potential in the figure is
\begin{align*}
  V(x) &= A \cos(kx).
\end{align*}
The corresponding Langevin equation is
\begin{align*}
  \dot{x} &= \mu \left[- \frac{\partial V(x)}{\partial x} + f_{nc} \right] +\zeta(t),
  \intertext{where $f_{nc}$ is a non-conservative force. Both forces can be written to a force $F(x)$ which allows us to write the Langevin equation in the form}
  &= \mu F(x) + \zeta(t).
\end{align*}

To derive the Fokker-Planck equation we have to look again at the
moments. The first three moments are given by
\begin{align*}
  M^{(0)} &= 1,\\
  M^{(1)} &= \braket{x(t+\tau) -x(t)} \\
  &= \Braket{\int_{t}^{t+\tau} \dot{x}(t') \diff t'}\\
  &=  \Braket{\int_{t}^{t+\tau} \mu F(x(t'),t') + \zeta(t') \diff t'} \\
  &= \mu F(x,t) \tau + \mathcal{O}(\tau^2),\\
  M^{(2)} &= 2 D \tau + \mathcal{O}(\tau^2).
\end{align*}
Plugging in leads to
\begin{align*}
  \frac{p(x,t+\tau)-x(t)}{\tau} &= - \partial_x [\mu F(x,t) p(x,t)] + D \partial_x^2 p(x,t) + \mathcal{O}(\tau).
\end{align*}
For small $\tau$ we find the \acct{Fokker-Planck equation}
\begin{align*}
  \boxed{ \partial_x p(x,t) =  -\partial_x [\mu F(x,t)p(x,t)] + \kB T \partial_x^2 p(x,t) }
\end{align*}
we can rewrite the Fokker-Planck equation to
\begin{align*}
  \partial_t p(x,t) &= - \partial_x j(x,t), 
\end{align*}
which is a \acct{continuity equation} for the probability
density. Here $j(x,t)$ describes the \acct{probability current}.
%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: