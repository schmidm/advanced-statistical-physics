\subsection{Renormalization group flow}

\begin{figure}[tb]
  \centering
  \def\cross[#1] at (#2,#3);{
    \draw[MidnightBlue,#1] ({#2-.1},{#3-.1}) -- ({#2+.1},{#3+.1});
    \draw[MidnightBlue,#1] ({#2-.1},{#3+.1}) -- ({#2+.1},{#3-.1});
    \node[MidnightBlue,draw,fill,circle,inner sep=.5pt] at ({#2},{#3}) {};
  }
  \begin{tikzpicture}[gfx,scale=0.75]
    \begin{scope}[shift={(0,0)}]
      \node  at (0,3) {(a)};
      \draw (0,0)--(3,0)--(3,2)--(0,2)--cycle;
      \foreach \x in {0,...,4}
      \cross[] at  (0.5*\x+0.5,1.5);
      \foreach \x in {0,...,4}
      \cross[] at  (0.5*\x+0.5,1);
      \foreach \x in {0,...,4}
      \cross[] at  (0.5*\x+0.5,.5);
    \end{scope}
    \draw[->] (3.5,1) -- node [midway,above] {integrating out} (6,1);
    \begin{scope}[shift={(6.5,0)}]
      \node  at (0,3) {(b)};
      \draw (0,0)--(3,0)--(3,2)--(0,2)--cycle;
      \foreach \x in {0,...,2}
      \cross[] at  (\x+0.5,1.5);
      \foreach \x in {0,...,2}
      \cross[] at  (\x+0.5,.5);
      \draw [decorate,decoration={brace,amplitude= 2pt,mirror,raise=2pt},yshift=0pt]
      (0.5,1.5) -- (1.5,1.5) node [midway, below, yshift=-2pt] {$b$};
    \end{scope}
  \end{tikzpicture}
  \caption{Remormalization group. (a) System of (eff.) coupling
    $\bm{K}$ and correlation length $\xi$. (b) After integrating out
    the degrees of freedom on a scale $b$ we get a new coupling
    $\bm{K}' = \mathcal{R}[\bm{K}]$ and the new correlation length
    $\xi' = \xi/b$. }
  \label{fig:8.7.15-1}
\end{figure}

Given is a system as depicted in figure~\ref{fig:8.7.15-1}. We start
with a system of (eff.) couplings $\bm{K}$ and correlation length
$\xi$. After integrating out the degrees of freedom on a scale $b$ we
get the new couplings $\bm{K}' = \mathcal{R}[\bm{K}]$ and the new
correlation length $\xi'=\xi/b$. Here $\mathcal{R}$ denotes the
\acct{RG transformation} and $b$ the decimation
transformation. Because we are interested in the behavior of the
coupling constants (manifold of coupling constants) around the
non-trivial fix point, we linearize the transformation equations. For
example in the case of the two dimensional Ising model we find a
flow chart as depicted in figure~\ref{fig:8.7.15-2}. In general we
will also find such a figure.
\begin{figure}[tb]
  \centering
\usetikzlibrary{decorations.markings}  
\begin{tikzpicture}[gfx,scale=0.75]
  \draw[<->] (0,3.5) node [above] {$K_2-K_n$} |- (5,0)  node [right] {$K$} ;
  \begin{scope}[decoration={
      markings,
      mark=at position 0.5 with {\arrow{>}}}
    ] 
    \draw[DarkOrange3,postaction={decorate} ,out = -70, in = 140] (1,3) to (2,1.5);
    \draw[DarkOrange3,postaction={decorate},in =-40 ,out = 155] (4,0.25)to(2,1.5);
    \draw[MidnightBlue,postaction={decorate},in =-20 ,out = 185] (2,1.5) to (0.25,1.75);
    \draw[MidnightBlue,postaction={decorate},in =-160 ,out = 5] (2,1.5) to (4.5,2);
    \draw[dashed,postaction={decorate}] (1.2,3) .. controls (2,1.5)  and (2,1.5) .. (4.5,2.2);
    \draw[fill] (2,1.5) circle (1.5pt) node[yshift=-3pt,below] {FP};	
  \end{scope}
\end{tikzpicture}
  \caption{Flow chart of the linearized recursion relation around the
    non-trivial fix point. The highlighted curve $K^c(\lambda)$ (orange)
    corresponds to the stable manifold. Here $\lambda$ is just a
    control parameter. }
  \label{fig:8.7.15-2}
\end{figure}
There are shown two kinds of manifolds, viz.\ the stable and the
unstable manifold. All starting points on the stable manifold
$K^c(\lambda)$ will end in the non-trivial fix pint. Starting on the
unstable manifold leads to a repulsive behavior. We can see:
\begin{itemize}
\item Al points (i.e.\ physical models) along $K^c(\lambda)$ are
  driven toward $K^\ast$.
\item The correlation length goes to infinity at the critical point
  $\xi^\ast(K) = \infty$ and hence $\xi(K'(\lambda)) = \infty$.
\end{itemize}
Consequently all $K^c(\lambda)$ are critical, despite having different
couplings. This direction in coupling space corresponds to an
irrelevant perturbation (irrelevant operator). From this discussion we
found the reasons for the universality properties.

\subsection{Universality}

All critical phenomena can be classified into \acct{universality
  classes} (same expectation within one class). Relevant criteria are:
\begin{itemize}
\item Spatial dimension 
\item Dimension (or symmetry) of the order parameters (see
  table~\ref{tab:8.7.15-1}).
\item Range of coupling:
  \begin{itemize}
  \item Short range
  \item Long range (MF)
  \end{itemize}
\end{itemize}

\begin{table}
  \centering
  \caption{Order parameters.}
  \begin{tabular}{ll}
    \toprule
    Dimension & Symmetry, Example \\
    \midrule
    $0$     & Binery $\mathbb{Z}_2$, Ising $\pm$, lattice gas occupied (uncorrelated) \\
    $2$     & Planar spins, X-Y model \\
    $3$     & Heisenberg model  \\
    \bottomrule
  \end{tabular}
  \label{tab:8.7.15-1}
\end{table}

\begin{theorem}[Mermin-Wagner theorem:] 
  In spatial dimension $d \le 2$ there is no spontaneous broken
  constant symmetry, i.e.\ no LR-order for $\mathrm{dim} \ge 2$.
\end{theorem}

According to the \acct{Mermin-Wagner theorem} fluctuations will destroy
the order for $d \le 2$. For $d=2$ there is no two dimensional solid!

\chapter{Systems with variable particle numbers (Grand-canonical ensemble)}

\section{Chemical potential}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \node at (-0.5,2.5) {(a)};     
      \draw (0,0)-- node [midway,below] {$E_1$, $N_1$} (2,0) --(2,0.5)--(2.5,0.5)--(2.5,0)-- node [midway,below] {$E_2$, $N_2$}(5,0) -- (5,2) -- node [midway,above] {\circled{2}} (2.5,2) -- (2.5,0.75) --  (2,0.75) -- (2,2) -- node [midway,above] {\circled{1}} (0,2)--cycle;
      \foreach \i in {1,...,250}
      \fill [MidnightBlue] (rnd*2cm+0.0cm, rnd*2cm) circle (.25pt);
      \foreach \i in {1,...,250}
      \fill [MidnightBlue] (rnd*2.5cm+2.5cm, rnd*2cm) circle (.25pt);
    \end{scope}
    \begin{scope}[shift={(6,0)}]
      \node at (-0.5,2.5) {(b)};  
      \draw [<->] (0,2) node [above] {$N_1$} |- (5,0)  node [right] {$t$};
      \draw[MidnightBlue,domain=0:5,samples=150]    plot (\x,{exp(-2*\x)+0.5+0.15*rnd});
      \draw[help lines,dashed] (0,0.575) node [left] {$\tilde{N}$}--(5,0.575);
    \end{scope}
    \begin{scope}[shift={(3,-3.75)}]
      \node at (-0.5,2.75) {(c)};  
      \draw [<->] (0,2) node [above] {$p(N_1)$} |- (5,0)  node [right] {$N_1$};
      \draw[MidnightBlue,domain=0:5,samples=150]  plot (\x,{exp(-(\x-2)^2/0.1)});
      \draw[help lines,dashed] (2,0) node [below] {$\tilde{N}$}--(2,1.5);
    \end{scope}
  \end{tikzpicture} 
  \caption{Model. (a) Two boxes \circled{1} and \circled{2} are filled
    with a gas and chan exchange particles. (a) The number of
    particles in the left box. (c) Histogram for the number of
    particles in box \circled{1}. }
  \label{fig:8.7.15-3}
\end{figure}

Given are two boxes filled with gas. The boxes are allowed to exchange
particles (see figure~\ref{fig:8.7.15-3}). The micro states are
denoted by
\begin{align*}
  \bm{\xi} &= \{ \bm{q}_i,\bm{p}_i \} . 
\end{align*}
In the whole system the particle number $N$ and the total energy $E$
is conserved
\begin{align*} 
 N & = \const = N_1(t) + N_2(t), \\
  E &= E_1(t) + E_2(t) = \const.
\end{align*}
Like in the first part of this lecture we can calculate the
probability $N_1$ by given $E$, $V$ and $N$ by
\begin{align*}
  p(N_1|E,V,N) &\propto \int \diff \bm{\xi} \delta(N_1(\bm{\xi}) -N_1)\delta(\HH(H)(\bm{\xi}) - E) \\
  &= \int\limits_{0}^{E} \diff E_1 \int \diff\bm{\xi} \delta(\HH_1(\bm{\xi})-E_1) \delta(N_1(\bm{\xi})-N_1) \delta(\HH_2(\bm{\xi}) - (E-E_1)) \\
  &= \int\limits_{0}^{E} \diff E_1  \Omega_1(E_1,V_1,N_1) \Omega_2(E-E_1,V_2,N-N_1) \frac{N!}{N_1!N_2!},
\end{align*}
where the last product $N!/(N_1!N_2!)$ are the distributions of $N$
particles between two chambers. Let's define 
\begin{align*}
  \tilde{\Omega}(E,V,N) \equiv \frac{\Omega(E,V,N)}{N!},
\end{align*}
so we can rewrite the probability
\begin{align*}
  p(N_1) &= \int\limits_{0}^{E} \diff E_1 \tilde{\Omega}_1(E_1,V_1,N_1) \tilde{\Omega}(E_2,V_2,N_1) \cdot \frac{1}{\sum\limits_{N_1=0}^{N} \int \diff E_1 \ldots},
\end{align*}
where the last factor is just the normalization which kills $N!$. The
probability to get $N_1$ and $E_1$ is then
\begin{align*}
  p(N_1,E_1) &= \frac{\tilde{\Omega}_1(E_1,V_1,N_1) \tilde{\Omega}(E_2,V_2,N_2)}{\text{Norm.}}
\end{align*}
The most likely value for the energy $E_1$ is denoted by $E_1^\ast$
and leads to
\begin{align*}
  \frac{\partial_E \tilde{\Omega}_1(E^{\ast}_1,V_1,N_1^\ast)}{\tilde{\Omega}_1(E_1^\ast,V_1,N_1^\ast)} &= \frac{\partial_E \tilde{\Omega}_2(E^{\ast}_2,V_2,N_2^\ast)}{\tilde{\Omega}_1(E_2^\ast,V_2,N_2^\ast)},
\end{align*}
with respect to the most likely value of $N_1$ which will be denoted
by $N_1^\ast$
\begin{align*}
  \frac{\partial_{N_1} \tilde{\Omega}_1(E^{\ast}_1,V_1,N_1^\ast)}{\tilde{\Omega}_1(E_1^\ast,V_1,N_1^\ast)} &= \frac{\partial_{N_2} \tilde{\Omega}_2(E^{\ast}_2,V_2,N_2^\ast)}{\tilde{\Omega}_1(E_2^\ast,V_2,N_2^\ast)}.
\end{align*}
This allows us to define the \acct{chemical potential} as
\begin{align*}
  \boxed{ \mu \equiv -\frac{1}{\beta} \partial_N \log \tilde{\Omega}(E,V,N)}
\end{align*}


%%% Local Variables:  
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: