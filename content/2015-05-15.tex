\section{Example: Weight lifting in contact with a heat bath}

Here we deal with the example depicted in
figure~\ref{fig:15.5.15-1}. A weight is fixed with a constraint at
special height. After releasing the constraint the eight is lifted to
a higher position. During the whole experiment the system is coupled
to an external heat bath with temperature $\beta$.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*1.5 cm, rnd*-1cm) circle (.25pt);
      \draw [fill opacity=.3,fill, DarkOrange3] (-1.5,-0.2) -- (-1.5,-2) --(3,-2) -- (3,-0.2) -- (1.5,-0.2) -- (1.5,-1) -- (0,-1) --(0,-.2)  -- cycle;
      \node[DarkOrange3] at (2,-1.75) {heat bath};
      \draw [fill,DimGray!75] (0.5,0) -- (1,0) --(1,0.5) -- (0.5,0.5)  -- cycle;
      \draw (0,1) -- (0,-1) -- (1.5,-1) -- (1.5,1)  (0,0) --(1.5,0) ;
      \draw [->]  (-0.5,1) -- (-0.5,0) node[left,midway] {$g$}; 
      \draw (1.5,0)--(2,0.15) (1.5,0)--(2,-0.15);
      \draw [->] (3.25,-0.5)--(4.75,-0.5);
    \end{scope}
    \begin{scope}[shift={(6.5,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*1.5 cm, rnd*-1.5cm+0.5cm) circle (.25pt);
      \draw [fill opacity=.3,fill, DarkOrange3] (-1.5,-0.2) -- (-1.5,-2) --(3,-2) -- (3,-0.2) -- (1.5,-0.2) -- (1.5,-1) -- (0,-1) --(0,-.2)  -- cycle;
      \node[DarkOrange3] at (2,-1.75) {heat bath};
      \draw [fill,DimGray!75] (0.5,0.5) -- (1,0.5) --(1,1) -- (0.5,1)  -- cycle;
      \draw (0,1) -- (0,-1) -- (1.5,-1) -- (1.5,1)   (0,0.5) --(1.5,0.5) ;
      \draw [->]  (-0.5,1) -- (-0.5,0) node[left,midway] {$g$}; 
      \draw (1.5,0.5)--(2,0.65)  (1.5,0.5)--(2, 0.35);
    \end{scope}
  \end{tikzpicture} 
  \caption{Weight under a constraint at a specific heigt. After
    releasing the constraint the weight is lifted. The whole system is
    coupled to a heat bath with temperature $\beta$.}
  \label{fig:15.5.15-1}
\end{figure}

After releasing the constraint we will end in a new equilibrium
\begin{align*}
  p(V) \propto \ee^{-\beta [F(V) - p^{\mathrm{ex}} V ]},
\end{align*}
where $p^{\mathrm{ex}}$ is the external pressure. The most likely
value $\hat{V}$ is
\begin{align*}
  - \frac{\partial F}{\partial V} \bigg|_{\hat{V}} &= p^{\mathrm{ex}} \equiv \frac{M g}{A}.
\end{align*}
The first law evaluated from $V^0$ is
\begin{align*}
  W &= \Delta E + Q \\
  &= \Delta E_{\mathrm{ges}} + p^{\mathrm{ex}} \underbrace{(\hat{V} - V^0)}_{= \Delta V} + Q.
\end{align*}
Here $\Delta E$ is the energy change of the system, $p^{\mathrm{ex}}
\Delta V$ the part from the weight, and $Q$ describes the heat
bath. The entropy change is then given by
\begin{align*}
  \Delta S \equiv \Delta (\hat{V},S^0) &= \log \frac{p(\hat{V})}{p(V^0)} \\
  &= - \left[ F(\hat{V}) - F(V^0) \right] - p^{\mathrm{ex}} \Delta V \geq 0. 
\end{align*}

\begin{notice}
  Note that the proof of the second law is here a direct consequence
  of the used notation and definition of the entropy because
  \begin{align*}
    \log \frac{p(\hat{V})}{p(V^0)} \geq 0.
  \end{align*}
  This is true because $p(\hat{V}) > p(V^0)$ (it is the most likely
  value) and hence $p(\hat{V})/p(V^0) > 0$. 
\end{notice}

The naive extracted work is given by
\begin{align*}
  \boxed{ W^{\mathrm{out}} \equiv p^{\mathrm{ex}} \Delta V \geq - \Delta F }
\end{align*}
Obviously the work extraction is bounded by the free energy.

\section{Equilibrium of volume}

Here we want to observe a box divided into two part by a piston as
shown in figure~\ref{fig:15.5.15-2}. The smaller part $V_1$ of the box
is filled with much more particles than the bigger part $V_2$ on the
right. The whole system will equilibrate.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \foreach \i in {1,...,150}
    \fill [MidnightBlue] (rnd*4 cm, rnd*1.5cm) circle (.25pt);
    \draw (0,0) --(4,0) --(4,1.5)  -- (0,1.5) node [midway,below]  {$V_2$} --cycle;
    \foreach \i in {1,...,500}
    \fill [MidnightBlue] (rnd*-1.5 cm, rnd*1.5cm) circle (.25pt);
    \draw (-1.5,0) --(0,0) --(0,1.5) -- (-1.5,1.5) node [midway,below]  {$V_1$} --cycle;
  \end{tikzpicture} 
  \caption{A box divided into two part with different volumes by a
    piston.}
  \label{fig:15.5.15-2}
\end{figure}

During the equilibration the total volume and the total energy are
conserved
\begin{align*}
  V_1 + V_2 &= V_{\mathrm{tot}} = \const ,\\
  E_1 + E_2 &= E_{\mathrm{tot}} = \const .
\end{align*}
After releasing the volume constraint the probability to find $V_1$ is
\begin{align*}
  p(V_1) &\propto \underbrace{\int \diff \xi_1 \int \diff \xi_2}_{V_1} \delta (\HH_{\mathrm{tot}} - E_{\mathrm{tot}}) \\
  &= \int\limits_{0}^{E_{\mathrm{tot}}} \diff E_1 \Omega(E_1,V_1) \Omega_2(E_{\mathrm{tot}} - E_1, V_{\mathrm{tot}} -V_1).
\end{align*}
The most likely value $\hat{V}_1$ and $\hat{E}_1$ leads to
\begin{align*}
  \beta_1(\hat{E}_1,\hat{V}_1) &= \beta_2(\hat{E}_2,\hat{V}_2),
\end{align*}
with $\gamma(\hat{E}_1,\hat{V}_1) = \gamma_2(\hat{E}_2,\hat{V}_2)$,
i.e.\ in equilibrium the temperature of both parts is the
same. Likewise the pressure is equal in both parts, because we find
\begin{align*}
  P_1 &= \frac{\gamma_1}{\beta_1} = \frac{\gamma_2}{\beta_2} = p_2,
\end{align*}
which is the equation of pressure.

\begin{notice}
  Allowing for energy exchange is crucial, otherwise we deal with the
  ``adiabatic'' piston.
\end{notice}

\chapter{Finite-time processes}

\section{Time-dependent process, sampling an initial condition}

For ``statistical'' physics we need a new average. Here we choose the
average over an initial phase space point $\bm{\xi}^0$.

Initial value drawn from
\begin{enumerate}
\item Microcanoncial distribution
  \begin{align*}
    p(\xi^0) \propto \frac{1}{\Omega(E)} \propto \delta(\HH(\xi^0) -E)
  \end{align*}
\item Canonical distribution
  \begin{align*}
    p(\xi^0) = \ee^{-\beta\HH(\xi^0) + \beta F}
  \end{align*}
\end{enumerate}

\section{Integral fluctuation theorem}

The \acct{integral fluctuation theorem} allows to derive an inequality
for classical systems.

Assume an initial distribution $p^0(\xi^0)$ that is non-zero
everywhere in phase space (canonical distribution) and $\xi^t =
\xi^t(\xi^0)$ which is normalized. For any normalized function
$p^1(\xi)$ we have
\begin{align*}
  1 &= \int \diff \xi^t p^1(\xi^t(\xi^0)) \\
  &= \int \diff \xi^0 p^1(\xi^t(\xi^0)) \\
  &= \int \diff \xi^0 \frac{p^1(\xi^t(\xi^0))}{p^0(\xi^0)} p^0(\xi^0) \\
  &= \int \diff p^0(\xi^0) \exp\left( \log\left( \frac{p^1(\xi^t(\xi^0)) }{p^0(\xi^0)}   \right)  \right) \\
  &= \Braket{\exp\left( \log \frac{p^1(\xi^t(\xi^0))}{p^0(\xi^0)} \right)} = 1.
\end{align*}

\begin{notice}
  For a given probability distribution $p(x)$ and a given function
  $R(x)$ we can write
  \begin{align*}
    \braket{\ee^{-R(x)}} &= 1 = \int \diff x p(x) \ee^{-R(x)}.
  \end{align*}
  The consequence is then
  \begin{align*}
    1 &= \braket{\ee^{-R}} \geq \ee^{-\braket{R}},
    \intertext{using the logarithm}
    0 &\geq \log \ee^{- \braket{R}} = - \braket{R}.
  \end{align*}
  Hence we find
  \begin{align*}
    \boxed{  \braket{R} \geq 0 }
  \end{align*}
\end{notice}

With help of the note above we can rewrite
\begin{align*}
  \Braket{\exp\left( \log\frac{p^0(\xi^0)}{p^1(\xi^t(\xi^0))}  \right)} = 1,
\end{align*}
to an inequality 
\begin{align*}
  \boxed{ \Braket{\log \frac{p^0(\xi^0)}{p^1(\xi^t(\xi^0))}} \geq 0 } 
\end{align*}

\section{Jarzynski relation (1997)}

We assume time dependent driving. The Hamilton function is given by
$\HH(\bm{\xi}, \lambda(t))$, where $\lambda(t)$ is a control
parameter. Given are two probability distribution, the initial
distribution (equilibrium)
\begin{align*}
  p^0(\xi^0) &= \ee^{-\beta\HH(\xi^0,\lambda^0) + \beta F(\lambda^0)}
\end{align*}
and a distribution at time $t>0$
\begin{align*}
  p^1(\xi) &= \ee^{-\beta\HH(\xi,\lambda^t) + \beta F(\lambda^t)}.
\end{align*}
Using the logarithm leads to
\begin{align*}
  \log \frac{p^0(\xi^0)}{p^1(\xi^t(\xi^0))} &= - \beta \HH^0(\xi^0,\lambda^0) + \beta F(\lambda^0) + \beta \HH^t(\xi^t,\lambda^t) - \beta F(\lambda^t) \\
  &= \beta \left( W[\xi^0,t] - \Delta F  \right),
\end{align*}
where $\Delta F$ is the free energy difference between the equilibrium
state corresponding to the final value $\lambda^t$ and the initial
state and $W[\xi^0,t]$ the work. Using the integral fluctuation
theorem gives
\begin{align*}
  \Braket{\ee^{-\beta(W-\Delta F)}} &= 1 \\
  \Braket{\ee^{-\beta W}} &= \ee^{-\beta \Delta F},
\end{align*}
which is known as the \acct{Jarzynski relation}. It says that the work
spent in driving the system from an initial state for a time $t$ is
connect to the free energy difference. The Jarzynski relation is valid
no matter how fast the process happens.

A typical example for the demonstration of the Jarzynski relation is
depicted in figure~\ref{fig:15.5.15-3}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-2cm+1cm, rnd*-1cm) circle (.25pt);
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-1,0) -- (-1,-1) (-2.5,-0.5)--(-2.5,-1.5); 
      \draw[decoration={aspect=0.3, segment length=2mm, amplitude=2mm,coil},decorate] (-1,-0.5) -- (-2.5,-.5);
      \draw[fill, DimGray!75] (-2.75,-1.5)--(-2.25,-1.5) -- (-2.25,-2) --(-2.75,-2) node [midway, below] {$\lambda^0$} --cycle;
      \draw[->] (-2.25,-1.75) -- (-1.75,-1.75); 
      \draw [->] (1.5,-0.5)--(2.5,-0.5);
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-1cm+1cm, rnd*-1cm) circle (.25pt);
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-0,0) -- (-0,-1) (-.75,-0.5)--(-.75,-1.5); 
      \draw[decoration={aspect=0.4, segment length=1mm, amplitude=2mm,coil},decorate] (0,-0.5) -- (-0.75,-.5);
      \draw[fill, DimGray!75] (-1,-1.5)--(-.5,-1.5) -- (-.5,-2) --(-1,-2) node [midway, below] {$\lambda^t$} --cycle;
      \draw[->] (-.5,-1.75) -- (0,-1.75); 
    \end{scope}
    \begin{scope}[shift={(1,-4)}]
      \draw[<->] (0,2) |- (3,0) node [below] {$t$};
      \draw [dashed, thin] (0,0.5) node [left] {$\lambda^0$} -- (3,0.5);
      \draw [dashed, thin] (0,1.5)  node [left] {$\lambda^f$} --( 3,1.5); 
      \draw[MidnightBlue] (0,0.5)--(1,1.5) ;   
      \draw[DarkOrange3] (0,0.5)--(2,1.5) ;   
    \end{scope}
  \end{tikzpicture} 
  \caption{A box filled with a gas is confined by piston at position
    $\lambda^0$ and coupled to a heat bath with temperature
    $\beta$. The piston is pushed into the box to a new position
    $\lambda^t$ with different speed. }
  \label{fig:15.5.15-3}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: