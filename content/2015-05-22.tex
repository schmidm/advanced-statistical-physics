
\part{Stochastic dynamics}
\chapter{Diffusion}
\section{Brownian motion and Langevin equation}

Consider a colloidal particle in a fluid as depicted in
figure~\ref{fig:22.5.15-1}. The colloidal particle changes its
position over an observation time. The path of the particle looks
random.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,decoration={random steps,segment length=0.3mm}]
    \begin{scope}[shift={(0,0)}]
      \foreach \i in {1,...,600}
      \fill [MidnightBlue] (rnd*3.0 cm, rnd*2cm) circle (.25pt);
      \draw (0,0)--(3,0)--(3,2)--(0,2)--cycle;			
      \draw[DarkOrange3,postaction={decorate,decoration={markings,mark=at position 0.5 with {\arrow{>}}}}] (0.5,0.5) to [out=70,in=150] (2,1.5);
      \draw[decorate,DarkOrange3,thin] (0.5,0.5) to  [out=-20,in=200](2,1.5);
      \shade [ball color=MidnightBlue] (0.5,0.5) ellipse (0.1 and 0.1);
      \shade [ball color=DarkOrange3] (2,1.5) ellipse (0.1 and 0.1);
    \end{scope}
  \end{tikzpicture}
  \caption{Colloidal particel in a fluid. The particle will change its
    position. The path of the particle is drawn in orange.}
  \label{fig:22.5.15-1}
\end{figure}
According to Newton the equation of motion is
\begin{align*}
  \dot{\bm{P}} &= F(\{\bm{p},\bm{r}_i \}).
\end{align*}
Replace this Hamilton function by a stochastic \acct{Langevin
  equation} leads to
\begin{align*}
  \dot{\bm{P}} &= - \tilde{\gamma} \bm{P} + \bm{f}(t),
\end{align*}
where the first term describes the friction and the second one the
stochastic force (random force, noise). The three components decouple
\begin{align*}
  \dot{P} &= - \tilde{\gamma} P + f(t)
\end{align*}
which is the Langevin equation. Here we used
\begin{align*}
  \tilde{\gamma} &= \frac{\gamma}{m} = \frac{6 \pi \eta R}{m}
\end{align*}
for the friction coefficient. Replacing $\tilde{\gamma} \to
\gamma$. The solution of the Langevin equation is then given by
\begin{align*}
  p(t) &= \ee^{-\gamma t} \left[\int\limits_{0}^{t} \diff t' \ee^{\gamma t'} f(t') + p(0) \right].
\end{align*}
Drawing the initial condition $p(0) = 0$ the finite time histogram is
\begin{align*}
  \overline{p^2} &\equiv \frac{1}{\tau} \int\limits_{0}^{\tau} \diff t p^2(t) \\
  &= \frac{1}{\tau} \int\limits_{0}^{\tau} \diff t  \ee^{-2\gamma t} \int\limits_{0}^{t} \diff t' \int\limits_{0}^{t} \diff t'' \ee^{\gamma(t'+t'')} f(t') f(t''),
\end{align*}
which depends on the force history/ history/ realization
$f(t')|_{0\leq t' \leq t}$.  Now we define the ensemble of random
forces from which any specific realization is drawn:
\begin{itemize}
\item No bias $\braket{f(t)} = 0$.
\item Non correlated forces $\braket{f(t')f(t'')} = 2 B \delta(t'-t'')$
\item Higher moments (all possible contractions with the Wick theorem)
  \begin{align*}
    \braket{f_1f_2f_3f_4} &= \braket{f_1f_2}\braket{f_2f_4} + \braket{f_1f_3}\braket{f_2f_4} + \braket{f_1f_4}\braket{f_2f_3},
  \end{align*}
  where $f(t)$ is a Gaussian process.
\end{itemize}
The ensemble average $\overline{p}^2$ is then
\begin{align*}
  \braket{\overline{p^2}} &= \frac{1}{\tau} \int\limits_{0}^{\tau} \diff t \ee^{-2 \gamma t} \int\limits_{0}^{t} \diff t' \int\limits_{0}^{t} \diff t'' \ee^{\gamma(t+t'')} \underbrace{\braket{f(t') f(t'')}}_{2 B \delta (t'-t'')} \\
    &= \frac{2B}{\tau} \int\limits_0^\tau \diff t \ee^{-2 \gamma t} \frac{1}{2\gamma} \left(\ee^{2 \gamma t} -1  \right) \\
    &= \frac{B}{\gamma \tau} \int\limits_0^\tau \diff t \left(1-\ee^{-2 \gamma t} \right) \\
    &= \frac{B}{\gamma} \big( 1+ \underbrace{\mathcal{O}(\ee^{-\gamma \tau})}_{\to 0} \big) \\
    &= \frac{B}{\gamma}.
\end{align*}
Expect in (canonical) ensemble
\begin{align*}
  \braket{p^2}_{\mathrm{eq}} &= \frac{m}{\beta} = \kB T m \\
  \implies B &= \frac{m}{\beta} \gamma = \kB T m \gamma.
\end{align*}
Here $B$ is the strength of the fluctuation and $\gamma$ the friction
dissipation. This is the simplest example of a
\acct{fluctuation-dissipation theorem}. Now we need to show that
$\overline{p^2}$ is independent of the realizations. Therefore we
calculate the variance
\begin{align*}
  \Braket{ \left( \overline{p^2} - \braket{(\overline{p^2})^2} \right)^2} &= \braket{(\overline{p^2})^2} - \braket{\overline{p^2}}^2.
\end{align*}
First we want to calculate the first term
\begin{multline*}
  \braket{(\overline{p^2})^2} = \frac{1}{\tau^2} \int\limits_{0}^{\tau} \diff t \int\limits_{0}^{\tau} \diff t' \ee^{-2 \gamma t} \int\limits_{0}^{t} \diff \tau \int\limits_{0}^{t} \diff \tau' \int\limits_{0}^{\tau'} \diff \sigma \int\limits_{0}^{\tau'} \diff \sigma' \\ \times \ee^{\gamma (\tau+\tau'+\sigma+\sigma')} \braket{f(\tau) f(\tau') f(\sigma) f(\sigma')}. 
\end{multline*}
Because this calculation is a bit cumbersome we just give the solution
which is
\begin{align*}
 \braket{(\overline{p^2})^2} &= \braket{\overline{p^2}}^2 + \mathcal{O} (1/\gamma^2).
\end{align*}
Hence the variance vanishes.

\section{Diffusion}

We draw the initial values for position $x(0) = 0$ and momentum $p(0)
= 0$ . The position is then given by
\begin{align*}
  x(t) &= \frac{1}{m} \int\limits_{0}^{t} \diff t'\left[ p(t') +x(0) \right] \\
  &= \frac{1}{m} \int\limits_{0}^{t} \diff t' \ee^{-\gamma t}  \int\limits_{0}^{t'} \diff \tau \ee^{\gamma \tau} f(\tau) \\
  \implies \braket{x(t)} &\propto \braket{f(t)} = 0.
\end{align*}
The quadratic position average is
\begin{align*}
  \braket{x^2(t)} &= \frac{1}{m} \int\limits_{0}^{t} \diff t' \int\limits_{0}^{t} \diff t'' \ee^{-\gamma(t'+t'')} \int\limits_{0}^{t'} \diff \tau \int\limits_{0}^{t''} \diff \sigma \ee^{\gamma(\tau+ \sigma)} \underbrace{\braket{f(\tau)f(\sigma)}}_{2 B \delta(\tau-\sigma)} \\
    &= \frac{1}{m^2} \int\limits_{0}^{t} \diff t' \int\limits_{0}^{t} \diff t'' \ee^{-\gamma(t'+t'')} \left( \ee^{2 \gamma \min(t',t'')} -1  \right) \frac{B}{\gamma}.
\end{align*}
It is simpler to look at the derivative, i.e.\
\begin{align*}
  \partial_t \braket{x^2(t)} &= \frac{2}{m^2} \int\limits_{0}^{t} \diff t'' \ee^{-\gamma(t+t'')} \left( \ee^{2 \gamma t''} -1 \right) \frac{B}{\gamma} \\
  &= \frac{2 B }{\gamma^2 m^2} \left( \ee^{-\gamma t}- \ee^{\gamma t} \right)\\
  &\to \frac{2 B}{\gamma^2 m^2}.
\end{align*}
For $t \gamma \gg 1$ we find
\begin{align*}
  \partial_t \braket{x^2(t)} &= \frac{2 B}{\gamma^2 m^2} 
  \intertext{and hence}
  \braket{x^2(t)} &\approx \frac{2 B}{\gamma^2 m^2} t = 2Dt,
\end{align*}
where we introduced the \acct{diffusion coefficient}
\begin{align*}
  D \equiv \frac{B}{m^2 \gamma^2} = \frac{\kB T}{m \gamma}.
\end{align*}
Thus we also find
\begin{align*}
  \Braket{\left(x(t) - \braket{x} \right)^2} \neq 0.
\end{align*}
In figure~\ref{fig:22.5.15-2} there are two different trajectories
$x(t)$ depicted. Perpendicular to each interception point the
probability distribution is drawn. As we can see the distribution
becomes wider the bigger $t$ we choose.

\begin{figure}[tb]
  \centering
  \pgfplotstableread{content/schmidi.dat}{\sch}
  \begin{tikzpicture}
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = 0, xmax = 9, 
        ymin = -0.3, ymax =0.3,
        xtick=\empty, 
        ytick=\empty,
        width = 5cm
        ]
        \addplot [Purple] table [x={time}, y={pos1}] {\sch};
        \addplot [MidnightBlue] table [x={time}, y={pos2}] {\sch};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(0.3,0.5)}]
      \begin{axis}[
        axis lines=left,
        hide y axis,
        xmin = -2, xmax = 2, 
        ymin = -0, ymax =1,
        anchor=origin,
        rotate around={-90:(current axis.origin)},
        xtick=\empty, 
        ytick=\empty,
        width = 4cm,
        height= 2cm
        ]
        \addplot[DarkOrange3,domain=-2:2,samples=50,smooth] {1*exp(-x^2/0.2)};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(1.2,0.05)}]
      \begin{axis}[
        axis lines=left,
        hide y axis,
        xmin = -2, xmax = 2, 
        ymin = -0, ymax =1,
        anchor=origin,
        rotate around={-90:(current axis.origin)},
        xtick=\empty, 
        ytick=\empty,
        width = 4cm,
        height = 2cm
        ]
        \addplot[DarkOrange3,domain=-5:5,samples=50,smooth] {0.6*exp(-x^2/1)};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Trajectories of $x(t)$. Perpendicular to the interception
    points the corresponding probability distribution is depicted.}
  \label{fig:22.5.15-2}
\end{figure}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: