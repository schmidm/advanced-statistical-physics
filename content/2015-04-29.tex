\section{Weak coupling: Canonical distribution}
Let \[ \HH_{\mathrm{int}} \ll \frac{1}{\beta}, \] then 
\begin{align*}
  \boxed{ p(\xi_0) = \frac{\ee^{-\beta \HH_0(\xi_0)}}{\int \diff \xi_0 \ee^{-\beta \HH_0(\xi_0)}}}
\end{align*}
This is the \acct{canonical distribution}. It corresponds with the
Boltzamann factor $\exp(-E/(\kB T))$. Note that for stronger coupling
the $\braket{\bullet}$ does not vanishs. The average for an observable
$A(\xi_0)$ is 
\begin{align*}
  \braket{A(\xi_0)} &= \int \diff\xi_0 p(\xi_0) A(\xi_0).
\end{align*}

\section{Finite coupling and effective transformation}
If $\beta \HH_{\mathrm{int}} \ll 1$ is violated then we introduce an
\acct{effective Hamiltonian}
\begin{align*}
  \HH_{\mathrm{eff}}(\xi_0,\beta) \equiv \HH_0(\xi_0) - \frac{1}{\beta} \log\left(\ee^{-\beta \HH_{\mathrm{int}}(q_0,q)} \right).
\end{align*}
The distribution becomes then 
\begin{align*}
  p(\xi_0) &= \frac{\ee^{-\beta \HH_{\mathrm{eff}}(\xi_0,\beta)}}{\int \diff \xi_0 \ee^{-\beta \HH_{\mathrm{eff}}(\xi_0,\beta)}}.
\end{align*}

\section{Application to fluid/colloidal-system}

The Hamilton function of a particle in a box with fluid is given by
\begin{align*}
  \HH_0 &= \frac{\bm{p}_0^2}{2m_0} + V(\bm{q}_0),
\end{align*}
where $V(\bm{q}_0)$ is for example described by the gravity
$-MgZ_0$. The probability to be in $\bm{\xi}_0$ is then
\begin{align*}
  \prob(\bm{\xi}_0) &= \ee^{- \beta \frac{\bm{p}_0^2}{2m_0} - \beta V(\bm{q}_0)} \braket{\ee^{-\beta \HH_{\mathrm{int}}(\bm{q}_0,\bm{q})}|\hat{E}^c}.
\end{align*}
The probability of momenta is
\begin{align*}
  \prob(\bm{p}_0) &= \int \diff \xi_0 \prob(\bm{p}_0,\underbrace{X_0,Y_0,Z_0}_{\bm{q}_0}) \\
  &= \ee^{- \beta \frac{\bm{p}_0^2}{2m_0}} \int \diff X_0 \diff Y_0 \diff Z_0 \ee^{- \beta V(X_0,Y_0,Z_0)} \frac{\braket{\bullet|\hat{E}^c}}{\text{Norm}} \\
  &= \ee^{- \beta \frac{\bm{p}_0^2}{2m_0}} \left[ \frac{2 \pi m_0}{\beta} \right]^{1/2}.
\end{align*}
Hence we obtain the \acct{Maxwell-Boltzmann distribution} for the velocity
(even by keeping the interaction). 

\begin{example}
  With gravity the probability is
  \begin{align*}
    \prob(Z_0) &= \ee^{-\beta M_0 g Z_0} \frac{1}{\text{Norm}}.
  \end{align*}
\end{example}

\section{Equivalence of \texorpdfstring{``the ensembles''}{“the ensembles”}}

\subsection{Motivation: Harmonic oscillator}
The Hamilton function of a harmonic oscillator is
\begin{align*}
  \HH(p,q) &= \frac{p^2}{2m} + \frac{m \omega^2}{2} q^2.
\end{align*}
The calculation of the microcanonical and the canonical distribution
do not show the same plots. In the case of the canonical distribution
\begin{align*}
  p^{\mathrm{can}}(q) &= \int \diff p \ee^{-\beta \HH} \propto \ee^{-\frac{\beta m\omega^2}{2} q^2}
\end{align*}
we find a Gaussian distribution. As depicted in
figure~\ref{fig:29.05.15-1} the microcanonical distribution shows
something different.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =-2, xmax = 2, 
	ymin = -10, ymax =90,
	xtick=\empty, 
	ytick=\empty,
        xtick = {-1,1},
        xticklabels = {$-q_{\mathrm{max}}$,$q_{\mathrm{max}}$},
	ylabel = $p^ {\mathrm{mc}}(q)$,
        xlabel = $q$,
	width = 5cm,
        xmajorgrids = true
        ]
	\draw [help lines] (axis cs: -3,0) -- (axis cs: 3,0);
	\draw [help lines] (axis cs: 0,-10) -- (axis cs: 0,100);
	\addplot [domain=-1:1,MidnightBlue,samples=150]  {asin(x*x)+10};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin = -3, xmax = 3, 
	ymin = -0.5, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
	ylabel = $p^ {\mathrm{can}}(q)$,
        xlabel = $q$,,
	width = 5cm
        ]
	\draw [help lines] (axis cs: -3,0) -- (axis cs: 3,0);
	\draw [help lines] (axis cs: 0,-0.5) -- (axis cs: 0,1.5);
	\addplot [MidnightBlue,samples=150]  {1*exp(-x^2/1)};
      \end{axis}
    \end{scope}
  \end{tikzpicture} 
  \caption{(a) Microcanonical distribution and (b) Canonical
    distribution for the harmonic oscillator.}
  \label{fig:29.05.15-1}
\end{figure}

\subsection{Big system}
Let $A$ be the observable of interest. The microcanoonical quantities
are $E$, $\Omega(E)$, and $\beta(E) \equiv \Omega'(E)/\Omega(E)$. The
canonical quantity is $\beta$. The Hamilton function of the system is
$\HH(\xi)$. To show the equivalence we calculate the average of $A$
\begin{align*}
  \braket{A|\beta} &= \int \diff \xi p^{\mathrm{can}}(\xi) A(\xi) \\
  &= \int \diff \xi \ee^{-\beta \HH(\xi)} A(\xi) \frac{1}{\int \diff\xi \ee^{-\beta \HH(\xi)}} \\
  &= \int \diff\xi \int \diff E \delta(\HH(\xi)-E) \ee^{-\beta\HH(\xi)} A(\xi) \frac{1}{\int \diff\xi \ee^{-\beta \HH(\xi)}} \\
  &= \int \diff E \underbrace{\ee^{-\beta E} \Omega(E)}_{=\ee^{-\beta E+\log\Omega(E)}} \braket{A|E} \frac{1}{\int \diff\xi \ee^{-\beta \HH(\xi)}}
\end{align*}
If we want to use the saddle point integration we have to determine
the peak of the function $\exp(-\beta E)\Omega(E)$. We find
\begin{align*}
  -\beta \Omega(E) + \Omega'(E) \big|_{\hat{E}} &= 0, \\
  \implies \beta = \frac{\Omega'(\hat{E})}{\Omega(\hat{E})} &\equiv \beta(\hat{E}). 
\end{align*}
Therefore the average is
\begin{align*}
  \braket{A|\beta} &\propto \ee^{\beta\hat{E}} \Omega(\hat{E}) \braket{A|\hat{E}} \int\limits_{-\infty}^{\infty} \diff \varepsilon \ee^{-\beta'(\hat{E}) \frac{\varepsilon^2}{2}} \frac{1}{\int \diff\xi \ee^{-\beta \HH(\xi)}},
  \intertext{with $\varepsilon = E-\hat{E}$. The normalization is}
  \int \diff \xi \ee^{-\beta\HH(\xi)} &= \int \diff \xi \int \diff E \ee^{-\beta E} \delta(\HH(\xi)-E) \\
  &= \int \diff E \ee^{-\beta E} \Omega(E) \\
  &= \ee^{-\beta \hat{E}} \Omega(\hat{E}) \int\limits_{-\infty}^{\infty} \diff \varepsilon \ee^{-\beta'(\hat{E}) \frac{\varepsilon^2}{2}}.
\end{align*}
Hence 
\begin{align*}
  \boxed{ \braket{A|E} \approx \braket{A|\hat{E}}  } 
\end{align*}
shows the equivalence of ''the ensembles''. The canonical average at
given $\beta$ can be replaced by a microcanonical one at the energy
$E$ which fulfills
\begin{align*}
  \beta(E) &\equiv \frac{\Omega'(E)}{\Omega(E)} = \beta(E).
\end{align*}
The microcanonical average of a given $E$ can be replaced by a
canonical one at (inverse) temperature $\beta$ which fulfills
\begin{align*}
  \beta \equiv \frac{\Omega'(E)}{\Omega(E)} = \beta(E).
\end{align*}
All this is true only if $\braket{A|E}$ does not vary substantially
around $\hat{E}$. 

\begin{notice}[Notation:]
  Note that the function $\beta(E)$ belongs to the microcanonical
  ensemble and $\beta$ to the canonical ensemble.
\end{notice}

\begin{example}
  Counter example:
  \begin{align*}
    p(E) =
    \begin{cases}
      \delta (E-E_0) &, \text{microcanonical} \\
      \propto \ee^{-\beta E} &, \text{canonical}
    \end{cases}.
  \end{align*}
\end{example}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: