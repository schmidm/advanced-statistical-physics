\section{Hamilton dynamics}

\subsection{Equation of motion} 

\begin{example}
  Here we want to discuss the one dimensional motion of a particle in
  a potential $V(x)$. For better clearness the system is depicted in
  figure~\ref{fig:ham-pot}. According to Newton's axioms the equation
  of motion is
  \begin{figure}[tb]
    \centering  \begin{tikzpicture}[gfx]
      \begin{axis}[
        xtick = \empty,
        ytick = \empty,
        xlabel = $x$,
        ylabel = $V(x)$,
        xmin = -2,	xmax = 2,
        ymin = -0.25,	ymax = 1.25,
        width = 5cm
        ]
        \draw[help lines] (axis cs:-3,0) -- (axis cs: 3,0);
        \draw[help lines] (axis cs:0,-2) -- (axis cs: 0,2);
        \addplot [MidnightBlue,samples=150]  { -x^2+0.5*x^4+0.75};
      \end{axis}
    \end{tikzpicture}
    \caption{One dimensional potential $V(x)$.}
    \label{fig:ham-pot}
  \end{figure}
  \begin{align*}
    m \ddot{x} &= F(x) = - \partial_x V(x), \\
    \dot{x} &= \frac{p}{m} = \frac{\partial \mathcal{H}}{\partial p},\\
    \dot{p} &= - \partial_x V(x) = - \frac{\partial \mathcal{H}}{\partial x},
  \end{align*}
  with $x \equiv q$ we can find the Hamilton function
  \begin{align*}
    \mathcal{H} (p,q) &= \frac{p^2}{2m} + V(x).
  \end{align*}
\end{example}

In general the Hamilton function takes on the form
\begin{align*}
  \mathcal{H}(p_j,q_j) &= \sum\limits_{j=1}^{3N} \frac{p_j^2}{2 m_j} + \Phi(\{ q_j \}),
\end{align*}
where the time evolution of the system is given by \acct{Hamilton's
equations}
\begin{align*}
  \dot{q}_j &= \frac{\partial \mathcal{H}}{\partial p_j}, \\
  \dot{p}_j &= - \frac{\partial \mathcal{H}}{ \partial q_j}.
\end{align*}

\begin{example}
  Like in the second model system we assume a particle in a
  fluid. The Hamilton function is
  \begin{align*}
    \mathcal{H} &= \frac{\bm{P}^2}{2 M } + \sum\limits_{i=1}^{N} \frac{\bm{p}_i}{2m_i} + V_0(Z) + V(\bm{r}_i) + V_{\mathrm{int}}(\bm{R},\bm{r}_i),
  \end{align*}
  where we considered the fluid interaction described by the potential
  $V(\bm{r_i})$, the interaction potential
  $V_{\mathrm{int}}(\bm{R},\bm{r_i})$ between the particle and the
  atoms of the fluid and the gravity potential $V_0(Z) = m g Z$. Note
  that $\bm{P}$ is the momentum of the particle and $\bm{p}_i$ the
  momenta of the atoms of the fluid. In this system the energy is
  conserved because the time derivative of the Hamilton function
  vanishes
  \begin{align*}
    \frac{\diff \mathcal{H}}{\diff t} &= \sum\limits_{j} \frac{\partial \mathcal{H}}{\partial q_j} \dot{q_j} + \frac{\partial \mathcal{H}}{\partial p_j} \dot{p}_j = 0.
  \end{align*}
\end{example}

\begin{notice}[Notation:] 
  We can combine the $6 N$ generalized coordinates $p_j$ and $q_j$ to
  an quantity $\xi$, which is in general a vector. The Hamilton
  function expressed in terms of $\xi$ is then
  \begin{align*}
    \mathcal{H}({p_j,q_j}) =  \mathcal{H}(\xi).
  \end{align*}
  So we can solve the equation of motion with 
  \begin{align*}
    \xi(t) = \xi^t = \xi^t(\xi^0).
  \end{align*}
\end{notice}

\subsection{Liouville's theorem and flow in phase space}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin = -3, xmax = 3, 
	ymin = -1,  ymax =1,
	xtick=\empty, 
	ytick=\empty,
	ylabel = $V(q)$,
        xlabel = $q$,,
	width = 5cm
        ]
	\draw [help lines] (axis cs: -3,0) -- (axis cs: 3,0);
	\draw [help lines] (axis cs: 0,-1) -- (axis cs: 0,1.5);
	\addplot [MidnightBlue,smooth,samples=50]  {-0.7*x^2+0.2*x^4)};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,-0.8)}]
      \begin{axis}[
        xtick = \empty,
        ytick = \empty,
        xmin = -5, xmax = 5,
        ymin = -2, ymax = 2,
        samples=31,
        xlabel = $q$,
        ylabel = $p$,
        width = 7cm
        ]
        \node[dot] at (axis cs: -2,0) {};
        \node[dot] at (axis cs: 2,0) {};
        \addplot[arrow inside={pos=.5,end=<},domain=0:2*pi] ({-2+.5*cos(deg(x))},{.25*sin(deg(x))});
        \addplot[arrow inside={pos=.5,end=<},domain=0:2*pi] ({-2+1*cos(deg(x))},{.5*sin(deg(x))});
        \addplot[arrow inside={pos=0,end=>},domain=0:-2*pi] ({2+.5*cos(deg(x))},{.25*sin(deg(x))});
        \addplot[arrow inside={pos=0,end=>},domain=0:-2*pi] ({2+1*cos(deg(x))},{.5*sin(deg(x))});

        \addplot[arrow inside={pos=.5,end=>},domain=pi/2:-pi/2] ({2+1.5*cos(deg(x))},{.75*sin(deg(x))});
        \addplot[arrow inside={pos=.5,end=<},domain=pi/2:3*pi/2] ({-2+1.5*cos(deg(x))},{.75*sin(deg(x))});
        \draw[arrow inside={pos=.4,end=>}] (axis cs:-2,.75) to[out=0,in=180] (axis cs:2,-.75);
        \draw[arrow inside={pos=.4,end=<}] (axis cs:-2,-.75) to[out=0,in=180] (axis cs:2,.75);

        \addplot[arrow inside={pos=.5,end=>},domain=pi/2:-pi/2] ({2+2*cos(deg(x))},{1*sin(deg(x))});
        \addplot[arrow inside={pos=.5,end=<},domain=pi/2:3*pi/2] ({-2+2*cos(deg(x))},{1*sin(deg(x))});
        \draw[arrow inside={pos=.5,end=>}] (axis cs:-2,1) to[out=0,in=180] (axis cs:0,.75) to[out=0,in=180] (axis cs:2,1);
        \draw[arrow inside={pos=.5,end=<}] (axis cs:-2,-1) to[out=0,in=180] (axis cs:0,-.75) to[out=0,in=180]  (axis cs:2,-1);
        \draw[help lines]  (axis cs:0,-5) -- (axis cs:0,5);
        \draw[help lines]  (axis cs:-5,0) -- (axis cs:5,0);
        \addplot[fill,red!60,fill opacity=0.5] coordinates {(0.4,0.6) (0.8,0.6) (0.8,1) (0.4,1)} --cycle;
        \addplot[fill,orange!80,fill opacity=0.5] coordinates {(3.4,0.4) (3.6,0.25) (4,0.55) (3.8,0.7)} --cycle;
        \draw[arrow inside={pos=.5,end=>},dashed,MidnightBlue] (axis cs:0.5,0.78) to[out=25,in=115] (axis cs:3.75,.475);
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{(a) Mexican hat potential. (b) The corresponding phase
    space. The depicted areas describe the behavior of a volume
    element along a trajectory.}
  \label{fig:phasespace}
\end{figure}

\begin{notice}
  The vector $\bm{\xi}^t$ is also called the phase space vector which
  contains the positions $q_j$ and momenta $p_j$ spanning up the whole
  phase space.
\end{notice}

In the case of Hamiltonian systems the phase space distribution
function is a constant along all trajectories of the system. This is
the essential statement of \acct{Liouville's theorem}, i.e.\ the phase
space volume is preserved. Therefore we look at the divergence of
$\dot{\bm{\xi}}^t \equiv \bm{v}(\bm{\xi})$
\begin{align*}
  \div \bm{v} &= \sum\limits_j \partial_{\xi_j} v_j(\bm{\xi}) \\
  &= \frac{\partial \dot{q}_j}{\partial q_j} + \frac{\partial \dot{p}_j}{\partial p_j} \\
  &= \frac{\partial}{\partial q_j} \frac{\partial\mathcal{H}}{\partial p_j} + \frac{\partial}{\partial p_j} \left( - \frac{\partial \mathcal{H}}{\partial q_j}\right) = 0.
\end{align*}
In the case of a fluid a vanishing divergence of the vector $\bm{v}$
says that the fluid is incompressible. 

Given is the map
\begin{align*}
  \bm{\xi}^0 \to \bm{\xi}^t(\bm{\xi}^0). 
\end{align*}
A volume element around $\bm{\xi}^0$ is then 
\begin{align*}
  \diff \xi_1^0 \ldots \diff \xi_{N}^0,
\end{align*}
where $N$ are the degrees of freedom. 

\begin{proof}
  To proof Liouville's theorem we look at the behavior of a volume
  element in the phase space along a trajectory as for example
  depicted in figure~\ref{fig:phasespace}. We start with the time
  evolution of a volume element which is given by the matrix
  $\mathcal{J}(\bm{\xi^0,t})$ so that
  \begin{align*}
    \diff \xi_1^t \diff \xi_2^t \ldots \diff \xi_N^t = \mathcal{J}(\bm{\xi}^0,t)  \diff \xi_1^0 \diff \xi_2^0 \ldots \diff\xi_N^0. 
  \end{align*}
  Written-out in components we find
  \begin{align*}
    \diff \xi_i^t &= \mathcal{J}_{ij} \diff \xi_i^0.
  \end{align*}
  The determinant of $\mathcal{J}_{ij}$ is 
  \begin{align*}
    \mathcal{J}(\bm{\xi}^0,t) \equiv \det\mathcal{J}_{ij}(\xi^0,t).
  \end{align*}
  Rewriting the expression above leads to
  \begin{align*}
    \mathcal{J}(t) &= \det \mathcal{J}_{ij}(t) \\
    &= \ee^{\tr\left[ \log \mathcal{J}_{ij}(t) \right]}.
  \end{align*}
  The time derivative is
  \begin{align*}
    \dot{\mathcal{J}}(t) &= \mathcal{J}(t) \tr \left[\dot{\bm{\mathcal{J}}} / \bm{\mathcal{J}} \right] \\
    &= \mathcal{J} \sum\limits_i (\mathcal{J}^{-1})_{ij} \dot{\mathcal{J}}_{ij} \\
    \intertext{using $\mathcal{J}_{ij} = \diff \xi_i^t / \diff \xi_j^0$ yields}
    &= \mathcal{J} \sum\limits_{i} \frac{\partial \xi_i^0}{\partial\xi_j^t} \frac{\partial \dot{\xi}_{j}^{t}}{\partial \xi_i^0} \\
    &= \mathcal{J}(t) \sum\limits_{i} \frac{\partial \dot{\xi}_j^t}{\partial \xi_i^t} \\
    &= \mathcal{J}(t) \div \bm{v} = 0\\
    \implies \mathcal{J}(t) &= 1 \quad \forall t.
  \end{align*}
  This implies that the volume element do not change along the
  trajectory.
\end{proof}

\section{Fundamental hypothesis and microcanonical distribution}

\subsection{Fundamental hypothesis}
\index{Fundamental hypothesis}

We will assume that the $\TT \to \infty$ limit captures the physics
for finite times, in practice, for times much bigger than the
relaxation time
\begin{align*}
  \lim\limits_{\TT \to \infty} p^{\mathrm{hist}}(A|\bm{\xi}^0).
\end{align*}

The fundamental hypothesis of statistical physics is that this $\TT
\to \infty$ limit is independent of the precise initial condition
$\bm{\xi}^0$. It rather depends only on E!

\subsection{Microcanonical distribution}
\index{Microcanonical distribution}

Suppose that the fundamental hypothesis is true 
\begin{align*}
  p^{\mathrm{hist}}(A,\TT\to\infty|E) \equiv p(A|E),
\end{align*}
then we find by rewriting the expression
\begin{align*}
  p^{\mathrm{hist}} (A,\TT \to \infty |E) &= \int \diff \xi^0 \delta(\HH(\xi^0)-E) \lim\limits_{\TT \to \infty} \frac{1}{\TT} \int\limits_{0}^{\TT} \diff t \delta(A(\xi^t)-A) \frac{1}{\int \diff \xi^0 \delta(\HH(\xi^0)-E)}
  \intertext{with the definition $\Omega(R) \equiv \int \diff \xi^0 \delta(\HH(\xi^0)-E)$ (size of the energy shell) and changing the limits}
  &= \lim\limits_{\TT \to \infty} \frac{1}{\TT} \int\limits_{0}^{\TT} \diff t \int \diff \xi^0 \delta(\HH(\xi^0)-E) \delta(A(\xi^t)-A) \frac{1}{\Omega(E)}
  \intertext{rewriting $\int \diff \xi^0 = \int \diff \xi^0 \det(\partial \xi_i^0 / \partial \xi_i^t)$, where the determinant is $1$ (as calculated before) leads to}
  &= \lim\limits_{\TT\to\infty} \frac{1}{\TT} \int\limits_{0}^{\TT}\diff t \underbrace{\int \diff \xi^t \delta(\HH(\xi^t) - E)\delta(A(\xi^t)-A)}_{\text{independent of t, i.e.\ $\xi^t \to \xi^0$}} \frac{1}{\Omega(E)} \\
  \implies p(A) &= \frac{ \int \diff\xi \delta(\HH(\xi) - E)\delta(A(\xi)-A) }{\int \diff \xi \delta(\HH(\xi) -E)}.
\end{align*}

\begin{notice}
  The integration over $\delta(\HH(\xi^t)-E)$ considers all trajectories
  $\xi^t$ on the energy shell.
\end{notice}

The result from above can be rewritten as (probability distribution of
a certain observable)
\begin{align*}
  p(A) &= \int \diff \xi p(\xi) \delta(A(\xi)-A),
\end{align*}
where $p(\xi)$ is the probability for the microstate $\xi$, which is then
\begin{align*}
  p(\xi) &= \frac{\delta(\HH(\xi)-E)}{\Omega(E)}.
\end{align*}

The average value of an observable $A$ is give by
\begin{align*}
  \braket{A} &= \braket{A|E} = \int \diff A p(A) A \\
  &= \int \diff \xi A(\xi)p(\xi) \\
  &= \frac{1}{\Omega(E)} \int \diff \xi \delta(\HH(\xi)-E) A(\xi).
\end{align*}
The average value is different from the most likely value $\hat{A}$
which fulfills
\begin{align*}
  \partial_A p(A)\bigg|_{A = \hat{A}} &= 0.
\end{align*}
An example for a Gaussian distribution $p(A)$ is depicted in
figure~\ref{fig:difference}.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      ytick = \empty,
      xlabel = $A$,
      ylabel = $p(A)$,
      xmin = 0.5, xmax =4. 5,
      ymin = 0, ymax = 1.1,
      width = 5cm,
      xtick = {2.5,3},
      xticklabels = {$\braket{A}$,$\hat{A}$},
      xmajorgrids = true,
      ]
      \draw [help lines] (axis cs: 0,0) -- (axis cs: 5,0);
      \addplot [MidnightBlue,smooth,samples=50]  { 1*exp(-(x-2.5)^2/(2*0.6^2))};
    \end{axis}
  \end{tikzpicture}
  \caption{The average value in the case of a Gaussian distribution
    $p(A)$ is given by the maximum. The most likely value $\hat{A}$ is
    different.}
  \label{fig:difference}
\end{figure}

\begin{notice}
  Sometimes the notation $\braket{A|E}$ is used for the average
  value. This has nothing to do with the quantum mechanical
  $\braket{\bullet|\bullet}$ notation.
\end{notice}

\section{Kinetic energy as paradigm}

\subsection{Model: \texorpdfstring{$N$}{N} particles in a box}

The Hamilton function for $N$ particles in a box is
\begin{align*}
  \HH &= \sum\limits_{j=1}^{3N} \frac{p_j^2}{2m_j} + \Phi(\{q_j \}), 
\end{align*}
where $\Phi(\{q_j\})$ contains gravity, interaction with walls, and
intermolecular interactions. The observable of interest is the total
energy which leads to 
\begin{align*}
  A &= E^k = \sum\limits_{j=1}^{3N} \frac{p_j^2}{2 m_j}.
\end{align*}
The probability distribution is give by
\begin{align*}
  p(E^k) &= \int \diff \xi \delta\left( \sum\limits_{j=1}^{3N} \frac{p_j^2}{2m_j} + \Phi(\{q_j\}) - E  \right) \delta\left( \sum\limits_{j=1}^{3N} \frac{p_j}{2m_j} -E^k  \right) \frac{1}{\Omega(E)} \\
  &= \underbrace{\int \diff \bm{p} \delta\left( \sum\limits_{j=1}^{3N} \frac{p_j}{2m_j} -E^k  \right)}_{\equiv \Omega^k(E^k)}  \underbrace{\int \diff \bm{q} \delta\left( \Phi(\{q_j\}) - (E-E^k)  \right)}_{\equiv \Omega^c(E^0 = E-E^k)} \frac{1}{\Omega(E)} \\
  &= \frac{\Omega^k(E^k) \Omega^c(E^0)}{\Omega(E)}.
\end{align*}
Here $\Omega^k(E^k)$ is called the \acct{kinetic integral} and
$\Omega^c(E-E^k)$ the \acct{configurational integral}.

\subsection{Kinetic integral}

Now we want to discuss the kinetic integral 
\begin{align*}
  \Omega^k(E^k) &= \int\limits_{-\infty}^{\infty} \diff p_1 \ldots \diff p_{3N} \delta\left( \sum\limits_{j=1}^{3N} \frac{p_j}{2m_j} -E^k  \right) 
  \intertext{with the substitution $u_j = p_j^2/E^k $ and $\delta(ax) = \delta(x) /a$ we find}
  &= \frac{(E^k)^{3N/2}}{E^k} \int\limits_{-\infty}^{\infty} \diff u_1 \ldots \diff u_{3N} \delta\left( \sum\limits_{j=1}^{3N} \frac{u_j}{2m_j} - 1  \right) \\
  &= \left(E^k \right)^{\frac{3N}{2}-1}.
\end{align*}
Introducing the $\beta$-function
\begin{align*}
  \beta^k(E^k) &\equiv \frac{\partial_{E^k} \Omega^{k}(E^k)}{\Omega^k(E^k)} \\
  &= \partial_{E^k} \log \Omega^k(E^k) \\
  &= \frac{\frac{3N}{2}-1}{E^k}.
\end{align*}

\begin{notice}
  Here we used the following notation:
  \begin{itemize}
  \item In textbooks we find $\beta = 1/(\kB T)$. As we
    will see later the $\beta$-function is the same.
  \item Henceforth we use the abbreviation $\partial_{E^k}
    \Omega^k(E^k) \equiv \Omega^{k\prime}(E^k)$.
  \end{itemize}
\end{notice}

For large $N \gg 1$ we find
\begin{align*}
  \beta^k(E^k) &\approx \frac{3N}{2} \\
  \implies \beta^{k\prime}(E_k) &= - \frac{3N}{2(E^k)^2}.
\end{align*}
%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: