\section{Mathematical preliminaries}

In this section we want to discuss some typical mathematical
expressions and tricks which we will need for further tasks.

\subsection{Useful formula}

\begin{itemize}
\item As we will see the \acct{Gaussian integral} is omnipresent in
  stochastic thermodynamics:
  \begin{align*}
    I_G &= \int\limits_{-\infty}^{\infty} \ee^{-ax^2/2} \diff x \\
    &= \left[ \int\limits_{-\infty}^{\infty} \ee^{-ax^2/2} \diff x \int\limits_{-\infty}^{\infty} \ee^{-ay^2/2} \diff y \right]^{1/2}
    \intertext{Using polar coordinates $\diff x \diff y = \varrho \diff \phi \diff \varrho$}
    &= \left[ \int\limits_{0}^{2\pi} \diff \phi \int\limits_{0}^{\infty} \diff \varrho  \varrho \ee^{-a\varrho^2/2} \right]^{1/2} 
    \intertext{Substitution with $u=\varrho^2/2$ yields}
    &= \left[ 2 \pi \int\limits_{0}^{\infty} \ee^{-a u} \diff u \right]^{1/2}\\
    &= \sqrt{\frac{2\pi}{a}}
  \end{align*}
\item Another Gaussian integral is 
  \begin{align*}
    I_G &= \int\limits_{-\infty}^{\infty} \diff x \ee^{ax^2/2 - bx}
    \intertext{completing the squares $-ax^2/2 + bx = -a (x-b/a)^2/2 +
      b^2/(2a)$ leads to}
    &= \int\limits_{-\infty}^{\infty} \ee^{-a(x-b/a)^2/2}\ee^{b^2/(2a)} \diff x \\
    &= \sqrt{\frac{2\pi}{a}} \ee^{b^2/(2a)}
  \end{align*}
\item Given is a function $f(x)$ with a maximum at $x_0$, where
  $a<x_0< b$ and $a$, $b$ are the integration limits as depicted in
  figure~\ref{fig:max}.  
  \begin{figure}[tb]
    \centering  \begin{tikzpicture}[gfx]
      \begin{axis}[
        ytick = \empty,
        xlabel = $x$,
        ylabel = $f(x)$,
        xmin = 0, xmax = 5,
        ymin = 0, ymax = 1.25,
        width = 5cm,
        xtick = {1,2.5,4},
        xticklabels = {$a$,$x_0$,$b$},
        xmajorgrids = true,
        ]
	\draw [help lines] (axis cs: 0,0) -- (axis cs: 5,0);
        \addplot [MidnightBlue,samples=150]  { 1*exp(-(x-2.5)^2/(2*0.6^2))};
      \end{axis}
    \end{tikzpicture}
    \caption{Function with a maximum at $x_0$.  }
    \label{fig:max}
  \end{figure}
  To solve the integral
  \begin{align*}
    I &= \int\limits_{a}^{b} \ee^{Nf(x)} \diff x \\
      &= \int\limits_{a}^{b} \exp\left(N f(x_0) -
        \frac{1}{2}(x-x_0)^2f''(x_0) \right) \diff x 
        \intertext{we expand $f(x)$ and using the Gauss integral from above (\acct{saddle point integration})}
      &= \ee^{Nf(x_0)} \left( \frac{2 \pi}{Nf''(x_0)} \right)^{1/2}.
        \intertext{With the logarithm we find}
        \ln I &= N f(x_0) + \mathcal{O}(\ln N) \\
      &\approx N f(x_0).
  \end{align*}
\item With \acct{Strirling's approximation} it is possible to
  calculate the factorial of large N ($N!$). Using the Gamma function
  allows us to express the factorial as
  \begin{align*}
    N! &= \int\limits_{0}^{\infty} \ee^{-x} x^N \diff x \\
    &= \int\limits_{0}^{\infty} \ee^{N(\log x - x/N)} \diff x \\
    & \approx \ee^{N(\log N -1)} \int\limits_{-\infty}^{\infty} \ee^{-Ny^2/(2N^2)} \diff y \\
    &= \ee^{N(\log N -1)}\sqrt{2 \pi N}.
  \end{align*}
  Hence Stirlings approximation is given by 
  \begin{align*}
    \boxed{ \log(N!) = N (\log N -1) + \mathcal{O}(\log N) }
  \end{align*}
\item The delta function $\delta(x)$, also known as Dirac delta
  distribution, fulfills the identity
  \begin{align*}
    \int\limits_{-\infty}^{\infty} \delta(x) \diff x = 1.
  \end{align*}
  The Fourier representation leads to
  \begin{align*}
    \delta(x) &= \int\limits_{-\infty}^{\infty} \frac{\diff k}{2 \pi} \ee^{\ii k x}, \\
    \delta(x) f(x) &= \delta(x) f(0).
  \end{align*}
\item The \acct{Heavisied step function} is a discontinuous function
  which is defined as
  \begin{align*}
    \Theta(x) \equiv
    \begin{cases}
      0 &,x<0\\
      \frac{1}{2} &, x=0 \\
      1 &,x>0
    \end{cases}.
  \end{align*}
  The step function is depicted in figure~\ref{fig:heaviside} .
  \begin{figure}[tb]
    \centering  \begin{tikzpicture}[gfx]
      \begin{axis}[
        xtick = \empty,
        ytick = \empty,
        xlabel = $x$,
        ylabel = $\Theta(x)$,
        xmin = -2,	xmax = 2,
        ymin = -0.25,	ymax = 1.25,
        width = 5cm
        ]
        \draw[help lines] (axis cs:-2,0) -- (axis cs: 2,0);
        \draw[help lines] (axis cs:0,-2) -- (axis cs: 0,2);
        \draw[MidnightBlue] (axis cs: -2,0) -| (axis cs: 0,1) -- (axis cs:2,1) ;
      \end{axis}
    \end{tikzpicture}
    \caption{Heaviside function $\Theta(x)$.}
    \label{fig:heaviside}
  \end{figure}
  The Heaviside function is the integral of the delta function. This
  is written as
  \begin{align*}
    \Theta(x) &= \int\limits_{-\infty}^{x} \delta(x') \diff x'.
  \end{align*}
\end{itemize}

\subsection{Probability distribution}
\index{Probability distribution}

\begin{itemize}
\item Discrete case: Given is a set of events
  $\{\alpha_1,\alpha_2,\ldots \alpha_N\}$. The probability for a
  certain event is then give by
  \begin{align*}
    p(\alpha_i) &= \frac{\text{\# occurrences}}{\text{\# possible events}}.
  \end{align*}
  The sum over all probabilities is normalized, i.e.\ 
  \begin{align*}
    \sum_i p_i &= 1, \\
    p(\alpha_i \cap \beta_i) &= p(\alpha_i) p(\beta_i).
  \end{align*}
\item Continuous case: Given is a space of events $x_a \le x \le x_b$
  with $x_a = - \infty$ and $x_b= \infty$. The probability
  distribution $p(x)$ fulfills 
  \begin{align*}
    \int\limits_{x_a}^{x_b} p(x) \diff x = 1,
  \end{align*}
  which corresponds to the normalization of the distribution. The
  probability distribution has the physical dimension
  $[p(x)] = 1/[x]$, where $x$ is the position. In the case
  $\bm{x}\in\mathbb{R}^{N}$ we can extend the above result, so we find
  \begin{align*}
    \int p(\bm{x}) \diff \bm{x} &= \int p(\bm{x}) \diff x_1 \diff x_2 \ldots \diff x_N = 1.
  \end{align*}
\item The \acct{mean value} of a function $f(x)$ is
  \begin{align*}
    \mean{f_\alpha(x)} &\equiv \braket{f_\alpha(x)} = \int f(x) p(x) \diff x .
  \end{align*}
  The probability at $x_0$ is given by
  \begin{align*}
    p(f_0) &= \mean{\delta(f(x)) - f_0)} = \int \delta(f(x) - x_0) p(x) \diff x . 
  \end{align*}
\item As we will later see it is helpful to define the \acct{$n$-th
    moment} of $p(x)$ as
  \begin{align*}
    M_n &\equiv \int\limits_{x_a}^{x_b} x^n p(x) \diff x = \mean{x^n}, \\
    (\Delta x)^2 &= M_2 -M_1^2 = \mean{x^2} - \mean{x}^2.
  \end{align*}
  Hence we can express the relative fluctuation as
  \begin{align*}
    \frac{\Delta x}{ \mean{x}} &= \frac{\sqrt{(\Delta x)^2}}{\mean{x}}.
  \end{align*}
\item In probability theory, the \acct{characteristic function}
  $\chi(k)$ of any probability distribution $p(x)$ is given by
  \begin{align*}
    \chi(k) &= \mean{\ee^{\ii k x}} = \int\limits_{x_a}^{x_b} \ee^{\ii k x} p(x) \diff(x) \\
    &= \int\limits_{x_a}^{x_b} \left( 1 - \ii kx + \frac{(\ii k)^2}{2} x^2 + \ldots \right) p(x) \diff(x) \\
    &= \sum\limits_{i=0}^{\infty} \frac{(-\ii k)^n}{n!} M_n.
  \end{align*}
  Note that
  \begin{align*}
    p(x) = \int\limits_{x_A}^{x_B} \chi(k) \ee^{\ii k x} \diff k.
  \end{align*}
\item The \acct{cumulants} $C_n$ are defined as 
  \begin{align*}
    \chi(k) &= \exp\bigg[ \sum\limits_{n=o}^{\infty} \frac{(\ii k)^n}{n!}  C_n  \bigg].
  \end{align*}
  Because the determination of the cumulant is sometimes a bit
  cumbersome here are the first three cumulants given
  \begin{align*}
    C_1 &= M_1, \\
    C_2 &= M_2 -M_1^2, \\
    C_3 &= M_3 - 3M_1M_2 + 2 M_1^3.
  \end{align*}
\item The correlation function for two variables $x_i$, $x_j$ with the
  probability distributions $p(x_i)$ and $p(x_j)$ is defined as
  \begin{align*}
    C_{ij} \equiv \mean{\left( x_i - \mean{x}_i  \right) \left( x_j - \mean{x}_j \right)}.
  \end{align*}
\end{itemize}

\subsection{Central limit theorem}
\index{Central limit theorem}

The variable of interest is
\begin{align*}
  X &= \sum_i x_i,
\end{align*}
which is the sum over the independent random variables $x_i$. The
central limit theorem shows that the probability distribution of $X$
is given by a Gaussian distribution
\begin{align*}
  p(x) &= \frac{1}{\sqrt{2 \pi (\Delta x)^2}} \exp\left(-\frac{(x-\mean{x})^2}{2 (\Delta x)^2}\right).
\end{align*}
Of course the mean value, the squared mean value, and the variance of
$X$ are 
\begin{align*}
  \mean{X} &= \sum_i \mean{x_i} = N \mean{x_i} \\
  \mean{X^2} &= \mean{\bigg( \sum_i x_i \bigg)^2} 
  = \sum_{i,j} \mean{x_i x_j}
  = \sum_{i=j} \mean{x_i^2} + \sum_{i\neq j} \mean{x_i} \mean{x_j} \\
  \implies (\Delta X)^2  &= \mean{(X - \mean{X})^2} 
  = \mean{X^2} - \mean{X}^2 
  = N (\Delta X)^2
\end{align*}
The relative fluctuation is then 
\begin{align*}
  \frac{\Delta X}{ \mean{X}} &= \frac{\sqrt{N (\Delta X)^2}}{N \mean{X}}\\
  &= \frac{1}{\sqrt{N}} \frac{(\Delta X)}{\mean{X}}.
\end{align*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: