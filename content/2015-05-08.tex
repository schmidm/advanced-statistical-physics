\chapter{Volume variation and pressure}

\section{Microscopic expression for pressure}

\subsection{A Model}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \foreach \i in {1,...,500}
    \fill [MidnightBlue] (rnd*1.5 cm, rnd*-1cm) circle (.25pt);
    \draw (-1.5,2) -- (-1.5,-2) --(3,-2) -- (3,2)  -- cycle;
    \draw [fill, DimGray!75] (0.5,0) -- (1,0) --(1,0.5) node[above] {$M$} -- (0.5,0.5)  -- cycle;
    \draw (0,1) -- (0,-1) -- (1.5,-1) -- (1.5,1) node[right,midway] {$z$}   (0,0) --(1.5,0) ;
    \draw [->]  (-0.5,1) -- (-0.5,0) node[left,midway] {$g$}; 
  \end{tikzpicture} 
  \caption{Model system: An isolated box in the gravitation field with
    a smaller one in it, where a piston in the height $z$ pushes over
    a wall a fluid consisting of $N$ particles.}
  \label{fig:8.5.15-1}
\end{figure}

Given is a isolated box with $N$ particles and a piston with the mass
$M$ in the gravitation field of the earth as depicted in
figure~\ref{fig:8.5.15-1}. The Hamilton function of the whole system
is given by
\begin{align*}
  \HH(\bm{\xi},P,Z) &= \HH_0(\bm{\xi},Z) +
\HH_{\mathrm{int}}(\bm{q},Z) + \frac{P^2}{2M} - MgZ,
\end{align*}
where $\HH_0(\bm{\xi},Z)$ describes the particles in the box,
$\HH_{\mathrm{int}}(\bm{q},Z)$ its interactions with the wall, and
$P^2/(2M)+gMZ$ the piston. The probability to observe the piston at
height $Z$ is
\begin{align}
  p(Z) &\propto \int \diff \bm{\xi} \int \diff P \delta \left(\HH^k(\bm{p}) +\Phi_0(\bm{q},Z) + \HH_{\mathrm{int}}(\bm{q},Z) + \frac{P^2}{2M} - MgZ - E\right) \nonumber
  \intertext{integrating over $\diff \bm{p}$ leads to}
  &= \int \diff \bm{\xi} \int \diff E^c \int \diff P \delta(\Phi(\bm{q},Z) - E^c) \delta\left(E-E^c-gMZ-\HH_{\mathrm{int}} - \frac{P^2}{2M} \right)\nonumber
  \intertext{with the approximation of the last $\delta$-function we obtain}
  &\approx \int \diff \bm{\xi} \int \diff E^c \int \diff P \delta(\Phi(\bm{q},Z) - E^c) \exp\left( -\beta^k(E-E^c-gMZ) \left[ \HH_{\mathrm{int}} + \frac{P^2}{2M} \right] \right) \Omega^k(E^c)\nonumber
  \intertext{with $E^k \equiv E-E^c-gMZ$ and the configurational average (integrating $\diff \bm{q}$) the integral takes on the form}
  &= \int \diff E^c \left( \frac{1}{\beta^k(E^k)} \right)^{1/2} \braket{\ee^{-\beta \HH_{\mathrm{int}}}|Z,E^c}^c \Omega^c(E^c,Z) \Omega^k(E^k) \nonumber
  \intertext{a saddle point integration leads to}
  &\approx \left(\frac{1}{\beta(\hat{E}^k)} \right)^{1/2} \braket{\ee^{-\beta \HH_{\mathrm{int}}}|Z,\hat{E}^k}^c \Omega^c(\hat{E}^k,Z) \Omega^k(\hat{E}^k). \label{eq:8.5.15-1} 
\end{align}

\subsection{Special case: Ideal gas}

In the case of a \acct{ideal gas} $\Omega^c$ becomes independent of
$E^c = 0$. Then the configurational integral is
\begin{align*}
  \Omega^c(E^c,Z) &= \int \diff \bm{q} = V^N.
\end{align*}
With the assumption of a hard wall $\HH_{\mathrm{int}}=0$ the
probability distribution to observe the piston at height $Z$ takes on
the form
\begin{align*}
  p(Z) & \propto V^N (E-MgZ)^{\frac{3N}{2}} = V^N (E^k)^{\frac{3N}{2}},
\end{align*}
where $V^N = (A Z)^N$ and $E-MgZ = E^k$. Note that the ideal gas is in
general not thermodynamically consistent. Hence we need a very week
interaction, e.g.\ indirectly by a hard wall (piston). The most
likely value $\hat{Z}$ is
\begin{align*}
  \partial_Z p(Z) &= 0 \\
  \implies \frac{N}{Z} &= \frac{3N}{2E^k(\hat{Z})} Mg.
\end{align*}
The external pressure is then 
\begin{align*}
  p^{\mathrm{ex}} &= \frac{Mg}{A} = \frac{Mg\hat{Z}}{V} \\
  &= \frac{2}{3} \frac{E^k(\hat{Z})}{\hat{V}} = \frac{N}{\beta V}.
\end{align*}
The (internal) thermodynamic pressure is equal to the external
pressure $p \stackrel{!}{=} p^{\mathrm{ex}}$, which then gives the
\acct{ideal gas law}.

\subsection{General case}

From equation~\ref{eq:8.5.15-1} we know
\begin{align*}
  p(Z) &\propto \int \diff E^c \frac{1}{\beta} \braket{\ee^{-\beta \HH_{\mathrm{int}}}} \Omega^c(E^c,Z) \Omega^k(E-E^c-MgZ),
  \intertext{where the product $\Omega^c \Omega^k$ gives the total $\Omega$ function}
  &\propto \Omega(E-MgZ,Z).
\end{align*}
The peak value is
\begin{align*}
  \partial_Z p(Z) &= 0 \\
  \implies \frac{\partial \Omega}{\partial E} (-Mg) + \frac{\partial \Omega}{\partial V} A &= 0,
  \intertext{where we used the logarithmic derivative. Hence}
  \frac{Mg}{A} \beta(E) &= \frac{\partial \log \Omega}{\partial V}.
\end{align*}
The external pressure is then
\begin{align*}
  \boxed{ p^{\mathrm{ex}} = \frac{1}{\beta(E)} \frac{\partial \log \Omega}{\partial V} \equiv \frac{\gamma(E,V)}{\beta(E,V)}}
\end{align*}
For any (large) system with given $\Omega(E,V) = \int_V \diff \xi
\delta(H - E)$ the thermodynamic pressure is
\begin{align*}
  p(E,V) &= \frac{\gamma(E,V)}{\beta(E,V)},
\end{align*}
which is the microscopic expression for the pressure.

\begin{notice}
  The $\gamma(E,V)$- and $\beta(E,V)$-functions are defined as
  \begin{align*}
    \gamma(E,V) &\equiv \frac{\partial_V \Omega(E,V)}{\Omega(E,V)} \\
    \beta(E,V) &\equiv \frac{\partial_E \Omega(E,V}{\Omega(E,V)}.
  \end{align*}
\end{notice}

\section{Pressure for a system in contact with a heat bath}

Given is a box with $N$ particles and a piston of the mass $M$. The
whole system is coupled to a heat bath of temperature $\beta$. Between
the heat bath and the small system there is weak coupling allowing
energy exchange. The whole system is depicted in
figure~\ref{fig:8.5.15-2}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \foreach \i in {1,...,500}
    \fill [MidnightBlue] (rnd*1.5 cm, rnd*-1cm) circle (.25pt);
    \draw [fill opacity=.3,fill, DarkOrange3] (-1.5,-0.2) -- (-1.5,-2) --(3,-2) -- (3,-0.2) -- (1.5,-0.2) -- (1.5,-1) -- (0,-1) --(0,-.2)  -- cycle;
    \node[DarkOrange3] at (2,-1.75) {heat bath};
    \draw [fill,DimGray!75] (0.5,0) -- (1,0) --(1,0.5) node[above] {$M$} -- (0.5,0.5)  -- cycle;
    \draw (0,1) -- (0,-1) -- (1.5,-1) -- (1.5,1) node[right,midway] {$z$}   (0,0) --(1.5,0) ;
	\draw [->]  (-0.5,1) -- (-0.5,0) node[left,midway] {$g$}; 
	\draw [<->,DarkOrange3] (0.2,-0.5)--(0.25,-1.5);
  \end{tikzpicture} 
  \caption{Box with $N$ particles and a piston with mass $M$ coupled
    to an external heat bath.}
  \label{fig:8.5.15-2}
\end{figure}
The probability given by the canonical distribution
\begin{align*}
  p(\bm{\xi},Z,P) &\propto \exp\left(-\beta\left[ \HH(\xi,Z) + \HH_{\mathrm{int}}(\bm{q},Z) + \frac{P^2}{2M} + MgZ \right]\right), 
  \intertext{where $\tilde{\HH}(\xi,Z)=\HH(\xi,Z) + \HH_{\mathrm{int}}(\bm{q},Z)$ describes the particles in the box. Hence we can write }
  &= \exp\left(-\beta\left[ \tilde{\HH}(\xi,Z)+ \frac{P^2}{2M} + MgZ \right]\right).
\end{align*}
The probability to observe the piston at the height $Z$ is then 
\begin{align*}
  p(Z) &\propto \int \diff P \int \diff \bm{\xi} p(\bm{\xi},Z,P),
\end{align*}
which is trivial to calculate. Rewriting leads to
\begin{align*}
  p(Z) &\propto \sum\limits_{\bm{\xi}} \exp\left(-\beta  \tilde{\HH}(\bm{\xi},Z) - \beta MgZ  \right) \\
  &= \exp\left( F(Z) - \beta MgZ \right),
  \intertext{where $\exp(\beta F) = Z$ is the partition function. With the external pressure one finds}
  &= \exp\left(-\beta F(V) - \beta p^{\mathrm{ex}} V \right). 
\end{align*}
The most likely value $\hat{V}$ is 
\begin{align*}
  p^{\mathrm{ex}} &\stackrel{!}{=} \frac{\partial F}{\partial V}\bigg|_{\hat{V}} = p(\beta,V),
\end{align*}
with the thermodynamic pressure $p(\beta,V)$. For large system we
obtain the equivalence of the ensembles
\begin{align*}
  \frac{\gamma(E,V)}{\beta(E,V)} &= - \frac{\partial F}{\partial V}.
\end{align*}

\section{\texorpdfstring{``Small''}{“Small”} system with a heat and volume reservoir}

Given is an isolated box with temperature $\beta$ and pressure
$P$. Inside the box we place a small system with volume $V_0$ and
Hamilton function $\HH_0$ as depicted in figure~\ref{fig:8.5.15-2}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \draw [DimGray, line width=0.1cm] (-3,1.5) -- (-3,-1.5) --(3,-1.5) -- (3,1.5) -- cycle;
    \draw [DimGray] (-1,0.5) circle[radius=0.75cm];
    \node  at (2.5,-1.25) {$\beta$, $p$};
    \node  at (-1,0.5) {$\HH_0$, $V_0$};
  \end{tikzpicture} 
  \caption{Isolated box with temperature $\beta$ and $P$. Inside the
    box we place a small system with volume $V_0$ and Hamilton
    function $\HH_0$.}
\end{figure}
The Hamilton function and the volume are given by
\begin{align*}
  \HH &= \HH_0 + \HH_{\mathrm{res},} \\
  V &= V_0 + V_{\mathrm{res}},
\end{align*}
which are both conserved. The probability for the small system to be
in a microstate $\xi_0$ at volume $V_0$ (where we neglect an explicit
interaction for simplification) is
\begin{align*}
  p(\xi_0,V_0) & \propto \int \diff \xi \bigg|_{V-V_0} \delta (\HH_0 + \HH_{\mathrm{res}}-E) \\
  &= \Omega^{\mathrm{res}}(E-\HH_0,V-V_0) 
\end{align*}
because $V_0$ is small compared to $V$ we can expand the function with 
\[ \Omega(E+\Delta E) = \exp(\beta \Delta E) \Omega(E)\] and obtain
\begin{align*}
  p(\xi_0,V_0) &\approx \ee^{-\beta(\HH_0 + pV_0)} \Omega^{\mathrm{res}}(E,V).
\end{align*}
With normalization the probability is 
\begin{align*}
  p(\xi_0,V_0) &= \ee^{-\beta(\HH_0 + pV) + G(\beta,p)}, 
\end{align*}
where we introduced \acct{Gibbs free energy} $G$
\begin{align*}
  \ee^{-\beta G(\beta,p)} \equiv \sum\limits_{\xi_0} \ee^{-\beta(\HH+pV)}.
\end{align*}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: