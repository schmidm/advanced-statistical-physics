\subsection{Renormalization group for the two dimensional Ising model}

\begin{figure}[tb]
  \centering
  \def\cross[#1] at (#2,#3);{
    \draw[DarkOrange3,#1] ({#2-.1},{#3-.1}) -- ({#2+.1},{#3+.1});
    \draw[DarkOrange3,#1] ({#2-.1},{#3+.1}) -- ({#2+.1},{#3-.1});
    \node[DarkOrange3,draw,fill,circle,inner sep=.5pt] at ({#2},{#3}) {};
  }
  \begin{tikzpicture}[gfx,scale=0.75]
    \draw[dashed] (2,4)--(1,3)--(2,2)--(3,3) -- cycle ;
    \draw[help lines] (-.5,-.5) grid (5.5,5.5);
    \foreach \x in {1,...,3}
    \draw[MidnightBlue,fill] (2*\x-1,5) circle (1.5pt);		
    \foreach \x in {0,...,2}
    \cross[] at  (2*\x,5);
    \foreach \x in {0,...,2}
    \draw[MidnightBlue,fill] (2*\x,4) circle (1.5pt);			
    \foreach \x in {1,...,3}
    \cross[] at  (2*\x-1,4);
    \foreach \x in {1,...,3}
    \draw[MidnightBlue,fill] (2*\x-1,3) circle (1.5pt);			
    \foreach \x in {0,...,2}
    \cross[] at  (2*\x,3);
    \foreach \x in {0,...,2}
    \draw[MidnightBlue,fill] (2*\x,2) circle (1.5pt);			 
    \foreach \x in {1,...,3}
    \cross[] at  (2*\x-1,2);
    \foreach \x in {1,...,3}
    \draw[MidnightBlue,fill] (2*\x-1,1) circle (1.5pt);			
    \foreach \x in {0,...,2}
    \cross[] at  (2*\x,1);
    \foreach \x in {0,...,2}
    \draw[MidnightBlue,fill] (2*\x,0) circle (1.5pt);			 
    \foreach \x in {1,...,3}
    \cross[] at  (2*\x-1,0);
    \node [DarkOrange3] at (2.2,2.7) {$0$};
    \node [MidnightBlue] at (2.2,3.7) {$1$};
    \node [MidnightBlue] at (3.2,2.7) {$2$};
    \node [MidnightBlue] at (1.2,2.7) {$3$};
    \node [MidnightBlue] at (2.2,1.7) {$4$};  
  \end{tikzpicture}
  \caption{Two dimensional Ising model. Quadratic spin lattice which
    is rotated by $\ang{45}$. The sites of the lattice are depicted by
    points . By using the decimation procedure we want to eliminate
    the spins on the sites marked with a cross.}
  \label{fig:3.7.15-1}
\end{figure}

Much more interesting is this decimation procedure for the two
dimensional Ising model due to the phase transition by a finite
temperature $T_c >0$. To do so we concentrate on the rotated quadratic
lattice as depicted in figure~\ref{fig:3.7.15-1}. The Hamiltonian
multiplied by $\beta$ is
\begin{align*}
  \HH &= - \sum\limits_{\text{n.n.}} K s_i s_j,
\end{align*}
where ``n.n.'' is the abbreviation for ``next neighbors''. To
calculate the partition function we concentrate on one single spin
(marked by a cross) and numerate the adjacent spins with $1$, $2$,
$3$, and $4$. His amount to the partition function is given by
\begin{align*}
   \sum\limits_{s_0 = \pm 1} \ee^{K (s_0s_1+s_0s_2+s_0s_3+s_0s_4) } 
  &= \ee^{K (s_1+s_2+s_3+s_4)}+\ee^{-K(s_1+s_2+s_3+s_4)} \\
  &\stackrel{!}{=} \ee^{K'(s_1s_2+s_2s_3+s_3s_4+s_4s_1) + A},
\end{align*}
where the last equal sign is what we want (like in the one dimensional
case). The problem is know that we have just 2 parameters ($K'$, $A$)
to satisfy 16 equations (without symmetries). Hence we have more
equations than free parameter. By introducing new couplings we can get
rid of this inconvenience. The result we now want to have takes on the
form
\begin{align*}
  \sum\limits_{s_0 = \pm 1} \ee^{K (s_0s_1+s_0s_2+s_0s_3+s_0s_4) } &= \ee^{\frac{K'}{2} (s_1s_2+s_2s_3+s_3s_4+s_4s_1) + L' (s_1s_3+s_2s_4) + M' s_1s_2S_3s_4 + A'}.
\end{align*}
This transformation considers after next neighbor spin interaction and
four spin interaction. The advantage of this procedure is that we know
deal with 4 parameter ($K'$, $L'$, $A'$, $M'$) that have to satisfy 4
equations (with symmetries). The coefficients as function of $K$ are
given by
\begin{align*}
  L'(K) &= \frac{1}{2} K'(K), \\
  K'(K) &= \frac{1}{4} \log\left( \cosh(4K) \right),\\
  M'(K) &= \frac{1}{8} \left[ \log(\cosh(4K)) - 4 \log(\cosh(2K))  \right] ,\\
  A'(K) &= \log 2 + \frac{1}{2} \left[ \log(\cosh(4K)) + 4 \log(\cosh(2K))  \right].
\end{align*}
Here another problem appears because the next step will generate
further new couplings. To make progress we have to truncate higher
order terms. Subsequent we want to use a very crude truncation by
neglecting the $M'$ coupling and replace $L'$ by increasing the $K'$
by
\begin{align*}
  L' \coloneq K+L' = \frac{3}{8} \log\left( \cosh(4K) \right),
\end{align*}
which is incidentally correct for $T=0$. We then find
\begin{align*}
  K' \approx
  \begin{cases}
    3 K^2 &,K \ll 1 \\
    \frac{3}{2}K + \const &, K \gg 1
  \end{cases}
\end{align*}
\begin{figure}[tb]
  \centering
  \usetikzlibrary{intersections}
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xmin = 0, xmax = 0.75, 
      ymin = 0, ymax =0.75,
      xtick=\empty, 
      ytick=\empty,
      ylabel = $K'$,
      ylabel=$K$,
      width = 6cm,
      xtick = {0.507},
      xticklabels = {$K^\ast$},
      % xmajorgrids = true
      ]
      \draw [help lines] (axis cs: 0.507,0) -- (axis cs: 0.507, 0.507 );
      % Left flow
      \draw [help lines] (axis cs: 0.4,0) -- (axis cs: 0.4,0.355 );
      \draw [help lines,->] (axis cs: 0.4,0.355) -- (axis cs: 0.355,0.355 );  
      \draw [help lines] (axis cs: 0.355,0) -- (axis cs: 0.355,0.355 );
      \draw [help lines,->] (axis cs: 0.355,0.293) -- (axis cs: 0.293,0.293 );
      \draw [help lines] (axis cs: 0.355,0.293) -- (axis cs: 0.293,0.293 );
      \draw [help lines] (axis cs: 0.293,0) -- (axis cs: 0.293,0.293 );
      \draw [help lines,->] (axis cs: 0.293,0.213) -- (axis cs: 0.213,0.213 );
      \draw [help lines] (axis cs: 0.213,0) -- (axis cs: 0.213,0.213 );
      \draw [help lines,->] (axis cs: 0.213,0.122) -- (axis cs: 0.122,0.122 );
      \draw [help lines] (axis cs: 0.122,0) -- (axis cs: 0.122,0.122 );
      \draw [help lines,->] (axis cs: 0.122,0.043) -- (axis cs: 0.043,0.043 );
      % right flow
      \draw [help lines] (axis cs: 0.6,0) -- (axis cs: 0.6,0.643 );
      \draw [help lines,->] (axis cs: 0.6,0.643) -- (axis cs: 0.643,0.643 );
      \draw [help lines] (axis cs: 0.643,0) -- (axis cs: 0.643,0.706 );
      \draw [help lines,->] (axis cs: 0.643,0.706) -- (axis cs: 0.706,0.706 );
      % Rest
      \draw[<-,Purple]  (axis cs: 0.1,0.507) -- (axis cs: 0.457,0.507 );
      \draw[->,Purple]  (axis cs: 0.557,0.507) -- (axis cs: 0.7,0.507 );
      \addplot [name path=line 1,samples=10]  {x};
      \addplot [name path=line 2,DarkOrange3,samples=75,smooth]  {(3/8)*ln(cosh(4*x))};
    \end{axis}
  \end{tikzpicture} 
  \caption{Iteration map of $K'$ over $K$. Depending where we have
    started the renormalization flow (purple) seeks to the fix point
    $K^\ast = 0$ or $K^\ast = \infty$.}
  \label{fig:3.7.15-2}
\end{figure}
As depicted in figure~\ref{fig:3.7.15-2} we have three different fix
points:
\begin{enumerate}
\item $K^\ast = 0$ which corresponds to $T = \infty$ (attractive fix point).
\item $K^\ast = \infty$ which corresponds to $T = 0$ (attractive fix point).
\item $K^\ast = 0.597$ which corresponds to $T = T_c$ (attractive fix point).
\end{enumerate}
The correlation length then is given by
\begin{align*}
  \xi(K') &= \frac{1}{\sqrt{2}} \xi(K).
\end{align*}
If $K$ is fix point, i.e.\ $K = K' = K^\ast$ the correlation length
fulfills the equation
\begin{align*}
  \xi(K^\ast)  = \frac{1}{\sqrt{2}}\xi(K^\ast).
\end{align*}

\subsection{Calculating non-trivial exponents}

From the recursion we can determine the partition function and the
free energy
\begin{align}
  \mathcal{Z}_{N}(K) &= \left(\ee^{A'} \right)^{N/2} \mathcal{Z}_{\frac{N}{2}}(K'),\\
  f(K) &= -\frac{1}{N} \log \mathcal{Z}_N(K) = -\frac{A'}{2} + \frac{1}{2} f(K').
\end{align}
Inverting the function leads to
\begin{align}
  f(K') &= 2 f(K) + A'(K). \label{eq:3.7.15-1}
\end{align}
The scaling form around $T_c$ is (what we expect)
\begin{align}
  f(T-T_c) &= B_{\pm} \left| T-T_c \right|^{2-\alpha} + f_{\mathrm{reg}}(T) , \\
  f(K-K^\ast) &= B_{\pm} \left| K-K^\ast \right|^{2-\alpha} + f_{\mathrm{reg}}(K). \label{eq:3.7.15-2}
\end{align}
Recursion around $K^\ast$ (expansion)
\begin{align}
  K'(K) &= K'(K^\ast + K - K^\ast) \nonumber \\
  &= K^\ast + (K-K^\ast) \frac{\partial K'}{\partial K}\big|_{K^\ast} + \text{h.o.} \label{eq:3.7.15-3}
\end{align}
Hence we can rewrite the free energy
\begin{align}
  f(K') &= 2 f(K) + A'(K) \nonumber \\
  &= B_{\pm} \left| K'-K^\ast \right|^{2-\alpha} + f_{\mathrm{reg}}(K') \nonumber \\
  &= 2 B_{\pm} \left|K - K^\ast \right|^{2-\alpha} + f_{\mathrm{reg}}(K) + A'(K), \label{eq:3.7.15-4}
\end{align}
where we used equation~\eqref{eq:3.7.15-2} to rewrite
equation~\eqref{eq:3.7.15-1}. Rewriting $K-K^\ast$ with
equation~\eqref{eq:3.7.15-3} then gives
\begin{align}
  B_\pm &= \left| (K-K^\ast) \frac{\partial K'}{\partial K}\bigg|_{K^\ast} \right|^{2-\alpha} + f_{\mathrm{reg}} (K'). \label{eq:3.7.15-5}
\end{align}
Because the singular parts have to compensate each other we get
\begin{align*}
  \left|\frac{\partial K'}{\partial K}\bigg|_{K^\ast}\right|^{2-\alpha} \stackrel{!}{=} 0.
\end{align*}
Here we compared equation~\eqref{eq:3.7.15-4} with
equation~\eqref{eq:3.7.15-5}. Consequently we found the critical
exponent $\alpha$ which is given by
\begin{align*}
  2 - \alpha &= \log \left[2 \left( \frac{\partial K'}{\partial K} \bigg|_{K^\ast} \right)^{-1} \right].
\end{align*}
An explicit calculation leads to
\begin{align*}
  \boxed{\alpha = 0.138}
\end{align*}


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: