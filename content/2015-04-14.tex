\part{Foundations}

\chapter{From classical mechanics to statistical physics}

\section{Three model systems}

\subsection{Model system 1: Classical particle in a box}

\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}[gfx,scale=0.85]
    \begin{scope}[shift={(0,-2)},x={(1cm,0cm)},y={(45:1cm)},z={(0cm,1cm)}]
      \draw[->] (0,0,0) -- (1,0,0) node[below] {$x$};
      \draw[->] (0,0,0) -- (0,1,0) node[above right] {$y$};
      \draw[->] (0,0,0) -- (0,0,1) node[above] {$z$};
    \end{scope}
    \begin{scope}[shift={(0,0)},x={(1cm,0cm)},y={(45:.5cm)},z={(0cm,1cm)}]
      \draw (0,0,0) -- (0,0,1) -- (1,0,1) -- (1,0,0) -- cycle;
      \draw (1,0,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- (0,0,1);
      \draw (1,0,1) -- (1,1,1);
      \node[dot] at (.5,0,.5) {};
      \draw[->] (-.2,0,.75) -- node[left] {$g$} (-.2,0,.25);
      \node (a) at (-.5,0,2) {(a)};
    \end{scope}
    \begin{scope}[shift={(2.7,0)},x={(1cm,0cm)},y={(-45:.75cm)},z={(0cm,1.5cm)}]
      \draw[->] (0,0,0) -- (5.5,0,0) node[below] {$t$};
      \draw[->] (0,-1.2,0) -- (0,1.2,0) node[below right] {$v$};
      \draw[->] (0,0,0) -- (0,0,1.2) node[above] {$z$};
      \draw[DarkOrange3] (0,0,0) -- (1,1,0);
      \draw[DarkOrange3,dashed] (1,1,0) -- (1,-1,0);
      \draw[DarkOrange3] (1,-1,0) -- (3,1,0);
      \draw[DarkOrange3,dashed] (3,1,0) -- (3,-1,0);
      \draw[DarkOrange3] (3,-1,0) -- (5,1,0);
      \draw[MidnightBlue] plot[domain=0:1] (\x,0,{-(\x)^2+1});
      \draw[MidnightBlue] plot[domain=1:3] (\x,0,{-(\x-2)^2+1});
      \draw[MidnightBlue] plot[domain=3:5] (\x,0,{-(\x-4)^2+1});
      \node at (-.75,0,0 |- a) {(b)};
    \end{scope}
    \begin{scope}[shift={(9,0)}]
      \fill[MidnightBlue!20] (0,0) to[in=180] (3,1.5) -- (0,1.5) -- cycle;
      \draw[MidnightBlue] (0,0) to[in=180] (3,1.5);
      \draw[<->] (0,2) node[left] {$z$} |- (3,0) node[below] {$p(z)$};
      \node at (-.75,0,0 |- a) {(c)};
    \end{scope}
  \end{tikzpicture}
  \caption{(a) Model system 1: Particle in a box with gravitation
    force. (b) Position and velocity of the particle as function of
    $t$. (c) Position $z$ over $p(z)$. }
  \label{fig:mod1}
\end{figure}
Given is a classical particle confined by a box in the gravitation
field of the earth as depicted in figure~\ref{fig:mod1} (a). In part
(b) we can see the corresponding motion over the time. In this example
we draw the initial conditions
\begin{align*}
  x(0) &= x^0 , \quad z(0) = z^0 , \\
  y(0) &= y^0 , \quad \dot{x}^0=\dot{y}^0 = \dot{z}^0. 
\end{align*}
Hence we get six values $\xi^0$. The \acct{Histogram} from time series
is defined as
\begin{align*}
  p^{\mathrm{hist}}(z|\mathcal{T},\xi^0) &\equiv \frac{1}{\mathcal{T}} \int\limits_{0}^{\mathcal{T}} \delta\left( z(t) - z \right) \diff t ,
\end{align*}
where $z(t)$ is the trajectory of the particle. For the velocity
(figure~\ref{fig:hist-vel}) we find
\begin{align*}
  p^{\mathrm{hist}} (v_z|\mathcal{T},\xi^0) &\equiv \frac{1}{\mathcal{T}} \int\limits_{0}^{\mathcal{T}} \delta\left( v_z(t) -v_z \right) \diff t.
\end{align*}

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xtick = \empty,
      ytick = \empty,
      xlabel = $v_z$,
      ylabel = $p^ {\mathrm{hist}}$,
      xmin = -2,	xmax = 2,
      ymin = -0.25,	ymax = 1.25,
      width = 5cm
      ]
      \draw[help lines] (axis cs:-2,0) -- (axis cs: 2,0);
      \draw[help lines] (axis cs:0,-2) -- (axis cs: 0,2);
      \draw[MidnightBlue] (axis cs: -2,0) -| (axis cs: -1,1) -| (axis cs:1,0) -- (axis cs: 2,0) ;
    \end{axis}
  \end{tikzpicture}
  \caption{Histogram for the velocity.}
  \label{fig:hist-vel}
\end{figure}

Introducing the \acct{propagator}
\begin{align}
  p^{\mathrm{hist}}(z_2,t;z_1|\xi^{0}) &\equiv \lim\limits_{\mathcal{T} \to \infty} \frac{1}{\mathcal{T}} \int\limits_{0}^{\mathcal{T}} \delta\left( z(t+t') - z_2 \right) \delta\left( z(t') - z_1  \right) \diff t' \label{eq:15.05.15-1}
\end{align}
allows us to determine the probability of finding the particle at
position $z_2$ at time $t+t'$ if we first pass the position $z_1$ at
the earlier time $t'$. The propagator is depicted in
figure~\ref{fig:prop}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{axis}[
      xtick = \empty,
      ytick = \empty,
      xlabel = $t$,
      ylabel = $p^{\mathrm{hist}}$,
      xmin = 0, xmax = 4,
      ymin = 0, ymax = 1.25,
      width = 5cm
      ]
      \pgfplotsinvokeforeach{1,2,3}{
	\draw[MidnightBlue]  (axis cs:#1-.1,0) -- (axis cs:#1-.1,1);
	\draw[MidnightBlue]  (axis cs:#1+.1,0) -- (axis cs:#1+.1,1);
      }
    \end{axis}
  \end{tikzpicture}
  \caption{Propagator for model system 1.}
  \label{fig:prop}
\end{figure}


\subsection{Model system 2: Classical particle in fluid}

Another model system is given by a box filed with a fluid and a heavy
particle. For better imagination the system is depicted in
figure~\ref{fig:box2}. Other typical quantities for this case are also
shown in figure~\ref{fig:model2-part}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = -0.01, xmax = 1.01,
        ymin = -0.01, ymax = 1.01,
        domain=0:1,
        xtick=\empty, ytick=\empty,
	width=5cm
        ]
        \addplot [only marks, mark=*, samples=50, mark size=0.3] {0.5 + 0.5*rand};
        \node[dot,MidnightBlue] at (axis cs: 0.4,0.6) {a};	
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Particle in a box with fluid.}
  \label{fig:box2}
\end{figure}

With equation~\eqref{eq:15.05.15-1} we can determine the propagator. A
more precise investigation shows:
\begin{itemize}
\item The relaxation time $\tau_{\mathrm{rel}}$, beyond which
  \begin{align*}
    p^{\mathrm{hist}}(z_2,t;z_1|\xi^{0}) \to p^{\mathrm{hist}}(z_2|\xi^0) \quad \text{for} \quad t \gg \tau_{\mathrm{rel}}.
  \end{align*}
\item The system seems to ``forget'' where it was.
\item The propagator becomes independent of the initial value.
\item However, it seems that the total energy matters.
\end{itemize}

\begin{figure}[tb]
  \centering 
  \begin{tikzpicture}[gfx,scale=0.85]
    \node (a) at (4,-0.8) {(d)};
    \node at (8.5,-0.8) {(e)};
    \node (a) at (1.75,3.2) {(a)};
    \node (a) at (6.25,3.2) {(b)};
    \node (a) at (10.75,3.2) {(c)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin = 0, xmax = 3, 
	ymin = -0, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
	xlabel = $t$,
        ylabel = $z$,
	width = 5cm
        ]
	\addplot[MidnightBlue, domain=0:3,samples=150] {0.3*rand+0.75};
	\draw[orange,dashed] (axis cs: 0,0.9) -- (axis cs: 5,0.9);
	\draw[orange,dashed] (axis cs: 0,0.6) -- (axis cs: 5,0.6 );
        \node[orange,label={-40:{\color{orange} $z_1$}},circle,fill,inner sep=1pt] at (axis cs:0,0.6) {};
        \node[orange,label={40:{\color{orange}$z_2$}},circle,fill,inner sep=1pt] at (axis cs:0,0.9) {};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(4.5,0)}]
      \begin{axis}[
	xmin = 0, xmax = 3, 
	ymin = -1.5, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
	xlabel = $t$,
        ylabel = $v$,
	width = 5cm
        ]
	\addplot[MidnightBlue, domain=0:3,samples=150] {0.8*cos(x*300)*cos(x*500)+rand/6};
	\draw [help lines] (axis cs: 0,0) -- (axis cs: 3,0);
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(9,0)}]
      \begin{axis}[
	xmin = 0, xmax = 3, 
	ymin = 0, ymax =1,
	xtick=\empty, 
	ytick=\empty,
	xlabel = $z$,
        ylabel = $p^{\mathrm{hist}}(z)$,
	width = 5cm
        ]
	\draw [help lines] (axis cs: 0.2,0) -- (axis cs: 0.2,0.72);
	\addplot [MidnightBlue,samples=150,domain=0.2:3]  {1*exp(-x/1)-0.1};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(2.25,-4)}]
      \begin{axis}[
	xmin = -3, xmax = 3, 
	ymin = -0.5, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
	xlabel = $v$,
        ylabel = $p^{\mathrm{hist}}(v)$,
	width = 5cm
        ]
	\draw [help lines] (axis cs: -3,0) -- (axis cs: 3,0);
	\draw [help lines] (axis cs: 0,-0.5) -- (axis cs: 0,1.5);
	\addplot [MidnightBlue,samples=150]  {1*exp(-x^2/1)};
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(6.75,-4)}]
      \begin{axis}[
	xmin =0, xmax = 2.5, 
	ymin = 0 , ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
	xlabel = $t$,
        ylabel = propagator,
	width = 5cm
        ]
	\draw [help lines,dashed] (axis cs: 0,1) -- (axis cs: 3,1);
	\addplot [MidnightBlue,samples=150]  {(1-exp(-x^2))};
	\node[label={-10:{$\tau_{\mathrm{rel}}$}},circle,fill,inner sep=1pt] at (axis cs: 0.84,0.5) {};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{(a) Position $z$ over $t$. (b) Velocity over $t$. (c)
    Boltzmann distribution as histogram over $z$. (d) Maxwell Boltzmann
    distrubition as histogram over the velocity. (e) Propagator.}
  \label{fig:model2-part}
\end{figure}
\subsection{Model system 3: Box with fluid}
The last model system is given by a box filled with $10^{23}$ solvent
particles as shown in figure~\ref{fig:box3}.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
        xmin = -0.01, xmax = 1.01,
        ymin = -0.01, ymax = 1.01,
        domain=0:1,
        xtick=\empty, ytick=\empty,
	width=5cm
        ]
        \addplot [only marks, mark=*, samples=100, mark size=0.3] {0.5 + 0.5*rand};
        \draw[help lines, dashed] (axis cs: 0.505,-0.01) -- (axis cs: 0.505,1.01) ;
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Box with solvent particles.}
  \label{fig:box3}
\end{figure}

Here we want to look at different observables:
\begin{enumerate}
\item The first observable is $N_{\ell}$, which is the number of
  particles in the left half of the box. In figure~\ref{fig:kin} (a)
  and (b) the fluctuating observable and the corresponding histogram
  is depicted. As shown the histogram shows a Gaussian distribution.
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx,scale=0.85]
      \node (a) at (4,3.2) {(a)};
      \node at (8.5,3.2|-a) {(b)};
      \node (a) at (1.75,-0.8) {(c)};
      \node (a) at (6.25,-0.8) {(d)};
      \node (a) at (10.75,-0.8) {(e)};
      \begin{scope}[shift={(2.25,0)}]
        \begin{axis}[
          xmin = 0, xmax = 3, 
          ymin = -0, ymax =1,
          xtick=\empty, 
          ytick=\empty,
          xlabel = $t$,
          ylabel = $N_\ell(t)$,
          width = 5cm,
          ]
          \addplot[MidnightBlue, domain=0:3,samples=150] {0.025*rand+0.5};
          \draw [help lines,dashed] (axis cs: 0,0.5) -- (axis cs: 3,0.5);
        \end{axis}
      \end{scope}
      \begin{scope}[shift={(6.75,0)}]
        \begin{axis}[
          xmin = 0, xmax = 3, 
          ymin = 0, ymax =1.5,
          xtick=\empty, 
          ytick=\empty,
          xlabel = $N_\ell$,
          ylabel = $p^{\mathrm{hist}}(N_\ell)$,
          width = 5cm,
          xtick = {1.5},
          xticklabels = {$N/2$},
          xmajorgrids = true
          ]
          \addplot [MidnightBlue,smooth,samples=50,domain=0.5:2.5]  {4/(3.1415*(1+20*(x-1.5)^2))};
        \end{axis}
      \end{scope}
      \begin{scope}[shift={(0,-4)}]
        \begin{axis}[
          xmin = 0, xmax = 3, 
          ymin = -0, ymax =1,
          xtick=\empty, 
          ytick=\empty, 
          xlabel = $t$,
          ylabel = $E^k(t)$,
          width = 5cm
          ]
          \addplot[MidnightBlue, domain=0:3,samples=150] {0.025*rand+0.5};
          \draw [help lines,dashed] (axis cs: 0,0.5) -- (axis cs: 3,0.5);
        \end{axis}
      \end{scope}
      \begin{scope}[shift={(4.5,-4)}]
        \begin{axis}[
          xmin = 0, xmax = 3, 
          ymin = 0, ymax =1.5,
          xtick=\empty, 
          ytick=\empty,
          xlabel = $E^k$,
          ylabel = $p^{\mathrm{hist}}(E^k)$,
          width = 5cm,
          xtick = {1.5},
          xticklabels = {$\hat{E}^k$},
          xmajorgrids = true
          ]
          \addplot [MidnightBlue,smooth,samples=50,domain=0.5:2.5]  {4/(3.1415*(1+20*(x-1.5)^2))};
        \end{axis}
      \end{scope}
      \begin{scope}[shift={(9,-4)}]
        \begin{axis}[
          xmin =0.7, xmax = 3, 
          ymin = -0.3 , ymax =0.3,
          xtick=\empty, 
          xlabel = $|\bm{r}_i-\bm{r}|$,
          ylabel = $V(|\bm{r}_i-\bm{r}|)$,
          width = 5cm,
          ytick = {0},
          ymajorgrids = true,
          ]
          \addplot [MidnightBlue,smooth,samples=50,domain=0.7:5]  {-(1/(x^6))+(1/(x^(12)))};
        \end{axis}
      \end{scope}
    \end{tikzpicture}
    \caption{(a) Number of the particles in the left half of the box
        as function of $t$. The number fluctuates around $N/2$. (b)
        Histogram with a maximum at $N/2$. (c) Fluctuating kinetic
        energy $E^k$ of the particles over time $t$. (d) Histogram as
        functino of the kinetic energy. At $\hat{E}^k$ the histogram
        has a maximum. (e) Interaction potential of the particles.}
      \label{fig:kin}
    \end{figure}
  \item The second observable is the kinetic energy of all particles
  \begin{align*}
    E^{\mathrm{k}} (t) &\equiv \sum\limits_{\ell = 1}^{N} \frac{\bm{p}_i^2(t)}{2m}.
  \end{align*}
  The most likely value $\hat{E}^{\mathrm{k}}$ will depend on
  \begin{align*}
    \frac{1}{N} \hat{E}^{\mathrm{k}} (E,V,N) &= \hat{\varepsilon}\left(\frac{E}{N},\frac{V}{N} \right)
  \end{align*}
  in the limit $E,V,N \to \infty$. In figure~\ref{fig:kin} (c)-(e)
  there are depicted the kinetic energy as function of the time, the
  histogram $p^{\mathrm{hist}}$ over $E^k$ and the interaction
  potential of the particles.
\end{enumerate}

Here we have take into account the assumption of homogeneity in the
thermodynamic limit!


%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: