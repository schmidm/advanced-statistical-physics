\part{Advanced foundations}

\chapter{Phase transitions}

\section{Phenomenology}

\begin{figure}[tbh]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =0, xmax = 5, 
	ymin = 0, ymax =5,
	xtick=\empty, 
	ytick=\empty,
    	ylabel = $p$,
        xlabel = $T$,
	width = 5cm,
        xmajorgrids = true
        ]
	\draw[] (axis cs: 0.5,0.5) -- (axis cs: 2.5,2.5)  (axis cs: 2.5,2.5) -- (axis cs: 4.5,3.5)  (axis cs: 0.75,4.25) -- (axis cs: 2.5,2.5);
	\node[circle,fill,inner sep=1pt] at (axis cs: 2.5,2.5) {};
	\node[circle,fill,inner sep=1pt] at (axis cs: 4.5,3.5) {};
        \node[MidnightBlue] at (axis cs: 3.6,1.2) {gas};  
	\node[MidnightBlue] at (axis cs: 1,2.2) {solid};  
	\node[MidnightBlue] at (axis cs: 3,4) {liquid};  
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin = 0, xmax = 5, 
	ymin = 0, ymax =5,
	xtick=\empty, 
	ytick=\empty,
        xlabel = $T$,
        ylabel = $p$,
	width = 5cm
        ]
	\draw[] (axis cs: 0.5,0.5) -- (axis cs: 2,2.5)  (axis cs: 2,2.5) -- (axis cs: 4.5,3)  (axis cs: 2,2.5) -- (axis cs: 3.5,4.25);
	\node[circle,fill,inner sep=1pt] at (axis cs: 2,2.5) {};
	\node[circle,fill,inner sep=1pt] at (axis cs: 4.5,3) {};
        \node[MidnightBlue] at (axis cs: 3.6,1.2) {gas};  
	\node[MidnightBlue] at (axis cs: 1,2.5) {solid};  
	\node[MidnightBlue] at (axis cs: 4,3.5) {liquid};  
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{Phase diagramm for: (a) water. (b) something else.}
  \label{fig:24.6.15-1}
\end{figure}

In figure~\ref{fig:24.6.15-1} there are depicted two different phase
diagrams, viz.\ for water and for something else (e.g.\ carbon
dioxide). According to the phenomenological description of phase
transitions (for example see \cite{reif,peliti,kadar,schwabl}) the
interception point of all three lines is called \acct{triple
  point}. The points on the end of each line are critical points which
corresponds to a second order phase transitions. Transitions along a
line are of first order.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,scale=0.6]
    \begin{scope}
      \foreach \x in {1,2,3} {
        \foreach \y in {1,2,3} {
          \shade[ball color=white] (\x,\y) circle (.2);
        }
      }
      \node at (2,0) {solid};
    \end{scope}
    \draw[->] (3.5,2) -- (5,2);
    \begin{scope}[shift={(4,0)}]
      \pgfmathsetseed{9}
      \foreach \x in {1,2,3} {
        \foreach \y in {1,2,3} {
          \shade[ball color=white] (\x+.4*rand+1,\y+.4*rand) circle (.2);
        }
      }
      \node at (2,0) {liquid/gas};
    \end{scope}
  \end{tikzpicture}
  \caption{Phase transition from solid to liquid or gaseous. The
    phenomenological theories do not allow to distinguish between the
    fluid or the gaseous phase.}
  \label{fig:24.6.15-2}
\end{figure}
As we can see in figure~\ref{fig:24.6.15-2} the phenomenological
theories do not allow to distinguish between the liquid or gaseous
phase, however, solids have a periodic order and the translation
symmetry is broken. In terms of symmetry, liquids and gases are the
same.

\section{Ising model}
The \acct{Ising model} is the simplest model for a magnet (1925).

\subsection{Definition}

Given is a lattice in $d$-dimensions. On each site of the lattice
there is a spin $s_i = \pm 1$. A micro state $\alpha$ of the system is
then characterized by
\begin{align*}
  \{s_i\} &= \{+1,-1,-1,\ldots \} \equiv \alpha.
\end{align*}
For the two dimensional case the Ising model is depicted in
figure~\ref{fig:24.6.15-3}. The whole system is in contact with a heat
bath of temperature $T(\beta)$.
\begin{figure}[tb]
  \centering
  \def\spin[#1] at (#2,#3);{
    \draw[DarkOrange3,#1] ({#2},{#3-.4}) -- ({#2},{#3+.4});
    \node[DarkOrange3,draw,fill,circle,inner sep=1pt] at ({#2},{#3}) {};
  }
  \begin{tikzpicture}[gfx,scale=0.6]
    \draw[help lines] (-.5,-.5) grid (4.5,3.5);
    \draw[MidnightBlue,->] (-1,1)--node [midway,left] {$H'$}(-1,2);
    \spin[<-] at (1,1);
    \spin[->] at (2,1);
    \spin[<-] at (3,1);
    \spin[->] at (1,2);
    \spin[<-] at (2,2);
    \spin[->] at (3,2);
  \end{tikzpicture}
  \caption{Ising model in two dimensions. The spins $s_i = \pm 1$ are
    characterized by orange arrows. An external field $H'$ can align
    the spins.}
  \label{fig:24.6.15-3}
\end{figure}
For the system energies we find
\begin{align*}
  E_1 &= - \sum\limits_i H' s_i, \\
  E_2 &= - J \sum\limits_{\braket{i,j}} s_i s_j,
\end{align*}
where $E_1$ is the energy of the parallel or antiparallel aligned
spins and $E_2$ describes the coupling between the nearest
neighbors. Here $J$ denotes the coupling constant and $\braket{i,j}$
the summation over the nearest neighbors. Hence we can write for a
micro state
\begin{align*}
  E_\alpha &= E_1(\alpha) + E_2(\alpha) \\
  &= - H'\sum\limits_i s_i - J \sum\limits_{\braket{i,j}} s_i s_j.
\end{align*}
So we can define the \acct{magnetization} in equilibrium as
\begin{align*}
  m &\equiv \frac{1}{N} \sum\limits_{i=0}^{N} \braket{s_i} \\
  &= \frac{1}{N} \sum\limits_{i=1}^{N} \sum\limits_\alpha s_i(\alpha) p_\alpha \\
  &= m(\beta H',\beta J),
\end{align*}
where the probability for a certain configuration is given by
\begin{align*}
  p_\alpha &\equiv \frac{1}{\mathcal{Z}} \ee^{- \beta E_\alpha}.
\end{align*}
Here $\mathcal{Z}$ denotes the \acct{partition function}. Note that
the magnetization is a dimensionless quantity. Hence we can define
the dimensionless variables
\begin{align*}
  H &\equiv \beta H', \\
  K &\equiv \beta J,
\end{align*}
where $H$ is the scaled field and $K$ the scaled coupling
constant. Without external field $H' = 0$ the magnetization takes on
the dependency $m = m(\beta J) = m(K)$. The question is know how it
depends on $T$.

\subsection{Mean field theory approximation (MFT)}
 
Below we want to show two different types of solutions for the two
dimensional Ising model. Because it is really hard to solve it we have
to do an approximation, viz.\ the \acct{mean field approximation}.

\minisec{Version 1} In the first version we want to regard just one
spin $s_i$ in the mean field of the adjacent ones. Hence we have only
to sum over the adjacent spins. The energy involving $s_i$ is then
given by
\begin{align*}
  E_i &= - H' s_i - J \sum\limits_{\braket{i,j}} s_is_j \\
      &= s_i \bigg(-H'-J\sum\limits_j s_j  \bigg) 
\intertext{the mean filed approximation now says that we have to replace the sum $\sum_j s_j$ with $m \cdot z$, where $z$ is the number of adjacent spins and $m$ the magnetization. Hence we find}
      &=  s_i \left(-H'-J z m  \right). 
\end{align*}
Introducing the new field
\begin{align*}
  \tilde{H} &= H' + Jzm
\end{align*}
allows us to rewrite the energy $E_i = -S_i \tilde{H}$. The mean
value of $s_i$ is then given by
\begin{align*}
  \braket{s_i} &= \sum\limits_{s_i} \frac{1}{\mathcal{Z}} s_i \ee^{-\beta H_i} \\
  &= \frac{1}{\mathcal{Z}} \sum\limits_{s_i = \pm 1} s_i \ee^{\beta \tilde{H}s_i}.
\end{align*}
The partition function $\mathcal{Z}$ is
\begin{align*}
  \mathcal{Z} &= \sum\limits_{s_i} \ee^{-\beta E_i} = 2 \cosh(\beta \tilde{H}).
\end{align*}
Plugging into the mean value leads to
\begin{align*}
  \braket{s_i} &= \frac{\sinh(\beta \tilde{H})}{\cosh(\beta\tilde{H})} = \tanh(\beta \tilde{H}).
\end{align*}
According to the definition of the magnetization $m$ we find (for
$H'=0$)
\begin{align*}
  \braket{s_i} \stackrel{!}{=} m.
\end{align*}
Plugging in the results then gives the implicit function
\begin{align*}
  \boxed{ \tanh(\beta J m z) = m }
\end{align*}
An analytic discussion of this kind of function is in general a bit
cumbersume. Therefore we solve it graphically. A possible solution is
given in figure~\ref{fig:24.6.15-4} (a).
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =-5, xmax = 5, 
	ymin = -1.5, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
    	ylabel = $$,
        xlabel = $m$,
	width = 5cm,
        xmajorgrids = true
        ]
        \addplot [MidnightBlue,smooth,samples=50] {tanh(x)};
        \addplot [DarkOrange3,smooth,samples=50] {0.3*x};
	\draw[help lines] (axis cs: 0,-1.5) --(axis cs: 0,1.5);
	\draw[help lines] (axis cs: -5,0) --(axis cs: 5,0);  
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin = 0, xmax = 2, 
	ymin = -1.5, ymax =1.5,
	xtick=\empty, 
	ytick=\empty,
        xlabel = $T$,
        ylabel = $p$,
	width = 5cm
        ]
	\draw[help lines] (axis cs: 0,0) --(axis cs: 2,0);
	\draw[MidnightBlue] (axis cs: 1,0) --(axis cs: 2,0);
	\draw[MidnightBlue,dashed] (axis cs: 0,0) --(axis cs: 1,0);
	% \addplot [MidnightBlue,smooth,samples=50,domain=0.01:1] {x/(0.5*ln(1+x)-0.5*ln(1-x))};
	% \addplot [MidnightBlue,smooth,samples=50,domain=0.01:1] {-x/(0.5*ln(1+x)-0.5*ln(1-x))};
	\addplot [MidnightBlue,smooth,samples=100,domain=0:1] {sqrt(1-x^2)};
	\addplot [MidnightBlue,smooth,samples=100,domain=0:1] {-sqrt(1-x^2)};
	\node[circle,fill,MidnightBlue,inner sep=0.5pt] at (axis cs: 1,0) {};
      \end{axis}
    \end{scope}
  \end{tikzpicture}
  \caption{(a) Graphical solution of the implicit function
$\tanh(\beta J mz) = m$. The blue curve corresponds to the left term
and the orange curve to the right term. (b) Magnetization as function
of the temperature. }
  \label{fig:24.6.15-4}
\end{figure}
From the graphical solution arrise different situations:
\begin{enumerate}
\item For $z\beta J > 1$ we find three solutions. Introducing the
  critical temperature $T_c \equiv 4 J/\kB$ then leads to
  \begin{align*}
    T < T_c = \frac{4J}{\kB}.
  \end{align*}
\item For $T> 4 J/\kB$ we only obtain one solution. Hence this is the
  case for $m=0$.
\end{enumerate}
Plotting the magnetization $m$ over the temperature $T$ then leads to
figure~\ref{fig:24.6.15-4} (b).




%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: