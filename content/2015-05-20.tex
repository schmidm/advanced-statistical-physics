Consider a system as depicted in figure~\ref{fig:20.5.15-1}. A box
filled with a fluid is constrained by a piston, which is coupled to a
spring. By applying a force at the spring one can change the position
of the piston.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx,,decoration={random steps,segment length=1.2mm}]
    \begin{scope}[shift={(0,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-2cm+1cm, rnd*-1cm) circle (.25pt);
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-1,0) -- (-1,-1) (-2.5,-0.5)--(-2.5,-1.5); 
      \draw[decoration={aspect=0.3, segment length=2mm, amplitude=2mm,coil},decorate] (-1,-0.5) -- (-2.5,-.5);
      \draw[fill, DimGray!75] (-2.75,-1.5)--(-2.25,-1.5) -- (-2.25,-2) --(-2.75,-2) node [midway, below] {$\lambda$} --cycle;
      \draw [->] (1.5,-0.5)--(2.5,-0.5);
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*-1cm+1cm, rnd*-1cm) circle (.25pt);
      \draw (-1.5,0) -- (1,0) -- (1,-1) -- (-1.5,-1)  (-0,0) -- (-0,-1) (-.75,-0.5)--(-.75,-1.5); 
      \draw[decoration={aspect=0.4, segment length=1mm, amplitude=2mm,coil},decorate] (0,-0.5) -- (-0.75,-.5);
      \draw[fill, DimGray!75] (-1,-1.5)--(-.5,-1.5) -- (-.5,-2) --(-1,-2) node [midway, below] {$\lambda=0$} --cycle;
    \end{scope}
    \begin{scope}[shift={(1,-4.5)}]
      \draw[<->] (0,2) |- (3,0) node [below] {$t$};
      \draw[MidnightBlue] (0,1) node [left] {$\lambda$} --(1,1) --(2.5,0);
      \draw[dashed] (1,0) node[below] {$t_1$} --(1,2);   
      \draw[dashed] (2.5,0) node[below] {$t_2$} --(2.5,2);   
      \draw[DarkOrange3,domain=0:1,variable=\x,samples=50] plot ({\x},{1.5+0.1*rand});
      \node [left,DarkOrange3] at (0,1.5) {$z$};
      \draw[DarkOrange3,domain=1:2.5,variable=\x,samples=75]  plot ({\x},{-1/1.5*\x+2.2+0.1*rand});
      \draw[DarkOrange3,domain=2.5:3,variable=\x,samples=25]  plot ({\x},{0.5+0.1*rand});
      \end{scope}
  \end{tikzpicture} 
  \caption{Given is box filled with fluid which is constraint by a
    piston. The piston is coupled to spring which a person can
    change. At time $t_1$ we change the position of the string by
    applying a force. Hence the piston will sink and fluctuate at the
    new height $z$.}
  \label{fig:20.5.15-1}
\end{figure}
The Hamilton function is given by
\begin{align*}
  \HH(\underbrace{\bm{\xi}_0,z,p}_{\equiv \bm{\xi}},\lambda) &= \HH_0(\bm{\xi}_0|z) + \frac{k}{2} (z-\lambda)^2 + \frac{p^2}{2m}.
\end{align*}
The time derivative of the Hamilton function then takes on the form
\begin{align*}
  \dot{\HH} &= \underbrace{\frac{\partial \HH}{\partial \bm{\xi}} \dot{\bm{\xi}}  }_{=0} + \frac{\partial \HH}{\partial \lambda} \dot{\lambda},
\end{align*}
where the left term vanishes because for fix $\lambda$ the energy is
conserved and the right term describes the work applied to the
system. Hence we can define the work as
\begin{align*}
  \diff W \equiv \frac{\partial \HH}{\partial \lambda} \diff \lambda  = - k (z-\lambda(t)) \diff \lambda.
\end{align*}
Here $\diff \lambda$ gives the displacement and $-k(z-\lambda(t))$ a
force. We draw the initial point $\bm{\xi}^1 = \bm{\xi}_0^1 ,z^1,p^1$
and shut of the heat bath. The total work is then
\begin{align*}
  W &= \int\limits_{t^1}^{t^2} \dot{\omega} \diff t \\
  &= \int\limits_{t^1}^{t^2} F(t) \dot{\lambda} \diff t \\
  &= W[\bm{\xi}^0,\lambda(t)].
\end{align*}
The histogram is shown in figure~\ref{fig:20.5.15-2}. It shows that
the faster we pull at the string, the wider is the peak and vice
versa.
\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \node (a) at (-0.5,3.2) {(a)};
    \node at (4.5,3.2|-a) {(b)};
    \begin{scope}[shift={(0,0)}]
      \begin{axis}[
	xmin =0, xmax = 5, 
	ymin = 0, ymax =5,
	xtick=\empty, 
	ytick=\empty,
    	ylabel = $p(W)$,
        xlabel = $W$,
	width = 5cm,
        xmajorgrids = true
        ]
	\addplot [domain=0:5,MidnightBlue,samples=50,smooth]  {4*exp(-(x-0.75)^2/(0.1))};
	\node[MidnightBlue] at (axis cs: 0.8,4.5) {slower};
	\addplot [domain=0:5,MidnightBlue,samples=35,smooth]  {2*exp(-(x-2)^2/(0.3))};
	\addplot [domain=0:5,MidnightBlue,samples=25,smooth]  {0.8*exp(-(x-3.5)^2/(0.7))};
        \node[MidnightBlue] at (axis cs: 3.6,1.2) {faster};  
      \end{axis}
    \end{scope}
    \begin{scope}[shift={(5,0)}]
      \begin{axis}[
	xmin = 0, xmax = 5, 
	ymin = 0, ymax =2,
	xtick=\empty, 
	ytick=\empty,
        xlabel = $W$,
	width = 5cm
        ]
	\addplot [DarkOrange3,smooth,samples=20]  {1.75*exp(-x)};
	\node[DarkOrange3] at (axis cs: 2.5,1.5) {$\exp(-\beta W)$};
      \end{axis}
    \end{scope}
  \end{tikzpicture}  
  \caption{(a) Histogram for different velocities of applying a force to
    the spring. (b) Jarzynski relation.}
  \label{fig:20.5.15-2}
\end{figure}
The histogram depends on the protocol. Schedule $\lambda(t)$ for fixed
$\lambda^{(1)}$ and $\lambda^{(2)}$ but varying the time difference
$t^{(2)} - t^{(1)}$. Jarzynski showed in 1997
\begin{align*}
  \int \diff \omega p(\omega) \ee^{-\beta \omega } &\stackrel{!}{=} \ee^{-\beta \Delta F},
\end{align*}
which means that the left side of this equation is independent for
$\lambda(t)$ for fixed $\lambda^{(1)}$ and $\lambda^{(2)}$. The free
energy is given by
\begin{align*}
  \ee^{-\beta F(\lambda)} &= \int \ee^{-\beta \HH} \diff \bm{\xi} \\
  &= \int \diff \bm{\lambda}_0 \diff z \diff p \ee^{-\beta[ \HH_0(\bm{\xi}_0,z)+ k/2 (z-\lambda^2) +p^2/(2m)]} \\
  &= \left(\frac{2 \pi m}{\beta}\right)^{1/2} \int \diff z \ee^{-\beta F_0(z)} \ee^{-\beta k(z-\lambda)^2}
    \intertext{with the saddle point integration $\partial_z F_0|_{\hat{z}} = - k (\hat{z} - \lambda)|_{\hat{z}(\lambda)}$ we find}
    &= \left( \frac{2\pi m}{\beta} \right)^{1/2} \ee^{-\beta F_0(\hat{z}) - \beta k/2(\hat{z}-\lambda)^2 } \left( \frac{2 \pi }{\beta (F_0''|_{\hat{z}} + k )}  \right)^{1/2}.
\end{align*}
Hence the free energy difference is
\begin{align*}
  \Delta F(\lambda) &= \underbrace{\Delta F_0(\hat{z})}_{\mathcal{O}((\kB T)^{23})} + \underbrace{\Delta \left( \frac{\beta k}{2} (\hat{z} - \lambda) \right)^2}_{\mathcal{O}(\kB T)} + \Delta \log F''.
  \intertext{Because the second and the third term are small compared to the first one we find}
  \Delta F(\lambda) &= \Delta F_0(\hat{z}(\lambda)).
\end{align*}
This relation implies $\braket{W}|_{\lambda(t)} \geq \Delta F$. The
equality of both sides holds for infinitely slowly pulling of the
spring.

If we look at a cyclic variation of the protocol parameter $\lambda$
the average of the work is
\begin{align*}
  \braket{W} \geq 0,
\end{align*}
which means that there is no work extraction from a cyclic process
(Kelvin's formulation of the second law).

\begin{notice}[Paradox:]
  A paradox is shown in figure~\ref{fig:20.5.15-3}.
  \begin{figure}[tb]
    \centering
    \begin{tikzpicture}[gfx]
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*1.5cm, rnd*0.5cm) circle (.25pt);
      \draw (0,2.5) -- (0,0) -- (1.5,0) -- (1.5,2.5)  (0,0.5) -- (1.5,0.5) (0.75,0.5)--(0.75,2.5); 
      \foreach \i in {1,...,500}
      \fill [MidnightBlue] (rnd*1.5cm+3cm, rnd*0.5cm) circle (.25pt);
      \draw (3,2.5) -- (3,0) -- (4.5,0) -- (4.5,2.5)  (3,2.0) -- (4.5,2.0) (3.75,2.0)--(3.75,2.5); 
      \draw[->] (1.75,1) --(2.75,1);
    \end{tikzpicture} 
    \caption{Given is a box with fluid which is compressed by a piston
      at position $z$. Then we remove the piston and the total work is
      $W =0$. Due to the tail of the Boltzmann distribution this is
      wrong. }
    \label{fig:20.5.15-3}
  \end{figure}
  A piston is placed into a box with fluid at the position $z$. After
  removing the piston the total work is
  \begin{align*}
    W = 0
  \end{align*}
  and we find for the Jarzynski relation
  \begin{align*}
    \braket{\ee^{-\beta \omega}} = 1 \neq \ee^{- \beta F} \neq 1.
  \end{align*}
  The solution of the paradox is that $W = 0$ not true because of the
  tail in the Boltzmann distribution, i.e.\ most of the events
  fulfills $W = 0$ but not all.
\end{notice}

\section{Heat exchange}
Here we want again discuss the example depicted in
figure~\ref{fig:13.5.15-1} (a). The Hamilton function is given by
\begin{align*}
  \HH^0 = \HH_1(\xi_1) + \HH_2(\xi_2).
\end{align*}
For $t>0$ (both systems in contact) we have to add an additional
interaction function
\begin{align*}
  \HH^1 &= \HH_1(\xi_1) + \HH_2(\xi_2) + \HH_{12}(\xi_1,\xi_2).
\end{align*}
Establishing the contact costs some work
\begin{align*}
  W[\bm{\xi}_1,\bm{\xi}_2] &= \HH^1(\bm{\xi}^0) - \HH^0(\bm{\xi}^0).
\end{align*}
Assume a big system so we can write for the probability
\begin{align*}
  p_{1,2}^{0}(\bm{\xi}_{1,2}^0) &= \ee^{-\beta_{1,2} \HH_{1,2}^0 + \beta_{1,2} F_{1,2}}. 
\end{align*}
The integral fluctuation theorem then says
\begin{align*}
  p^1(\beta) &= p^0(\bm{\xi})\\
  \implies \braket{\ee^{- \log \frac{p^1(\xi^t)}{p^0(\xi^0)}}} &=1.
\end{align*}
with
\begin{align*}
  \log \frac{p^1(\xi^t)}{p^0(\xi^0)} &= - \beta_1 \left( \HH_1(\bm{\xi^t}) -\HH_1(\xi^0)  \right) - \beta_2 \left(\HH_2(\bm{\xi}^t) - \HH_2(\bm{\xi}^0) \right)\\
  &= - \beta_1 \Delta E_1 - \Delta E_2 \beta_2.
\end{align*}
Hence we find 
\begin{align*}
  \Braket{\ee^{-\beta_1 \Delta E_1 - \beta_2 \Delta E_2}} = 1
\end{align*}
and the inequality
\begin{align*}
  \beta_1 \braket{\Delta E_1} + \Delta \braket{\Delta E_2} \geq 0.
\end{align*}
Assume that $\HH_{12} \ll \HH_{1,2}$ (small coupling) the energy
conservation implies
\begin{align*}
  \Delta E_1 + \Delta E_2 &= 0.
\end{align*}
Plugging this in leads to
\begin{align*}
  (\beta_1 - \beta_2) \braket{\Delta E_1} \geq 0.
\end{align*}
Now we can compare two cases:
\begin{enumerate}
\item For $\beta_1 > \beta_2$ the average $\braket{(\Delta E_1)} \geq
  0$ which gives the direction of the heat flow (kind of proof of
  second law).
\item For $\beta_1 < \beta_2$ we find $\braket{(\Delta E_1)} \leq 0$.
\end{enumerate}
The average $\braket{\exp(-R)} = 1$ implies that there are (a few)
events with $R < 0$.
%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "en_US"
%%% TeX-master: "../ASP.tex"
%%% End: